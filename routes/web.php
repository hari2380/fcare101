<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('api/sessId', 'apiController@sessId');
Route::post('api/verifyotp', 'apiController@verifyotp');
Route::post('api/sendotp', 'apiController@sendotp');
Route::post('apistore/dedupedata', 'apistoreController@dedupedata');
Route::post('api/redirection', 'apiController@redirection');
Route::post('apistore/failData', 'apistoreController@failData');
Route::post('apistore/pandedupe', 'apistoreController@pandedupe');
Route::post('apistore/stageData', 'apistoreController@stageData');
Route::post('api/sessUpdate', 'apiController@sessUpdate');
Route::post('api/aadhaardedupe', 'apiController@aadhaardedupe');
Route::post('api/aadhaarotpverify', 'apiController@aadhaarotpverify');
Route::post('api/aadhaarverify', 'apiController@aadhaarverify');
Route::post('api/amlverification', 'apiController@amlverification');
Route::post('api/panverify', 'apiController@panverify');
Route::post('api/namecheck', 'apiController@namecheck');
Route::post('api/dms_inicialize', 'apiController@dms_inicialize');











