var sess = {};
var rating = '';
var rating_str = '';
var emailids = '';
var rat = '';
var hau_data = '';
var orderId = '';
var payment_id = '';
var request_id = '';


//for loader		
$(document).ready(function() {
    $(document).ajaxStart(function() {
        $("#wait").show();
    });
    $(document).ajaxComplete(function() {
        $("#wait").hide();
    });

});
$(document).ajaxStart(function() {
	$("#wait").show();
});
$(document).ajaxComplete(function() {
	$("#wait").hide();
});



const urlParams = new URLSearchParams(window.location.search);

$( document ).ready(function() {
	fetch_profile_data_api(urlParams.get('profileId'),urlParams.get('ref'));
});

	

function fetch_profile_data_api(profileid,refno){

 
   
	$.post('https://101.fincarebank.com/101/api.php', {
		request_for: 'fetch_profile_data',
		profileid: profileid
	}, function (data, result) {

		if(data == 'failed'){

			getLinkForVideoKYC(profileid,refno);

		}
		else{
			$("#Rating_div").addClass("modal");
	        $("#Rating_div").attr("style", "display:block");
	        $("#Rating_div").modal({
				escapeClose: false,
				clickClose: false,
				showClose: false
			});
		}
		
		//console.log(data);
		
		
	});
}

// //if video capture failed 

function getLinkForVideoKYC(profileid,refno) {

	$.post('https://101.fincarebank.com/101/api.php', {
		request_for: 'getLink',
		profileId: profileid,
	}, function (data, result) {
		console.log(data);
		var res = JSON.parse(data);
		if (res.capture_link.length != 0) {
			sess.videoKYCLink = res.capture_link;

			var customer_ref = refno;
			var videoLink = sess.videoKYCLink;
			var update_profileId = profileid;
			var redirect_URL = 'https://101.fincarebank.com/101/index_rating.html?ref=' + customer_ref+'%26profileId='+update_profileId;
		
			var final_URL = videoLink + "&redirect_uri=" + redirect_URL;
			//console.log(final_URL);
			window.location.href = final_URL;


			
		} else {
			alert("Unable to genrate Capture Link, Try again later");
		}

	});
}

function setRating(obj) {

	if (obj.id == "r1") {
		rating = 1;
		$('#r1').removeClass('staropa');
		$('#r2').addClass('staropa');
		$('#r3').addClass('staropa');
		$('#r4').addClass('staropa');
		$('#r5').addClass('staropa');
		$('#261__Rating__customer_rating_less').removeClass('hide');
		$('#button_submit_rm').addClass('hide');
		$('#260__Rating__customer_rating').removeClass('hide');
		$('#263__Rating__customer_rating_more_tab').addClass('hide');
		$('#262__Rating__customer_rating_more').addClass('hide');
		$("#button_submit_rl").css("display", "block");
	} else if (obj.id == "r2") {
		rating = 2;
		$('#r1').removeClass('staropa');
		$('#r2').removeClass('staropa');
		$('#r3').addClass('staropa');
		$('#r4').addClass('staropa');
		$('#r5').addClass('staropa');
		$('#261__Rating__customer_rating_less').removeClass('hide');
		$('#button_submit_rm').addClass('hide');
		$('#260__Rating__customer_rating').removeClass('hide');
		$('#263__Rating__customer_rating_more_tab').addClass('hide');
		$('#262__Rating__customer_rating_more').addClass('hide');
		$("#button_submit_rl").css("display", "block");
	} else if (obj.id == "r3") {
		$('#r1').removeClass('staropa');
		$('#r2').removeClass('staropa');
		$('#r3').removeClass('staropa');
		$('#r4').addClass('staropa');
		$('#r5').addClass('staropa');
		rating = 3;
		$('#262__Rating__customer_rating_more').removeClass('hide');
		$('#261__Rating__customer_rating_less').addClass('hide');
		$('#button_submit_rm').removeClass('hide');
		$('#260__Rating__customer_rating').removeClass('hide');
		$('#263__Rating__customer_rating_more_tab').removeClass('hide');

	} else if (obj.id == "r4") {
		$('#r1').removeClass('staropa');
		$('#r2').removeClass('staropa');
		$('#r3').removeClass('staropa');
		$('#r4').removeClass('staropa');
		$('#r5').addClass('staropa');
		$('#262__Rating__customer_rating_more').removeClass('hide');
		$('#261__Rating__customer_rating_less').addClass('hide');
		$('#button_submit_rm').removeClass('hide');
		$('#260__Rating__customer_rating').removeClass('hide');
		$('#263__Rating__customer_rating_more_tab').removeClass('hide');

		rating = 4;
	} else if (obj.id == "r5") {
		$('#r1').removeClass('staropa');
		$('#r2').removeClass('staropa');
		$('#r3').removeClass('staropa');
		$('#r4').removeClass('staropa');
		$('#r5').removeClass('staropa');
		$('#262__Rating__customer_rating_more').removeClass('hide');
		$('#261__Rating__customer_rating_less').addClass('hide');
		$('#button_submit_rm').removeClass('hide');
		$('#260__Rating__customer_rating').removeClass('hide');
		$('#263__Rating__customer_rating_more_tab').removeClass('hide');

		rating = 5;
	}
	//console.log(rating);
	localStorage.setItem("rating", rating);


}

function ratingStore() {
	rat = localStorage.getItem('rating');
	request_for = 'ratingData';
	var cif = sess.CIF;
	var rating = rat;
	var email = emailids;
	var msg = hau_data;
	var comments = rating_str;
	$.post('https://101.fincarebank.com/101/apistoredata.php', {
		request_for: request_for,
		cif: cif,
		rating: rating,
		email: email,
		msg: msg,
		comments: comments
	}, function(data, result) {
		console.log(data);
		$.modal.close();
		$('.selection').hide();
		$(".vkyc-sccess").attr("style", "display:block");
		$(".vkyc-sccess1").attr("style", "display:none");
	});
	//upi_qr_gen();
}

function Storehau() {
	if ($('input[name="checkbox_customer_hau"]:checked')) {
		hau_data = $('input[name="checkbox_customer_hau"]:checked').val();
		if ($('input[name="checkbox_customer_hau"]:checked').val() == "Other_sources") {
			hau_data += $('#text_hau').val();
		}
		$.modal.close();
		ratingStore();
	}
	$("#264__Rating__customer_hau").hide();
	$("#13__Step4__checkbox_dbt_avail_yn").removeClass("hide");
	$("#16__Step4__checklist_dbtyn").removeClass("hide");
}

function Storeratingless() {
	rat = localStorage.getItem('rating');
	$("#button_submit_rm").hide();
	if(rating == 1 || rating == 2) {
		if($('input[name="checkbox_customer_rating_less"]:checked')) {
			rating_str = $('input[name="checkbox_customer_rating_less"]:checked').val();
		}
		if($('input[name="checkbox_customer_rating_less"]:checked').val() == "Other Reasons") {
			rating_str += $('#rating-input').val();
		}
	}
	
	$.modal.close();
	    ratingStore();
		Moengage.destroy_session();
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function Storeratingmore() {
	$("#button_submit_rl").hide();
	rat = localStorage.getItem('rating');
	if(rating == 3 || rating == 4 || rating == 5) {
		if($("#text_customer_rating_more_email1").val().length == 0) {
			functionAlert("Please Enter Email Id");
			return false;
		}
		for(i = 1; i <= 5; i++) {
			if($("#text_customer_rating_more_email" + i).val().length > 0) {
				if(i == 1) emailids = $("#text_customer_rating_more_email" + i).val();
				else {
					if(emailids == "") emailids += $("#text_customer_rating_more_email" + i).val();
					else emailids += "," + $("#text_customer_rating_more_email" + i).val();
				}
			}
		}
	}
		
	var email = emailids;
	
	$.post('https://101.fincarebank.com/101/kyc_schedule.php', {
		request_for: 'ratingmail',
		email: email
	}, function(data, result) {
		console.log(data);
		$.modal.close();
	    ratingStore();
	});
	Moengage.destroy_session();
	
	$.modal.close();
	    ratingStore();
}

function setShare(obj) {

	var id = obj.id;
	if (id == "defaultOpen")
		cityName = "share";
	if (id == "btn_customize")
		cityName = "customize";
	var i, tabcontent, tablinks;
	tabcontent = document.getElementsByClassName("tabcontent");
	for (i = 0; i < tabcontent.length; i++) {
		tabcontent[i].style.display = "none";
	}
	tablinks = document.getElementsByClassName("tablink");
	for (i = 0; i < tablinks.length; i++) {
		tablinks[i].style.backgroundColor = "";
	}
	document.getElementById(cityName).style.display = "block";
}

function fund_now() {
	$('#fund__Account').removeClass('hide');
}

function getRefFromURL() {
	const urlParams = new URLSearchParams(window.location.search);
	const ref = urlParams.get('ref');
	console.log(ref);
    getCustomerData(ref);
}



var mobile='';
var email='';


function getCustomerData(ref) {
	var customerRef = ref;
	$.post('https://101.fincarebank.com/101/apistoredata.php', {
		request_for: 'getCustomerData',
		customerRef: customerRef
	}, function (data, result) {
		var result = JSON.parse(data);
		//console.log(result);
		sess.cust_name = result.full_name;
		sess.CIF = result.cif;
		sess.step_1_email_verify = result.emailID;
		sess.step_1_mobile_verify = result.registered_mobile;
		sess.branch_code = result.BOO;
		sess.branch_name = result.branch_name;
		sess.account_no = result.account_num;
		sess.cust_prefix = result.prefix;
		appendCustomerDetails();
		mobile = sess.step_1_mobile_verify;
		email = sess.step_1_email_verify;

	});
}


function appendCustomerDetails() {
	$("#label_cust_name").html(sess.cust_prefix + " " + sess.cust_name);
	$("#input_account_name").val(sess.cust_name);
	$("#input_customer_id").val(sess.CIF);
	$("#input_account_no").val(sess.account_no);
	$("#input_branch").val(sess.branch_code + '-' + sess.branch_name);
	$("#label_ifsc_code").val("FSFB0000001");
}

var orderId = '';
var payment_id = '';
var request_id = '';

function fund_amount() {
	
	var acc = sess.account_no;
	var amount=$('#input_fund_amount').val();
	
	 if(amount<=90000 && amount>=1000){
		 $.post('https://101.fincarebank.com/101/api.php', {
		request_for: 'fund',
		acc: acc,
		amount: amount,
		}, function (data, result) {
			console.log(data);
			orderId = JSON.parse(data).order_id;
			request_id = JSON.parse(data).request_id;
			funding();
		});

	}	
    else{
		functionAlert("Minimum amount you can fund is  ₹ 1,000 and Maximum amount is ₹ 90,000.");
	}

}

function funding() {
	var amt =$("#input_fund_amount").val();

	var options = {
		"key": "rzp_live_GfGD4An1CauIgU",
		"amount": amt,
		"currency": "INR",
		"name": "Fincare",
		"description": "Small Finance Bank",
		"image": "images/Logo.png",
		"order_id": orderId,
		"callback_url": 'https://101.fincarebank.com/101/index.html',
		"redirect": false,
		"handler": function (response) {
			payment_id = response.razorpay_payment_id;
console.log("funding");
			console.log(response);
			//razor_pay();
		},
		"prefill": {
			"name": sess.cust_name,
			"email": sess.step_1_email_verify,
			"contact": sess.step_1_mobile_verify
		},
		"notes": {
			"address": "note value"
		},
		"theme": {
			"color": "#F37254"
		}
	};
	var rzp1 = new Razorpay(options);

	rzp1.open();
	//e.preventDefault();
}

function functionAlert(msg, myYes) {
	var confirmBox = $(".backalert");
	//confirmBox.find(".message").text(msg);
	$('#alert-msg').html(msg);
	confirmBox.find(".yes").unbind().click(function () {
		confirmBox.hide();
	});
	confirmBox.find(".yes").click(myYes);
	confirmBox.show();
}


function vkycsms(){
   var mob = '9538953404';
	
	$.post('https://101.fincarebank.com/101/kyc_skip.php', {
		request_for: 'smssend',
		mobile: mob
		
	}, function(data, result) {
		console.log(data);
	});
}

function vkycmail(){
	var email = 'sowmya.d@fincarebank.com';
	
	$.post('https://101.fincarebank.com/101/kyc_skip.php', {
		request_for: 'mailsend',
		emailId: email
		
	}, function(data, result) {
		console.log(data);
	});
}