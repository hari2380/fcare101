﻿var ReferenceNumber = '';
var aadhaarverifytxn = '';
var amlrefid="";
var amlst="";
var cifaadhar="";
var session = "";
//sess.images = [];
var currnt_qtn = "";
var session_vals = "";
var fields = {};
var main_name = "";
//vkyc
 var domainurl = 'http://localhost/101_php/101_uat/public';
 var domainpapiurl = domainurl + '/api.php';//vkyc

 alert($('meta[name="csrf-token"]').attr('content'));
          $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

$(function(){
	sess.images = [];
	 sess.linkArray = [];//vkyc
	console.log(sess);
});
function xmlToJson(xml) {
	// Create the return object
	var obj = {};
	if(xml.nodeType == 1) { // element
		// do attributes
		if(xml.attributes.length > 0) {
			obj["@attributes"] = {};
			for(var j = 0; j < xml.attributes.length; j++) {
				var attribute = xml.attributes.item(j);
				obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
			}
		}
	} else if(xml.nodeType == 3) { // text
		obj = xml.nodeValue;
	}
	// do children
	// If just one text node inside
	if(xml.hasChildNodes() && xml.childNodes.length === 1 && xml.childNodes[0].nodeType === 3) {
		obj = xml.childNodes[0].nodeValue;
	} else if(xml.hasChildNodes()) {
		for(var i = 0; i < xml.childNodes.length; i++) {
			var item = xml.childNodes.item(i);
			var nodeName = item.nodeName;
			if(typeof(obj[nodeName]) == "undefined") {
				obj[nodeName] = xmlToJson(item);
			} else {
				if(typeof(obj[nodeName].push) == "undefined") {
					var old = obj[nodeName];
					obj[nodeName] = [];
					obj[nodeName].push(old);
				}
				obj[nodeName].push(xmlToJson(item));
			}
		}
	}
	return obj;
}

function replaceAll(str, find, replace) {
	return str.replace(new RegExp(escapeRegExp(find), 'g'), replace);
}

function escapeRegExp(str) {
	return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}
//send sms to mobile no
function send_otp(key, fid, channel = "") {
	$('#wait').show();
	var obj = document.getElementById(fid);
	classes = obj.parentNode;
	currentdiv = classes;
	nextc = classes.nextElementSibling;
	nextdiv = nextc;
	console.log(nextc);
	
	
	if(key == "mobile") {
		mobile = document.getElementById(fid).value;
		
		if(mobile.length == 10 && mobile.match(/^(?!(\d)\1{9})[6-9]\d{9}$/)) {
			submitdistimer("step_1_number_mobile_verify","btn-verify-inac custom-labeltxt","btn-verify",10);
			request_for = 'dedupedata';
			//alert($('meta[name="csrf-token"]').attr('content'));
			$.ajax ({
      url: "apistore/dedupedata",
      type: "POST",
      data: {
        //request_for: request_for,
        mobile: mobile,
       
        },
      
      success: function(data,result){
	        
				   console.log(data);
                   if(data=="failed"){
                   	$.ajax ({
      url: "api/sendotp",
      type: "POST",
      data: {
        //request_for: request_for,
        mobile: mobile,
		session : session,
		
		},
	
      success: function(data,result){
                   	  //alert(data);
							var resp = JSON.parse(data);
							var res=resp['res'];
							$('#wait').hide();
							if( resp.status == "Success" && resp.stage == "MobileOTP") {
								ReferenceNumber = res['ReferenceNumber'];
								var dt = "mobile@@" + mobile + "@@sendMobileOtp('" + mobile + "')@@" + ReferenceNumber;
								$('#otp_model_hidden').html(dt);
								$('#otp_sent_to').html(mobile);
								$("#otp_modal_div").attr("style", "display:block");
								$("#otp_modal_div").modal({
									escapeClose: false,
									clickClose: false,
									showClose: false
								});
							} 
							else if(resp.status == "Failed" && resp.error_msg == "Reached Maximum attempt"){
								functionAlert("You have reached maximum attemp. Please try after some time");
								window.location.reload();
							}else {
								$('#wait').hide();
								console.log('Invalid Mobile Number');
								submitdistimer("step_1_number_mobile_verify","btn-verify-inac custom-labeltxt","btn-verify",3);
							}
						}
					});
                   }
                   else{
                   	
						  $('#wait').hide();
                   	  	$("#mobilededupe_modal_div").removeClass("hide");
						$("#mobilededupe_modal_div").addClass("modal");
						$("#mobilededupe_modal_div").attr("style","display:block");
						$("#mobilededupe_modal_div").modal({
									  escapeClose: false,
									  clickClose: false,
									  showClose: false
									});
                   }
				}
			});
		} 
		else {
			functionAlert("Please Enter Valid Mobile Number");
		}
	}
	if(key == "aadhaar") {
		aadhaar = document.getElementById(fid).value;
		if(aadhaar != '') {
			send_addhaar_otp("aadhaar", a_f_id, ch);
		} else {
			functionAlert("Invalid Aadhaar. Please Check");
		}
	}
	$('#wait').hide();
}
// aadhaar

function aadharVault() {
	//$('#wait').show();
	aadhar = $("#aadharverify_number_aadhar").val();
	
	request_for = 'aadharValut';
	$.post('http://localhost/101_php/101_uat/public/api.php', {
		request_for: request_for,
		aadharnumber: aadhar
	}, function (data, result) {
		console.log(data);
		var res = data;
		res = JSON.parse(res);

		if (res.RESPONSE.STATUS == '1') {
			sess['aadhaarRefence'] = res.RESPONSE.UUID;
			generate_aadhaar_otp('aadharverify_number_aadhar', 'aadharverify_radio_otp_recieve');
			cifaadhar = $("#aadharverify_number_aadhar").val();
			/*generate_aadhaardedupe_otp('aadharverify_number_aadhar', 'aadharverify_radio_otp_recieve');
			cifaadhar = $("#aadharverify_number_aadhar").val();*/
		} 
		else if (res.RESPONSE.STATUS == '0') {
			generate_aadhaardedupe_otp('aadharverify_number_aadhar', 'aadharverify_radio_otp_recieve');
			cifaadhar = $("#aadharverify_number_aadhar").val();
		}
		else {
			functionAlert("Failed, Try Again");
		}
	});
}

function generate_aadhaar_otp(aadhar_field_id, channel_field_name) {
	$('#wait').show();
      sessUpdate("dedupe_check","Clicked" );
	
	a_f_id = aadhar_field_id;
	c_f_name = channel_field_name;
	if (verify_number_aadhar(document.getElementById(aadhar_field_id))) {
		//aadhaar = sess.aadhaarRefence;
		aadhaar = $("#aadharverify_number_aadhar").val();
		var ch = $("input[name='" + channel_field_name + "']:checked").val();
		if (ch == undefined || ch.length == 0) {
			$('#wait').hide();
			functionAlert("Please Select Channel of Aadhaar OTP");
			return false;
		}
		if (aadhaar.length == 16) {
			send_otp("aadhaar", aadhar_field_id, ch);
		} else if (aadhaar.length == 12) {
			request_for = 'aadhaavaultDedupe';
			$.post('http://localhost/101_php/101_uat/public/api.php', {
				request_for: request_for,
				aadhaarNo: aadhaar
			}, function (data, result) {
				console.log(data);
				var resp = JSON.parse(data);
				if (resp.Resp.status == 'success') {
					$('#wait').hide();
					$("#dedupe_modal_div").removeClass("hide");
						$("#dedupe_modal_div").addClass("modal");
						$("#dedupe_modal_div").attr("style","display:block");
						$("#dedupe_modal_div").modal({
									  escapeClose: false,
									  clickClose: false,
									  showClose: false
									});
						sessUpdate("dedupe_check","Old Customer" ); 
				} 
				else if (resp.Resp.status == 'failed') {
					$('#wait').hide();
					var ch = $("input[name='" + c_f_name + "']:checked").val();
					send_addhaar_otp("aadhaar", a_f_id, ch);
					sessUpdate("dedupe_check", "New Customer");

				}

			});
		}
	}
	$('#wait').hide();
}

function send_addhaar_otp(key, fid, channel = "") {
	if(key == "aadhaar") {
		request_for = 'aadhaarotp';
		aadhaar = document.getElementById(fid).value;
		var radioValue = $("input[name='aadharverify_radio_otp_recieve']:checked").val();
		//aadhaar = sess.aadharRef;
		$.post('http://localhost/101_php/101_uat/public/api.php', {
			request_for: request_for,
			aadhaarNo: aadhaar,
			channel: channel
		}, function(data, result) {
			$('#wait').hide();
			var txnid = JSON.parse(data).txnid;
			console.log(txnid);
			send_data('Aadhaar OTP Send API called','Syntizen_OTP');
			var resp=JSON.parse(data);
			var a=JSON.parse(data).OTPSendTo;
			var b=a.split(",");
			var sendto=b[0];
			if(radioValue == '2'){
				 sendto=b[1];
			}
			
       if(result == 'success') {
             if(JSON.parse(data).result == 'y'){
					console.log('success');
					var dt = "aadhaar@@" + aadhaar + "@@generate_aadhaar_otp('" + a_f_id + "','" + c_f_name + "')@@" + txnid;
					$('#otp_model_hidden').html(dt);
					$('#otp_sent_to').html(sendto);
					$("#otp_modal_div").attr("style", "display:block");
					$("#otp_modal_div").modal({
						escapeClose: false,
						clickClose: false,
						showClose: false
					});
					$("#aadharverify_button_verify").removeClass("btn-verify");
					$("#aadharverify_button_verify").addClass("btn-vfd");
					$("#aadharverify_button_verify").html("Verified");
					document.getElementById("aadharverify_button_verify").disabled = true;
					$("#aadharverify_number_aadhar").attr("readOnly", true);
					document.getElementById("button_continue_aadhaar").className = "button-ac";
					$('.aadhaarverifytxn').val(txnid);
					$('.aadhaarno').val(aadhaar);
					sessUpdate("aadhaar_vfd_result","Yes");
			 }	
			 else if(JSON.parse(data).result == 'n') {
			 	if(JSON.parse(data).errMsg == 'Aadhaar number does not have verified mobile' || JSON.parse(data).errMsg == 'Aadhaar number does not have verified mobile/email' || JSON.parse(data).errCode=='111'){
			 		$("#aadharotp_modal_div").removeClass("hide");
						$("#aadharotp_modal_div").addClass("modal");
						$("#aadharotp_modal_div").attr("style","display:block");
						$("#aadharotp_modal_div").modal({
									  escapeClose: false,
									  clickClose: false,
									  showClose: false
									});
			 	}
			 	else if(JSON.parse(data).errMsg=='OTP Flooding error' || JSON.parse(data).errCode=='952'){
			 		$("#otpflood_modal_div").removeClass("hide");
						$("#otpflood_modal_div").addClass("modal");
						$("#otpflood_modal_div").attr("style","display:block");
						$("#otpflood_modal_div").modal({
									  escapeClose: false,
									  clickClose: false,
									  showClose: false
									});
			 	}
			 	else{
			 		$("#otpflood_modal_div").removeClass("hide");
						$("#otpflood_modal_div").addClass("modal");
						$("#otpflood_modal_div").attr("style","display:block");
						$("#otpflood_modal_div").modal({
									  escapeClose: false,
									  clickClose: false,
									  showClose: false
									});
			 	}
				sessUpdate("aadhaar_vfd_result","NO"+JSON.parse(data).errMsg);
			}
			else{
			    functionAlert("Failed to Receive the Response from Aadhaar Server. Please try after some time");
				sessUpdate("aadhaar_vfd_result",result+"NO Response From Server");
			return false;
			}
		} 
		else{
			functionAlert("Failed to Receive the Response from Aadhaar Server. Please try after some time");
			sessUpdate("aadhaar_vfd_result",result+"NO Response From Server");
			return false;
		}
	 });
		dd_comm_add();
	}
	$('#wait').hide();
}

function stringToXml(str) {
	if(window.DOMParser) {
		var parser = new DOMParser();
		return parser.parseFromString(str, "text/xml");
	} else // Internet Explorer
	{
		var xmlDoc = new ActiveXObject("Microsoft.XMLDOM");
		xmlDoc.async = false;
		xmlDoc.loadXML(str);
		return xmlDoc;
	}
}
var sta = '';

function dd_comm_add()
{
	$("#comm_add_state,#input_address_proof_type").select2();
}

function otp_send() {
				//document.getElementById("button_otp_send").disabled = true;
	var strval=$("#otp_model_hidden").html();
	var strarr=strval.split("@@");
	var key=strarr[0];
	var mobotp=strotpapnd.value;
	var resendfun=strarr[2];
	var refno=strarr[3];
	
	if(key=="mobile")
	{     
		if(strotpapnd=="" || strotpapnd.length!=6){
          functionAlert('Please Enter the OTP');
		  return false;
	    }
		verifyMobileOtp(strotpapnd,refno);
		alert(refno);
     }
	else if(key=="aadhaar")
	{
		if(strotpapnd=="" || strotpapnd.length!=6){
          functionAlert('Please Enter the OTP');
		  return false;
	    }
		ekyc_otp(strotpapnd,refno);
	}
}

function verifyMobileOtp(otp,refnum) {
	 	var request_for = 'verifyotp';
		submitdistimer("button_otp_send","button-popupinac","button-popup",10);
		$.ajax ({
      url: "api/verifyotp",
      type: "POST",
      data: {
        //request_for: request_for,
        otp: otp,
		refno: refnum,
		session : session
        },
      
      success: function(data,result){

		
			if(JSON.parse(data).status != 'failed') {
				// $("#otp_modal_div").modal('close');
				
				var numbr = document.getElementById("step_1_mobile_verify").value;
				Moengage.add_unique_user_id(numbr);
				var prefix = "91";
				var number = prefix.concat(numbr);
				Moengage.add_mobile(number);  
				
				Moengage.track_event("101_OTP_Validation_Completed", {
				 "Mobile Number": number
				});	
				
				$.modal.close();
				$("#step_1_number_mobile_verify").removeClass("btn-verify");
				$("#step_1_number_mobile_verify").addClass("btn-vfd");
				$("#step_1_number_mobile_verify").html("Verified");
				document.getElementById("step_1_number_mobile_verify").disabled = true;
				$("#step_1_mobile_verify").attr("readOnly", true);
				$('#27__Step1__email_verify').addClass('forms-ac');
				$('#27__Step1__email_verify').removeClass('forms-inac');
				// $(".ReferenceNumber").val('');
				submitdistimer("button_otp_send","button-popupinac","button-popup",2);
				clean_modalvalue();
				ReferenceNumber = '';
				redirection() ;
			} else {
				functionAlert('OTP Mismatch. Please try again.');
				$.modal.close();
				$('#step_1_mobile_verify').val('');
				document.getElementById("step_1_number_mobile_verify").disabled = true;
				$('#step_1_number_mobile_verify').addClass("custom-labeltxt");
				$("#step_1_number_mobile_verify").addClass("btn-verify-inac");
				$("#step_1_number_mobile_verify").removeClass("btn-verify");

			}
		}
	});
		$(".select2-container").addClass("off");
}

function ekyc_otp(otp,txnid){
	
	  var newtxn=txnid;
	console.log(txnid);
	if(newtxn== '' || newtxn.length <= '3'){
		var strval=$("#otp_model_hidden").html();
		var strarr=strval.split("@@");
		var key=strarr[0];
		var mobotp=strotpapnd.value;
		var resendfun=strarr[2];
		var refno=strarr[3];
		newtxn=refno;
	}
	
	request_for = 'aadhaarotpverify';
		aadhar = $('.aadhaarno').val();
		submitdistimer("button_otp_send","button-popupinac","button-popup",40);
		$.ajax ({
      url: "api/aadhaarotpverify",
      type: "POST",
      data: {
        otp: otp,
		aadhaarNo: aadhar,
		txn: newtxn
        },
      success: function(data,result){
		
			data = data;
			send_data('Aadhaar OTP verification API','Syntizen_OTP_Verify');
			console.log(data);
			if(JSON.parse(data).status != 'failed') {
				$("#aadharverify_button_verify").removeClass("btn-verify");
				$("#aadharverify_button_verify").addClass("btn-vfd");
				$("#aadharverify_button_verify").html("Verified");
				//document.getElementById("aadharverify_button_verify").disabled = true;
				$("#aadharverify_number_aadhar").attr("readOnly", true);
				document.getElementById("button_continue_aadhaar").className = "button-ac";
				var json = JSON.stringify(data);
				//var aad_str = wordWrap(json, 10);
				var canvas = document.createElement("CANVAS");
				var ctx = canvas.getContext("2d");
				wrapText(ctx, json, 10, 10, 150, 12);

				function wrapText(context, text, x, y, maxWidth, fontSize, fontFace) {
					var words = text.split(' ');
					var line = '';
					var lineHeight = fontSize + 2;
					context.font = fontSize + " " + fontFace;
					for(var n = 0; n < words.length; n++) {
						var testLine = line + words[n] + ' ';
						var metrics = context.measureText(testLine);
						var testWidth = metrics.width;
						if(testWidth > maxWidth) {
							context.fillText(line, x, y);
							line = words[n] + ' ';
							y += lineHeight;
						} else {
							line = testLine;
						}
					}
					context.fillText(line, 10, 10);
				}
				data = JSON.parse(data);
				console.log(data);
				sess.images.aadhaar_front = canvas.toDataURL();
				if(data.SynKycRes != undefined)
				{
				if(data.SynKycRes[0].ret == "Y") {
					ekyc_resp = data;
					sess['ekyc_resp'] = data.SynKycRes[0];
					var result = data.SynKycRes[0];
					$("#cust_name").val(data.SynKycRes[0].Poi.name);
					$("#cust_relation_type").val();
					$("#cust_relation_name").val((result.Poa.co != "NA") ? (result.Poa.co) : "");
					$("#cust_dob").val(data.SynKycRes[0].Poi.dob);
					$("#cust_gender").val(data.SynKycRes[0].Poi.gender);
					sess["oin"] = result.code;
					//sess["aadhaar"] = result.UidData.uid;
					sess["aadharRef"] = result.uuid;
					var kycstr = result.Poi.name.split(" ");
					var vidaadhaar=result.UidData.uid;
					if($("#cust_gender").val() == "M") {
						$('#cust_prefix').val("MR.");
					}
					if($("#cust_gender").val() == "F") {
						$('#cust_prefix').val("MS.");
					}
					if(kycstr.length == 1) {
						sess['first_name'] = '';
						sess["last_name"] = kycstr[0];
						sess["middle_name"] = '';
					}
					if(kycstr.length == 2) {
						sess['first_name'] = kycstr[0];
						sess["last_name"] = kycstr[1];
						sess["middle_name"] = '';
					}
					if(kycstr.length == 3) {
						sess['first_name'] = kycstr[0];
						sess["middle_name"] = kycstr[1];
						sess["last_name"] = kycstr[2];
					}
					if(kycstr.length == 4) {
						sess['first_name'] = kycstr[0];
						sess["middle_name"] = kycstr[1];
						sess["last_name"] = kycstr[2] + " " + kycstr[3];
					}
					if(kycstr.length == 5) {
						sess['first_name'] = kycstr[0];
						sess["middle_name"] = kycstr[1];
						sess["last_name"] = kycstr[2] + " " + kycstr[3]+ " " + kycstr[4];
					}
					if(kycstr.length == 6) {
						sess['first_name'] = kycstr[0];
						sess["middle_name"] = kycstr[1];
						sess["last_name"] = kycstr[2] + " " + kycstr[3]+ " " + kycstr[4]+ " " + kycstr[5];
					}
					
					var aadfulladdr = ((result.Poa.house != "NA") ? (result.Poa.house) + " " : "") + ((result.Poa.street != "NA") ? (result.Poa.street) + " " : "") + ((result.Poa.lm != "NA") ? (result.Poa.lm) + " " : "") + ((result.Poa.loc != "NA") ? (result.Poa.loc) + " " : "") + ((result.Poa.vtc != "NA") ? (result.Poa.vtc) + " " : "") + ((result.Poa.subdist != "NA") ? (result.Poa.subdist) + " " : "") + ((result.Poa.dist != "NA") ? (result.Poa.dist) + " " : "") + ((result.Poa.po != "NA") ? (result.Poa.po) + ", " : "");
					var splittedAddr = splitAadhaarAddress(aadfulladdr, 39);
					$("#permenant_add1").val(lines[0]);
					$("#permenant_add2").val(lines[1]);
					$("#permenant_add3").val(lines[2]);
					$("#permenant_pincode").val((result.Poa.pc != "NA") ? (result.Poa.pc) : "");
					$("#permenant_city").val((result.Poa.dist != "NA") ? (result.Poa.dist) : "");
					sta = result.Poa.state;
					$("#permenant_state option:contains(" + sta + ")").attr('selected', 'selected');
					$("#aadhaarEkyc").attr('src', "data:image/jpg;base64," + result.Pht)
					if(($("#aadharverify_number_aadhar").val()).length == 16) {
											request_for = 'aadhaarverify';
							//submitdistimer("aadharverify_button_verify","btn-verify-inac custom-labeltxt","btn-verify",40);
							$.ajax ({
						      url: "api/aadhaarverify",
						      type: "POST",
						      data: {
						        aadhaarNo: vidaadhaar
						        },
						      success: function(data,result){
							
								if(result == 'success') {
									var r = data.replace(/\r?\n|\r/g, ' ').replace(/ns1\:/g, '').replace(/ns2\:/g, '');
									r = stringToXml(xmlToJson(stringToXml(r)).Responses.Response.Representations.Representation.Element.replace('&lt;?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?&gt;', '').replace(/\&amp\;/g, '&').replace(/\&lt\;/g, '<').replace(/\&gt\;/g, '>'));
									r = xmlToJson(r).SQL_CFG_1.dedup.final_output.CustomerSearch.Aadhaar;
									if(r.Exception && r.Exception.Response == "99") {
										if(($("#aadharverify_number_aadhar").val()).length == 12) {
											 sessUpdate("aadhaar_vfd","Aadhaar Verified");
									$("#aadhaar_info_display_div").show();
									$("#28__Step1__aadhaar_verify").hide();
									$("#button_continue_aadhaar").hide();
									//sess['permenant_state']=$('#permenant_state').val();
									$.modal.close();
									StoreSession();
										}
									} else {
										$("#dedupe_modal_div").removeClass("hide");
										$("#dedupe_modal_div").addClass("modal");
										$("#dedupe_modal_div").attr("style","display:block");
										$("#dedupe_modal_div").modal({
													  escapeClose: false,
													  clickClose: false,
													  showClose: false
													});
										sessUpdate("dedupe_check","Old Customer" );
										
									}
								} else {
									functionAlert('Please Enter Valid Aadhaar Number');
									sessUpdate("dedupe_check",result);
									submitdistimer("aadharverify_button_verify","btn-verify-inac custom-labeltxt","btn-verify",3);
								}
							}
						});
					}
					 sessUpdate("aadhaar_vfd","Aadhaar Verified");
					$("#aadhaar_info_display_div").show();
					$("#28__Step1__aadhaar_verify").hide();
					$("#button_continue_aadhaar").hide();
					//sess['permenant_state']=$('#permenant_state').val();
					 sess.aadharverify_number_aadhar_mask = sess.aadharRef;
	                 sess.aadharverify_number_aadhar = sess.aadharRef;
					 sessUpdate("aadharverify_number_aadhar",sess.aadharRef);
					$.modal.close();
					StoreSession();
				} 
				else if(data.SynKycRes[0].ret == "n"){
					submitdistimer("button_otp_send","button-popupinac","button-popup",4);
				     if(data.SynKycRes[0].reason == "OTP validation failed"){
					    functionAlert("You have entered Invalid OTP. Please check and re-enter");
						$("button[id='btn-resend']").attr("disabled", true);
						$('#btn-resend').removeClass('btn-resend');
						$('#btn-resend').addClass('otp-no');
						$("button[id='button_otp_send']").attr("disabled", false);
						$('#button_otp_send').removeClass('btn-resend');
						$('#button_otp_send').addClass('otp-no');	
					 }
					 else if(data.SynKycRes[0].reason == "Retry attempt exceeded. Generate new OTP."){
						 submitdistimer("button_otp_send","button-popupinac","button-popup",4);
					    functionAlert("Retry attempt exceeded. Please click on Resend Button to get new OTP.");
						$("button[id='btn-resend']").attr("disabled", false);
						$('#btn-resend').removeClass('otp-no');
						$('#btn-resend').addClass('btn-resend');	
					 }
					 else if(data.SynKycRes[0].reason == "Invalid PID XML format"){
						 submitdistimer("button_otp_send","button-popupinac","button-popup",4);
					   	functionAlert("You have entered Invalid OTP. Please check and re-enter");
						$("button[id='btn-resend']").attr("disabled", true);
						$('#btn-resend').removeClass('btn-resend');
						$('#btn-resend').addClass('otp-no');
						$("button[id='button_otp_send']").attr("disabled", false);
						$('#button_otp_send').removeClass('btn-resend');
						$('#button_otp_send').addClass('otp-no');
				    }
					else if(data.SynKycRes[0].reason == "The operation has timed out"){
						
					    functionAlert("Validation Failed.Please click on Resend Button to get new OTP.");
						$("button[id='btn-resend']").attr("disabled", false);
						$('#btn-resend').removeClass('otp-no');
						$('#btn-resend').addClass('btn-resend');	
					 }
					 sessUpdate("aadhaar_vfd",data.SynKycRes[0].reason);   				
				}
				else {
					 sessUpdate("aadhaar_vfd","Aadhaar Not Verified");
					 submitdistimer("button_otp_send","button-popupinac","button-popup",3);
					functionAlert("Unable to fetch details from UIDAI");
					document.getElementById("button_otp_send").disabled = false;
					$("button[id='btn-resend']").attr("disabled", false);
					$('#btn-resend').removeClass('otp-no');
					$('#btn-resend').addClass('btn-resend');
					var items = document.getElementsByName('model_otp');
					for(var i = 0; i < items.length; i++) items[i].value = '';
					return false;
				}
			}
			} else {
				 sessUpdate("aadhaar_vfd","Aadhaar Not Verified");
				functionAlert("Unable to fetch details from UIDAI. Try again.");
				submitdistimer("button_otp_send","button-popupinac","button-popup",3);
				document.getElementById("button_otp_send").disabled = false;
				$("button[id='btn-resend']").attr("disabled", false);
				$('#btn-resend').removeClass('otp-no');
				$('#btn-resend').addClass('btn-resend');
				var items = document.getElementsByName('model_otp');
				for(var i = 0; i < items.length; i++) items[i].value = '';
				return false;
			}
		}
	});
	$(".select2-container").addClass("off");	
}

function email_check() {
	//$('#wait').show();
	submitdistimer("button_email_verify","button-inac","button-ac",10);
	
	verify_pan();
	/*if(amlst=="fail" || amlst=="")
	{
		callAML();
	}*/
	
	/*
	var email_ver = $("#step_1_email_verify").val();
	var pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	if(email_ver.length < 1) {
		$('#wait').hide();
		submitdistimer("button_email_verify","button-ac","button-inac",3);
		functionAlert("Please Enter Email ID");
		$("#28__Step1__aadhaar_verify").hide();
		$("#27__Step1__email_verify").show();
		$("#button_email_verify").show();
		$("#aadhaar_info_display_div").hide();
		$("button[id='button_email_verify']").attr("disabled", false);
		$("#button_email_verify").removeClass('hide');
		$("#button_email_verify").addClass('button-ac');
		return false;
	} else if(!email_ver.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)) {
		$('#wait').hide();
		submitdistimer("button_email_verify","button-ac","button-inac",3);
		functionAlert("Please Enter Valid Email ID");
		$("#28__Step1__aadhaar_verify").hide();
		$("#27__Step1__email_verify").show();
		$("#button_email_verify").show();
		$("#aadhaar_info_display_div").hide();
		$("button[id='button_email_verify']").attr("disabled", false);
		$("#button_email_verify").removeClass('hide');
		$("#button_email_verify").addClass('button-ac');
		return false;
	} else if(email_ver.match(pattern)) {
		request_for = 'emailverify';
		email = document.getElementById("step_1_email_verify").value;
		$.post('http://localhost/101_php/101_uat/public/api.php', {
			request_for: request_for,
			email: email
		}, function(data, result) {
			//sessUpdate("KARZA_EMAIL","Done" );
			
			send_data('step_1_email_verify','KARZA_Email');
			var email_suc = JSON.parse(data);
			if(email_suc['status-code'] == "101") {
				sessUpdate("KARZA_EMAIL","Done" );
				if(email_suc.result.data['disposable'] == false && email_suc.result.data['mx_records'] == true && email_suc.result.data['regexp'] == true || email_suc.result.data['regexp'] == "accept-all" && email_suc.result.data['result'] == true && email_suc.result.data['smtp_check'] == true) {
					console.log("Valid Email");
					sessUpdate("KARZA_EMAIL","Valid" );
					$('#wait').hide();
				} else {
					$('#wait').hide();
					alert("Incorrect Email ID. Please Check and Reenter");
					submitdistimer("button_email_verify","button-ac","button-inac",3);
					$("#28__Step1__aadhaar_verify").hide();
					$("#27__Step1__email_verify").show();
					$("#button_email_verify").show();
					$("#aadhaar_info_display_div").hide();
					$("button[id='button_email_verify']").attr("disabled", false);
					$("#button_email_verify").removeClass('hide');
					$("#button_email_verify").addClass('button-ac');
				}
			} else {
				$('#wait').hide();
				alert('Please put valid email');
				submitdistimer("button_email_verify","button-ac","button-inac",3);
				$('#step_1_email_verify').val('');
				$("#28__Step1__aadhaar_verify").hide();
					$("#27__Step1__email_verify").show();
					$("#button_email_verify").show();
					$("#aadhaar_info_display_div").hide();
					$("button[id='button_email_verify']").attr("disabled", false);
					$("#button_email_verify").removeClass('hide');
					$("#button_email_verify").addClass('button-ac');
			}
		});
	} else {
		$('#wait').hide();
		submitdistimer("button_email_verify","button-ac","button-inac",3);
		functionAlert("Incorrect Email ID. Please Check and Reenter");
		$("#28__Step1__aadhaar_verify").hide();
		$("#27__Step1__email_verify").show();
		$("#button_email_verify").show();
		$("#aadhaar_info_display_div").hide();
		$("button[id='button_email_verify']").attr("disabled", false);
		$("#button_email_verify").removeClass('hide');
		$("#button_email_verify").addClass('button-ac');
		return false;
	}
	*/
		
	//sessUpdate("KARZA_EMAIL","Done" );
	sessUpdate("KARZA_EMAIL","Valid" );
	//$('#wait').hide();
}

function verify_pan() {
	$('#wait').show();
  submitdistimer("button_pan_verify","button-inac","button-ac",3);
	
	var pan_ver = $("#step_1_pan_verify").val();
	var pattern = /^([a-zA-Z]{3})([aA]|[bB]|[cC]|[eE]|[fF]|[gG]|[hH]|[lL]|[jJ]|[pP]|[tT]{1})([a-zA-Z]{1})(\d{4})([a-zA-Z]{1})/;
	if(pan_ver.length < 1) {
	submitdistimer("button_pan_verify","button-ac","button-inac",3);
		$('#wait').hide();
		functionAlert("Please Enter PAN Number");
		$("#28__Step1__aadhaar_verify").hide();
		$("#31__Step1__pan_verify").show();
		$("#aadhaar_info_display_div").hide();
		$('#27__Step1__email_verify').hide();
		$("#button_pan_verify").show();
		$("#27__Step1__email_verify").attr("style", "display:none");
		$("button[id='button_pan_verify']").attr("disabled", false);
		$("#button_pan_verify").removeClass('hide');
		$("#button_pan_verify").addClass('button-ac');
		return false;
	} else if(pan_ver.length != 10) {
	submitdistimer("button_pan_verify","button-ac","button-inac",3);
		$('#wait').hide();
		functionAlert("Please Enter Valid PAN number");
		$("#28__Step1__aadhaar_verify").hide();
		$("#31__Step1__pan_verify").show();
		$("#aadhaar_info_display_div").hide();
		$('#27__Step1__email_verify').hide();
		$("#27__Step1__email_verify").attr("style", "display:none");
		$("#button_pan_verify").show();
		$("button[id='button_pan_verify']").attr("disabled", false);
		$("#button_pan_verify").removeClass('hide');
		$("#button_pan_verify").addClass('button-ac');
		return false;
	} else if(pan_ver.match(pattern)) {
		request_for = 'panverify';
		var pan = document.getElementById("step_1_pan_verify").value;
		$.ajax ({
      url: "api/panverify",
      type: "POST",
      data: {
        pan: pan
        },
      success: function(data,result){
		
			resp = JSON.parse(data);
			send_data('step_1_pan_verify','KARZA_Pan');
			console.log(resp);
			if(resp['status'] == "504"){
				verify_pan();
			}
			else{
				if(resp['status-code'] == "101") {
					sessUpdate("KARZA_PAN","Done" );
					$('#31__Step1__pan_verify').css('display','none');
					panName = resp.result.name;
					name_check();
					
				}
				//else if(resp['status-code'] == "102") {
					//functionAlert("Please Try After Some Time.");
					//$('#wait').hide();
					//window.location.reload();
				//}
				else {
					$('#wait').hide();
					submitdistimer("button_pan_verify","button-ac","button-inac",3);
					functionAlert("Please check and Reenter your PAN number");
					$("#28__Step1__aadhaar_verify").hide();
					$("#31__Step1__pan_verify").show();
					$("#button_pan_verify").show();
					$("#aadhaar_info_display_div").hide();
					$("#27__Step1__email_verify").attr("style", "display:none");
					$('#27__Step1__email_verify').hide();
					$("button[id='button_pan_verify']").attr("disabled", false);
					$("#button_pan_verify").removeClass('hide');
					$("#button_pan_verify").addClass('button-ac');
					$('#button_continue_aadhaar').removeClass('button-inac');
					$("#button_pan_verify").addClass('button-ac');
					$('#button_continue_aadhaar').addClass('hide');
					$("#button_pan_verify").removeClass('button-inac');
				}
			}
		}
	});
	} else {
		$('#wait').hide();
	   submitdistimer("button_pan_verify","button-ac","button-inac",3);
		stopLoader();
		functionAlert("Incorrect PAN. Please Check and Reenter");
		$("#28__Step1__aadhaar_verify").hide();
		$("#31__Step1__pan_verify").show();
		$("#aadhaar_info_display_div").hide();
		$('#27__Step1__email_verify').hide();
		$("#27__Step1__email_verify").attr("style", "display:none");
		$("#button_pan_verify").show();
		$("button[id='button_pan_verify']").attr("disabled", false);
		$("#button_pan_verify").removeClass('hide');
		$("#button_pan_verify").addClass('button-ac');
		return false;
	}
	
}

function fill_comm_addr() {
	//$(':input').removeAttr('placeholder');
	$("#comm_add_add1").val(sess.permenant_add1);
	$("#comm_add_add2").val(sess.permenant_add2);
	$("#comm_add_add3").val(sess.permenant_add3);
	$("#comm_add_city").val(sess.permenant_city);
	$("#select2-comm_add_state-container").text(sta);
	$(".select2-container").addClass("off");
	$("#comm_add_pincode").val(sess.permenant_pincode);
	$('input:radio[name="rd-same_as_comm_address_yn"]').filter('[value="Same_As_Permenant_Address"]').attr('checked', true);
}

//Name check API
var scr = 0.0;
var pan_img_req = false;

function name_check() {
	$('#wait').show();
	request_for = "namecheck";
	name1 = $('#cust_name').val();
	name2 = panName;
	if(name1.length < 1 || name1.length < 1) {
		$('#wait').hide();
		functionAlert("Invalid PAN. Please Try Again");
		window.location.reload();
		//verify_pan();
		return false;
	}
	else{
	$.ajax ({
      url: "api/namecheck",
      type: "POST",
      data: {
        name1: name1,
		name2: name2
        },
      success: function(data,result){
	
		  $('#wait').hide();
		resp = JSON.parse(data);
		send_data('cust_name','KARZA_Name_Check');
		if(resp.statusCode == "101") {
			$('#wait').hide();
			sessUpdate("KARZA_NAMECHECK","Done" );
			scr = parseFloat(resp.result.score);
			console.log(scr);
			var cus_name = $("#cust_name").val();
			//sessUpdate("Name_As_Per_Aadhaar",cus_name);
			sessUpdate("Name_As_Per_PAN",name2);
			sessUpdate("Namecheck_Score",+parseFloat(scr*100).toFixed(2));
			if(scr < 0.60) {
				$("#32__Step1__pan_img_capture").removeClass("hide");
				$("#32__Step1__pan_img_capture").addClass("span_on_img");
				pan_img_req = true;
				$("#aadhaar_info_display_div").hide();
				$("#Step1").show();
				$("#28__Step1__aadhaar_verify").show();
				$("#31__Step1__pan_verify").hide();
				$("#31__Step1__pan_verify").attr("style", "display:none");
				$("#button_continue_aadhaar").attr("style", "display:block");
				$("#button_continue_aadhaar").addClass("button-inac");
				$("#button_continue_aadhaar").removeClass("button-ac");
				steps_progress("S1");
				
			} else {
				var canvas = document.createElement("CANVAS");
				var ctx = canvas.getContext("2d");
				ctx.font = "20px calibre";
				ctx.fillText("Name As Per Aadhaar:", 10, 25);
				ctx.fillText($("#cust_name").val(), 20, 50);
				ctx.fillText("Name As Per PAN:", 10, 75);
				ctx.fillText(panName, 20, 100);
				ctx.fillText("Score : " + parseFloat(scr * 100).toFixed(2), 10, 125);
				sess.images.pan = canvas.toDataURL();
				dms_inicialize_redirection() ;
				$("#aadhaar_info_display_div").hide();
				$('#Step2').addClass('cont-div');
				$('#Step2').removeClass('hide');
				$('#28__Step1__aadhaar_verify').addClass('hide');
				$('#28__Step1__aadhaar_verify').removeClass('custom-labeltxt');
				$('#28__Step1__aadhaar_verify').removeClass('forms-ac');
				$('#291__Step1__continue').addClass('hide');
				sessUpdate("Name_CheckScore_Canvas","CanvasImageCreated");
				fill_comm_addr();
			}
		}
	}
});
	}	 
	$('#wait').hide();
}

function aadhar_ok() {
	$('#wait').show();
	send_data('cust_dob','Aadhaar_Data_Submit');
	var dbCheck = $('#cust_dob').val();
	dbCheck = dbCheck.replace(/-/g, '');
	var day = parseInt(dbCheck.substr(0, 2)); //Day
	var month = parseInt(dbCheck.substr(2, 2)); //month
	var year = parseInt(dbCheck.substr(4, 6)); //year
	var today = new Date();
	var age = today.getFullYear() - year;
	submitdistimer("button_continue_aad","button-inac","button-ac",10);
	if(today.getMonth() < month || (today.getMonth() == month && today.getDate() < day)) {
		age--;
	}
	if(age < 18) {
		$('#wait').hide();
		//functionAlert("You must be 18 years Old to Open an Account with us.");
		return false;
		$("#aadharage_modal_div").removeClass("hide");
						$("#aadharage_modal_div").addClass("modal");
						$("#aadharage_modal_div").attr("style","display:block");
						$("#aadharage_modal_div").modal({
									  escapeClose: false,
									  clickClose: false,
									  showClose: false
									});
		//window.location.reload();
	}
	else {
		$("button[id='button_continue_aad']").attr("disabled", true);
    $("#button_continue_aad").addClass('button-inac');
      $("#button_continue_aad").removeClass('button-ac');
	    callAML();
		email_check();
        //karza_calls();		
		fill_comm_addr();
		
		var ver_type = $('input:radio[name="aadharverify_radio_otp_recieve"]:checked').val();
		if(ver_type =='1'){
			ver_type = "Mobile";
		}
		else {
			ver_type = "Mail";
		}
		Moengage.track_event("101_Step 2_E-KYC Completed", {
        	     "Verification Type": ver_type
		});	
	}
	//dms_insert_redirection();
			
	
}
jQuery(document).ready(function($) {
	
	if($('#profile_info_annual_income option').length < 2) {
		request_for = 'masterapi';
		field = 'INCOME';
		$.post('http://localhost/101_php/101_uat/public/api.php', {
			request_for: request_for,
			field: field
		}, function(data, result) {
			// console.log(result);
			if(result == 'success') {
				data = JSON.parse(data);
				var str2 = '';
				for(var i = 0; i < data.length; i++) {
					str2 += "<option value='" + data[i].keyword + "'>" + data[i].des + "</option>";
				}
				$("#profile_info_annual_income").append(str2);
			}
		});
	}
	
	if($('#permenant_state option').length < 2 || $('#branchlead_state option').length < 2 || $('#comm_add_state option').length < 2 || $('#kyc_state option').length < 2) {
		request_for = 'masterapi';
		field = 'STATE';
		$.post('http://localhost/101_php/101_uat/public/api.php', {
			request_for: request_for,
			field: field
		}, function(data, result) {
			if(result == 'success') {
				data = JSON.parse(data);
				var str6 = '';
				for(var i = 0; i < data.length; i++) {
					str6 += "<option value='" + data[i].keyword + "'>" + data[i].des + "</option>";
				}
				$("#permenant_state").append(str6);
				$("#comm_add_state").append(str6);
				$('#branchlead_state').append(str6);
				$('#kyc_state').append(str6);
			}
		});
	}
	if($('#dropdown_nominee_relation option').length < 2) {
		request_for = 'masterapi';
		field = 'RELATION';
		$.post('http://localhost/101_php/101_uat/public/api.php', {
			request_for: request_for,
			field: field
		}, function(data, result) {
			if(result == 'success') {
				data = JSON.parse(data);
				var str8 = '';
				for(var i = 0; i < data.length; i++) {
					str8 += "<option value='" + data[i].keyword + "'>" + data[i].des + "</option>";
				}
				$("#dropdown_nominee_relation").append(str8);
				$("#input_minor_nominee_rel").append(str8);
			}
		});
	}
	if($('#input_address_proof_type option').length < 2) {
		request_for = 'masterapi';
		field = 'ADDTYPE';
		$.post('http://localhost/101_php/101_uat/public/api.php', {
			request_for: request_for,
			field: field
		}, function(data, result) {
			if(result == 'success') {
				data = JSON.parse(data);
				var str9 = '';
				for(var i = 0; i < data.length; i++) {
					str9 += "<option value='" + data[i].keyword + "'>" + data[i].des + "</option>";
				}
				$("#input_address_proof_type").append(str9);
			}
		});
	}
	
});
//AML
function callAML() {
	
	re = getCurTime();
	sess['ref'] = re;
	aadhar = $("#aadharverify_number_aadhar").val();
	aadhar='779122708154';
	//ReferenceNumber = re;
	if(amlrefid=="")
	{	
		amlrefid=re;
		ReferenceNumber = re;
	}
	var dob=$("#cust_dob").val();
	var pan=$("#step_1_pan_verify").val();
	var add1=$("#permenant_add1").val();
	var add2=$("#permenant_add2").val();
	var add3=$("#permenant_add3").val();
	var city=$("#permenant_city").val();
	var state=$("#permenant_state").val();
	var pin=$("#permenant_pincode").val();
	request_for = 'amlverification';
	$.ajax ({
      url: "api/amlverification",
      type: "POST",
      data: {
        aadharnumber: aadhar,
		ReferenceNumber: amlrefid,
		First_Name: sess.first_name,
		Middle_Name: sess.middle_name,
		Last_Name: sess.last_name,
		dob: dob,
		pan: pan,
		add1: add1,
		add2: add2,
		add3: add3,
		city: city,
		state: state,
		pin: pin
        },
      success: function(data,result){
	
		if(result == 'success') {
			console.log(result);
			console.log("AML Ack Success");
			sessUpdate("AML_Call",result);
			amlst="Success";
		} else {
			console.log(result);
			console.log("AML Ack Failure");
			amlst="fail";
			sessUpdate("AML_Call",result);
		}
	}
});
	sessUpdate("ReferenceNumber",sess.ref);
	sessUpdate("AML_Call","success");
}

function getCurTime() {
	var date = new Date();
	y = date.getFullYear();
	m = ('0' + (date.getMonth() + 1)).slice(-2);
	d = ('0' + date.getDate()).slice(-2);
	h = date.getHours();
	mn = date.getMinutes();
	s = date.getSeconds();
	mis = date.getMilliseconds();
	var dtt = y + "" + m + "" + d + "" + h + "" + mn + "" + s + "" + mis;
	//functionAlert(dtt);
	return dtt;
	sessUpdate("Cust_Ref_Num",dtt );
}

function loadData(){
	
	var values={};
	capture_browser_info1();
    values=sess.browser_info;
	var session_vals= JSON.stringify(values);
	var currentdate = new Date();
	var leadtime = currentdate.getFullYear() + "-" + (currentdate.getMonth() + 1) + "-" + currentdate.getDate() + "  " + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();
	//alert($('meta[name="csrf-token"]').attr('content'));
	$.ajax ({
      url: "api/sessId",
      type: "POST",
      data: {
        values: session_vals,
		leadtime: leadtime
        },
      success: function(data,result){
	
		console.log("Data Store Success");
		console.log(data);
		var resp=data;
		resp=resp.replace(/ns1:/g,'');
    resp=xmlToJson(new DOMParser().parseFromString(resp,"text/xml"));
    resp=resp.Responses.Response.Representations.Representation.Element;

	resp=resp.replace(/\\/g,'');
    resp=resp.replace(/\"\[/g,"[");
    resp=resp.replace(/\"\{/g,"{");
    
    resp=resp.replace(/\]\"/g,"]");
    resp=resp.replace(/\}\"/g,"}");
    
    resp=JSON.parse(resp);
    session=resp.session_id;
	}
});
}

 function StoreSession() {
 	
	var values = {};
	$.each($('#smart_forms').serializeArray(), function(i, field) {
		values[field.name] = field.value;
		sess[field.name] = field.value;
	});
	var img_ids = $("#Step1").find("img").map(function() {
		return this.name;
	}).get();
	for(var i = 0; i < img_ids.length; i++) {
		if(img_ids[i].length > 0) {
			var dt = $("#" + img_ids[i]).attr("src");
			//console.log(dt);
			if(dt.startsWith("data:")) sess['images'][img_ids[i]] = dt != undefined ? dt : "";
		}
	}
	
	var session_id = session;
	var session_vals = values;
	console.log(session_vals);
	var currentdate = new Date();
	var leadtime = currentdate.getFullYear() + "-" + (currentdate.getMonth() + 1) + "-" + currentdate.getDate() + "  " + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();
	$.ajax ({
      url: "apistore/stageData",
      type: "POST",
      data: {
        app_id: "3",
		session_id: session_id,
		session_vals: session_vals,
		inserted_at: leadtime
		
        },
        
      success: function(data,result){

		console.log("Data Store Success");
		console.log(data);
	}
});
}

function sessUpdate(_key,_val){
	
	var session_id = session;
	var inVal= _val;
	var inKey= _key;
	alert(inKey);
	$.ajax ({
      url: "api/sessUpdate",
      type: "POST",
      data: {
        session_id: session_id,
		inKey: inKey,
		inVal: inVal,
		
		},
	
      success: function(data,result){
	
		console.log("Session Updated");
		}
	});
}

function clean_modalvalue() {
	$(".model_otp1").val('');
	$(".model_otp2").val('');
	$(".model_otp3").val('');
	$(".model_otp4").val('');
	$(".model_otp5").val('');
	$(".model_otp6").val('');
}

function create_account(creationflag='instant') {
	$('#wait').show();
	ReferenceNumber = sess.ref;
	customer_referenceNo = sess.ref;
	product_type = sess.rd_product_select;
	branch_code = sess.profile_info_branch;
	CustomerNumber = sess.CIF;
	name_on_card = sess.cust_name.substr(0, 20);
	nom_name = sess.input_nominee_name || "";
	nom_add1 = sess.nominee_add1 || "";
	nom_add2 = sess.nominee_add2 || "";
	nom_add3 = sess.nominee_add3 || "";
	nom_rel = sess.dropdown_nominee_relation || "";
	nom_min_gur_name = sess.text_minor_nominee_name || "";
	nom_dob = sess.date_nominee_dob || "";
	nom_min_gur_age = sess.input_minor_nominee_age || "";
	nom_min_gur_rel = sess.input_minor_nominee_relation || "";
	request_for = 'accountcreation';
	$.post('http://localhost/101_php/101_uat/public/api.php', {
		request_for: request_for,
		ReferenceNumber: ReferenceNumber,
		customer_referenceNo: customer_referenceNo,
		creationflag: creationflag,
		product_type: product_type,
		branch_code: branch_code,
		CustomerNumber: CustomerNumber,
		name_on_card: name_on_card,
		nom_name: nom_name,
		nom_add1: nom_add1,
		nom_add2: nom_add2,
		nom_add3: nom_add3,
		nom_rel: nom_rel,
		nom_min_gur_name: nom_min_gur_name,
		nom_dob: nom_dob,
		nom_min_gur_age: nom_min_gur_age,
		nom_min_gur_rel: nom_min_gur_rel
	}, function(data, result) {
		
		$("#wait").hide();
		if(data.includes('initiate')){
        // if(data.slice(0,8) == 'initiate'){
            console.log(data);
            return;
        }
		
		console.log(data);
		data = data.replace(/ns1:/g, '');
		data = data.replace(/ns2:/g, '');
		data = xmlToJson(new DOMParser().parseFromString(data, "text/xml"));
		data = data.Responses.Response.Representations.Representation.Element;
		data = xmlToJson(new DOMParser().parseFromString(data, "text/xml"));
		if(data && data.Response && data.Response.AccountNumber) {
			
			sess["account_no"] = data.Response.AccountNumber;
			$('#account_sucess_page').removeClass('hide');
			$.modal.close();
			$('#Step4').addClass('hide');
			full_kyc();
		    rm_details();
			$("#label_cust_name").html(sess.cust_prefix + " " + sess.cust_name);
			$("#input_account_name").val(sess.cust_name);
			$("#input_customer_id").val(sess.CIF);
			$("#input_account_no").val(sess.account_no);
			$("#input_branch").val(sess.selected_branch_name);
			$("#label_ifsc_code").val("FSFB0000001");
			StoreSession();
			sessUpdate("Account","Account Created");
			clearInterval(myVar);
			dms_inicialize();
			facebook();
			twitter();
			var acc=$("#input_account_no").val();
             var cif=$("#input_customer_id").val();
			 var cifaccDetails = cif+"@@"+acc;
             sessUpdate("CIFACC",cifaccDetails);
			 Moengage.add_user_attribute("Savings Account Number",sess.account_no );

		} else {
			//functionAlert("So sorry. We're facing technical difficulty to open your account. Please contact the Bank on 'Support Number 1800 3010 0111' and we will assist you immediately!!.");
			$("#cif_modal_div").removeClass("hide");
						$("#cif_modal_div").addClass("modal");
						$("#cif_modal_div").attr("style","display:block");
						$("#cif_modal_div").modal({
									  escapeClose: false,
									  clickClose: false,
									  showClose: false
									});
			sessUpdate("Account Failure","Account Not Created");
			//window.location.reload();
		}
	});
}
function resendOTP1() {
	//document.getElementById("button_otp_send").disabled = false;
	var items = document.getElementsByName('model_otp');
	for(var i = 0; i < items.length; i++) items[i].value = '';
	var refno = ReferenceNumber;
	var otpVal=$("#otp_model_hidden ").html();
	var resOtp=otpVal.split("@@");
	var key=resOtp[0];
	if(key == 'mobile') {
		send_otp('mobile', 'step_1_mobile_verify');
	} else if(key == 'aadhaar') {
		send_addhaar_otp("aadhaar", a_f_id, channel = "");
	}
}

function resendOTP(){
	var items = document.getElementsByName('model_otp');

    for (var i = 0; i < items.length; i++)

        items[i].value='';
	
	var otpVal=$("#otp_model_hidden ").html();
	var resOtp=otpVal.split("@@");
	//var key=resOtp[1];
	var resendfun=resOtp[2];
	//var refno=resOtp[3];
	eval(resendfun);
	
}
//get branch details
function get_branchdetails() {
	$('#wait').show();
		var str = '';
	request_for = 'getbranch';
	var pincode = $('#comm_add_pincode').val();
	pincode = pincode.substr(0,3);
	$.post('http://localhost/101_php/101_uat/public/apistoredata.php', {
		request_for: request_for,
		pincode: pincode
	}, function(data, result) {
		var testbranch={};
		testbranch=data;
		branchlen=testbranch.length;
		console.log(data);
		var resp=JSON.parse(data);
		if(result == 'success') {
			if(data == '[]' || branchlen <= '3') {
				$('#profile_info_branch').find('option').remove().end().append('<option value="10349">Sarjapur</option>');
				$("#50__Step3__branch_select").hide();
				$('#wait').hide();
			} else {
				
				for(var i = 0; i < resp.length; i++) {
					//str6 += "<option value='" + data[i].keyword + "'>" + data[i].des + "</option>";
					str += "<option value='" + resp[i].branch_code + "'>" + resp[i].branch_name + " - " + resp[i].branch_address + "</option>";
				}
				$("#profile_info_branch").append(str);
				$('#wait').hide();
			}
		}else{
			$('#wait').hide();
			$("#50__Step3__branch_select").hide();
			$('#profile_info_branch').find('option').remove().end().append('<option value="10349">SARJAPUR - FINCARE SMALL FINANCE BANK LTD.5TH FLOOR, BREN MERCURY, KAIKONDANAHALLI, SARJAPUR MAIN ROAD, BANGALORE</option>');
		}
	});
	$('#wait').hide();
	
}
function get_newbranch() {
	var str = '';
	request_for = 'branchdetails';
	var pincode = $('#comm_add_pincode').val();
	$.post('http://localhost/101_php/101_uat/public/api.php', {
		request_for: request_for,
		pincode: pincode
	}, function(data, result) {
		console.log(data);
		var resp=JSON.parse(data);
		if(result == 'success') {
			if(JSON.parse(data)[0].msg == "No Records found!") {
				$('#profile_info_branch').find('option').remove().end().append('<option value="10349">Sarjapur</option>');
				$("#50__Step3__branch_select").hide();
			} else {
				
				for(var i = 0; i < resp.length; i++) {
					//str6 += "<option value='" + data[i].keyword + "'>" + data[i].des + "</option>";
					str += "<option value='" + resp[i].branch_code + "'>" + resp[i].branch_name + " - " + resp[i].branch_address + "</option>";
				}
				$("#profile_info_branch").append(str);
			}
		}else{
			$("#50__Step3__branch_select").hide();
			$('#profile_info_branch').find('option').remove().end().append('<option value="10349">SARJAPUR - FINCARE SMALL FINANCE BANK LTD.5TH FLOOR, BREN MERCURY, KAIKONDANAHALLI, SARJAPUR MAIN ROAD, BANGALORE</option>');
		}
	});
}
//rating
var rating_str = '';
var emailids = '';
var rat = '';
var hau_data = '';

function ratingStore() {
	rat = localStorage.getItem('rating');
	request_for = 'ratingData';
	var cif = sess.CIF;
	var rating = rat;
	var email = emailids;
	var msg = hau_data;
	var comments = rating_str;
	$.post('http://localhost/101_php/101_uat/public/apistoredata.php', {
		request_for: request_for,
		cif: cif,
		rating: rating,
		email: email,
		msg: msg,
		comments: comments
	}, function(data, result) {
		console.log(data);
		$.modal.close();
		$('.selection').hide();
		/*$.dialog({
            title: 'Congratulations !',  content: 'Thank you for banking with Us',animationSpeed: 2000,
        });
        setInterval(function() {location.reload(true);}, 3000);*/
	});
	
	//upi_qr_gen();
}

function Storehau() {
	if($('input[name="checkbox_customer_hau"]:checked')) {
		hau_data = $('input[name="checkbox_customer_hau"]:checked').val();
		if($('input[name="checkbox_customer_hau"]:checked').val() == "Other_sources") {
			hau_data += $('#text_hau').val();
		}
		$.modal.close();
		ratingStore();
	}
	$("#264__Rating__customer_hau").hide();
	$("#13__Step4__checkbox_dbt_avail_yn").removeClass("hide");
	$("#16__Step4__checklist_dbtyn").removeClass("hide");
}

function Storeratingless() {
	rat = localStorage.getItem('rating');
	$("#button_submit_rm").hide();
	if(rating == 1 || rating == 2) {
		if($('input[name="checkbox_customer_rating_less"]:checked')) {
			rating_str = $('input[name="checkbox_customer_rating_less"]:checked').val();
		}
		if($('input[name="checkbox_customer_rating_less"]:checked').val() == "Other Reasons") {
			rating_str += $('#rating-input').val();
		}
	}
	
	$.modal.close();
	    ratingStore();
		Moengage.destroy_session();
}

function Storeratingmore() {
	$("#button_submit_rl").hide();
	rat = localStorage.getItem('rating');
	if(rating == 3 || rating == 4 || rating == 5) {
		if($("#text_customer_rating_more_email1").val().length == 0) {
			functionAlert("Please Enter Email Id");
			return false;
		}
		for(i = 1; i <= 5; i++) {
			if($("#text_customer_rating_more_email" + i).val().length > 0) {
				if(i == 1) emailids = $("#text_customer_rating_more_email" + i).val();
				else {
					if(emailids == "") emailids += $("#text_customer_rating_more_email" + i).val();
					else emailids += "," + $("#text_customer_rating_more_email" + i).val();
				}
			}
		}
	}
			
	var email = emailids;
	
	$.post('http://localhost/101_php/101_uat/public/kyc_schedule.php', {
		request_for: 'ratingmail',
		email: email
	}, function(data, result) {
		console.log(data);
		$.modal.close();
	    ratingStore();
	});
	Moengage.destroy_session();
}
//AML score check
function aml_score_check() {
	$('#wait').show();
	sessUpdate("AML_ScoreButton","Clicked");
	$('#button_shownext_tnc').addClass('button-inac');
			$('#button_shownext_tnc').removeClass('button-ac');
				document.getElementById("button_shownext_tnc").disabled = true;
	//request_for = 'aml_score_new';
	
	request_for = 'aml_score_new';
	$.post('http://localhost/101_php/101_uat/public/api.php', {
		request_for: request_for,
		ref_no: amlrefid,
	}, function(data, result) {
		
		//    console.log(data);
		if(JSON.parse(data).AML_status == "Success") {
			console.log(JSON.parse(data).AML_status);
			sessUpdate("AML_SCRORE",JSON.parse(data).AML_Score);
	        sessUpdate("AML_Status","AML Done");
			var amscore=JSON.parse(data).AML_Score;
			if(amscore<=1.8){
				$('#wait').hide();
				//newCIFcreation();
				video_kyc();
			}	
			else{
				$('#wait').hide();
				//functionAlert("Thanks for showing interest in banking with us. Our Official / representative will get in touch with you");
				$("#aml_modal_div").removeClass("hide");
						$("#aml_modal_div").addClass("modal");
						$("#aml_modal_div").attr("style","display:block");
						$("#aml_modal_div").modal({
									  escapeClose: false,
									  clickClose: false,
									  showClose: false
									});
				sessUpdate("AML_Status","AML Score not available");
				//window.location.reload();
			}
		}
		 else {
			 $('#wait').hide();
			functionAlert("Please Retry.");
			$('#button_shownext_tnc').removeClass('button-inac');
			$('#button_shownext_tnc').addClass('button-ac');
			document.getElementById("button_shownext_tnc").disabled = false;
			
		}
	});
}
var qrImage='';
function qrCode(){
	$("#aimg").attr("href","data:image/jpg;base64," +qrImage);
}
// QR generation
function upi_qr_gen() {
	request_for = 'upi_qr_gen';
	var email = sess.step_1_email_verify;
	var custName = sess.cust_name;
	var accno = $('#input_account_no').val();
	$.post('http://localhost/101_php/101_uat/public/api.php', {
		request_for: request_for,
		accountNumber: accno,
		customerName: custName,
		email: email
	}, function(data, result) {
		console.log(data);
		  qrImage=data.qr_img;
	var qrUrl=decodeURIComponent(qrImage);
	$("#qrimage").attr('src', "data:image/jpg;base64," + qrUrl);
	});
}
//CRM dedup
function crm_dedup() {
	$('#wait').show();
	request_for = 'crm_dedup';
	var custName = $("#cust_name").val();
	var referenceno = sess.ref;
	var motherName = $("#profile_info_mother_first_name").val();
	var dob = $("#cust_dob").val();
	var aadharNumber = $("#aadharverify_number_aadhar").val();
	var panNumber = $("#step_1_pan_verify").val();
	$.post('http://localhost/101_php/101_uat/public/api.php', {
		request_for: request_for,
		referenceNo: referenceno,
		custName: custName,
		dob: dob,
		motherName: motherName,
		aadharNumber: aadharNumber,
		panNumber: panNumber
	}, function(data, result) {
		console.log(data);
		var res = JSON.parse(data).root.NewDataSet.Table1.ID;
		if(res == "00") {
			$('#wait').hide();
			$('#Step4').addClass('cont-div');
			$('#Step4').removeClass('hide');
			$('#Step3').addClass('hide');
			$('#Step3').removeClass('cont-div');
			$('#28__Step1__aadhaar_verify').css('display', 'none');
			$('#32__Step1__pan_img_capture').css('display', 'none');
			$("#68__Step4__account_nominee_yn").addClass("new-width");
			sessUpdate("Second_dedupe_check","New Customer" );
		} else {
			$('#wait').hide();
			$("#dedupe_modal_div").removeClass("hide");
						$("#dedupe_modal_div").addClass("modal");
						$("#dedupe_modal_div").attr("style","display:block");
						$("#dedupe_modal_div").modal({
									  escapeClose: false,
									  clickClose: false,
									  showClose: false
									});
			sessUpdate("Second_dedupe_check","Old Customer" );
			
			
		}
			
	});
	$('#wait').hide();
	
}

function dms_inicialize() {
	var img_keys = Object.keys(sess.images);
	var custreferenceno = sess.ref;
	var Doc_Reference_Number = sess.ref;
	var Number_of_Documents = img_keys.length;
	sessUpdate("Number_of_Documents",Number_of_Documents);
	$.ajax ({
      url: "api/dms_inicialize",
      type: "POST",
      data: {
        customer_reference_number: custreferenceno,
		Doc_Reference_Number: Doc_Reference_Number,
		Number_of_Documents: Number_of_Documents
        },
      success: function(data,result){
	
		if(JSON.parse(data).Status == "Success") {
			dms_insert();
		}
	}
});
}

function dms_inicialize_redirection() {
	
	var img_keys = Object.keys(sess.images);
	var custreferenceno = sess.ref;
	var Doc_Reference_Number = sess.ref;
	var Number_of_Documents = img_keys.length;
	sessUpdate("Number_of_Documents",Number_of_Documents);
	$.ajax ({
      url: "api/dms_inicialize",
      type: "POST",
      data: {
        customer_reference_number: custreferenceno,
		Doc_Reference_Number: Doc_Reference_Number,
		Number_of_Documents: Number_of_Documents
        },
      success: function(data,result){
	
		if(JSON.parse(data).Status == "Success") {
			dms_insert_redirection();
		}
	}
});
}

function dms_insert() {
  	 var img_keys=Object.keys(sess.images);
       for(var i=0;i<img_keys.length;i++)
       {
           
			var img_b64=sess.images[img_keys[i]];
			
			img_b64=img_b64.replace('data:image/png;base64,','');
			img_b64=img_b64.replace('data:image/jpg;base64,','');
          
		   var custreferenceno = sess.ref;
			var base64 = img_b64;
            var Doc_Type=img_keys[i];
           
			var date = new Date()
            var h=date.getHours();
            var mn=date.getMinutes();
            var s=date.getSeconds();
            var cif=sess.CIF;
			var acc=sess.account_no;
            var ran=Math.floor(1000 + Math.random() * 9000);
			
	        var leadtime = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + "  " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds(); 
            var partId=""+h+mn+s+ran;
            var branch= sess.profile_info_branch;
			var pan= sess.step_1_pan_verify;
			var aadhaar= sess.aadharRef;
			var addType= sess.input_address_proof_type;
			var addNum= sess.input_address_proof_number;
			var Doc_Part_Reference_Number = partId;
			$.post('http://localhost/101_php/101_uat/public/api.php', {

				request_for : 'dms_insert',
				customer_reference_number : custreferenceno,
				Doc_part_num : Doc_Part_Reference_Number,
                base64  : base64,
                Doc_Type : Doc_Type,
				CIF: cif,
				Account: acc,
				uploaddate: leadtime,
				branch: branch,
				pan: pan,
				aadhaar: aadhaar,
				addNum: addNum,
				addType: addType,

            }, function(data, result) {
			   console.log(data);
			});
            
            
	   }						
}

function dms_insert_redirection() {
  	 var img_keys=Object.keys(sess.images);
	 var Number_of_Documents = img_keys.length;
	sessUpdate("Number_of_Documents",Number_of_Documents);
	
       for(var i=0;i<img_keys.length;i++)
       {
           
			var img_b64=sess.images[img_keys[i]];
			
			img_b64=img_b64.replace('data:image/png;base64,','');
			img_b64=img_b64.replace('data:image/jpg;base64,','');
			img_b64=img_b64.replace('data:image/jpeg;base64,','');
          
		   var custreferenceno = sess.ref;
			var base64 = img_b64;
            var Doc_Type=img_keys[i];
           
			var date = new Date()
            var h=date.getHours();
            var mn=date.getMinutes();
            var s=date.getSeconds();
            var ran=Math.floor(1000 + Math.random() * 9000);
			
	        var leadtime = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + "  " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds(); 
            var partId=""+h+mn+s+ran;
			var Doc_Part_Reference_Number = partId;
			sessUpdate(Doc_Type+"_id",Doc_Part_Reference_Number+"@@"+custreferenceno) ;
			
			$.post('http://localhost/101_php/101_uat/public/dms_api_redirection.php', {

				request_for : 'dms_insert_redirection',
				customer_reference_number : custreferenceno,
				Doc_part_num : Doc_Part_Reference_Number,
                base64  : base64,
                Doc_Type : Doc_Type,
				uploaddate: leadtime

            }, function(data, result) {
			   console.log(data);
			});
            
            
	   }						
}

function dms_address_insert_redirection() {

	var img_keys = Object.keys(sess.images);
	var custreferenceno = sess.ref;
	var Doc_Reference_Number = sess.ref;
	var Number_of_Documents = img_keys.length;
	sessUpdate("Number_of_Documents",Number_of_Documents);
	$.post('http://localhost/101_php/101_uat/public/dms_api_redirection.php', {
		request_for: 'dms_inicialize',
		customer_reference_number: custreferenceno,
		Doc_Reference_Number: Doc_Reference_Number,
		Number_of_Documents: Number_of_Documents
	}, function(data, result) {
		if(JSON.parse(data).Status == "Success") {
			
		
      
			  for(var i=0;i<img_keys.length;i++)
			   {
				   if(img_keys[i] == "address_front" || img_keys[i] == "address_back" )
				   {
				   
						var img_b64=sess.images[img_keys[i]];
						
						img_b64=img_b64.replace('data:image/png;base64,','');
						img_b64=img_b64.replace('data:image/jpg;base64,','');
						img_b64=img_b64.replace('data:image/jpeg;base64,','');
					  
					   var custreferenceno = sess.ref;
						var base64 = img_b64;
						var Doc_Type=img_keys[i];
					   
						var date = new Date()
						var h=date.getHours();
						var mn=date.getMinutes();
						var s=date.getSeconds();
						var ran=Math.floor(1000 + Math.random() * 9000);
						
						var leadtime = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + "  " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds(); 
						var partId=""+h+mn+s+ran;
						var Doc_Part_Reference_Number = partId;
						sessUpdate(Doc_Type+"_id",Doc_Part_Reference_Number+"@@"+custreferenceno) ;
						
						$.post('http://localhost/101_php/101_uat/public/dms_api_redirection.php', {

							request_for : 'dms_insert_redirection',
							customer_reference_number : custreferenceno,
							Doc_part_num : Doc_Part_Reference_Number,
							base64  : base64,
							Doc_Type : Doc_Type,
							uploaddate: leadtime

						}, function(data, result) {
						   console.log(data);
						});
				   }	
					
			   }	

		}
	});	
	
}

var kyc_date = '';
var kyc_time = '';
var kyc_time_check='';
function fullKyc_submit() {
	kyc_time = $('#kyc_time').val();
	kyc_date = $('#kyc_date').val();
	cif = sess.CIF;
	var address = /[0-9a-zA-Z\,\- ]/g;
	var pat1 = /^\d{6}$/;
	var kadd1 = document.getElementById("kyc_comm_add_add1").value;
	var kadd2 = $('#kyc_comm_add_add2').val() + " " + $('#kyc_comm_add_add3').val();
	var kpincode = document.getElementById("kyc_comm_add_pincode").value;
	var kcity = document.getElementById("kyc_comm_add_city").value;
	var kstate = document.getElementById("kyc_state").value;
	if(($('input[type=radio][name=rd-same_as_curr_address_yn]:checked').length > 0) && ($('input[name="rd-same_as_curr_address_yn"]:checked').val() == "Same_As_Current_Address")) {
		if(kyc_date == "") {
			functionAlert("Please Enter Valide Date");
			return false;
		}
		if(kyc_time_check == "") {
			functionAlert("Please Enter Valide Time");
			return false;
		}
		addresstype = "Y";
	}
	if(($('input[type=radio][name=rd-same_as_curr_address_yn]:checked').length > 0) && ($('input[name="rd-same_as_curr_address_yn"]:checked').val() == "Other")) {
		if(kadd1 == '') {
			functionAlert("Please Enter Address Line1");
			return false;
		}
		if(kadd2 == '') {
			functionAlert("Please Enter Address Line2");
			return false;
		}
		addresstype = "N";
	}
	if(kpincode == '') {
		functionAlert("Please Enter Valid Pincode");
		return false;
	}
	if(!pat1.test(kpincode)) {
		functionAlert("Pincode should be 6 digits ");
		return false;
	}
	if(!kcity.match(/^[a-zA-Z ]+$/)) {
		functionAlert("Please Enter Valid City");
		return false;
	}
	if(kstate == "Select Value") {
		functionAlert("Please Select State");
		return false;
	}
	if(kstate == "") {
		functionAlert("Please Select State");
		return false;
	}
	if(kyc_date == "") {
		functionAlert("Please Enter Valide Date");
		return false;
	}
	if(kyc_time == "") {
		functionAlert("Please Enter Valide Time");
		return false;
	}
	if(kyc_time=="Morning"){
	    kyc_time_check="1";
	}
    else if(kyc_time=="Afternoon"){
	    kyc_time_check="2";
	}
	else if(kyc_time=="Evening"){
		 kyc_time_check="3";
    }
	
	var mobileas='91';
	var mob=mobileas.concat(sess.step_1_mobile_verify);
	
	var custreferenceno = sess.ref;
	var cif = sess.CIF;
	var email = sess.step_1_email_verify;
	var custName = sess.cust_name;
	var mobile = sess.step_1_mobile_verify;
	var date = $('#kyc_date').val();
	var timeslot = $('#kyc_time').val();
	var sch_flag = "schdule";
	
	$.post('http://localhost/101_php/101_uat/public/api.php', {
		request_for: 'whatsappKYC',
		email: email,
		mob: mob,
		cif: cif,
		customerName: custName,
		date: date,
		timeslot: timeslot,
		sch_flag: sch_flag,
	}, function(data, result) {
		console.log(data);
		sessUpdate("whatsapp_kyc_msg","sent");
		$.post('http://localhost/101_php/101_uat/public/masterapi_new.php', {
		ref: custreferenceno,
		name: custName,
		cif: cif,
		kadd1: kadd1,
		kadd2: kadd2,
		kcity: kcity,
		kpincode: kpincode,
		state: kstate,
		date: date,
		timeslot: kyc_time_check,
		},
		function(data, result) {
		console.log(data);
		$.post('http://localhost/101_php/101_uat/public/kyc_schedule.php', {
		request_for: 'ecomkycschedule',
		email: email,
		mobile: mobile,
		cif: cif,
		customerName: custName,
		date: date,
		timeslot: timeslot,
		sch_flag: sch_flag,
	}, function(data, result) {
		console.log(data);
		$.modal.close();
		$("#Debit_Card_Details").removeClass("hide");
			$("#Debit_Card_Details").addClass("modal");
			$("#Debit_Card_Details").attr("style","display:block");
			$("#Debit_Card_Details").modal({
						  escapeClose: false,
						  clickClose: false,
						  showClose: false
						});	
	});
		
	});
	});
	timecheck();
  

}
// ecom skip
function kyc_skip() {
	$.modal.close();

	$("#Debit_Card_Details").removeClass("hide");
			$("#Debit_Card_Details").addClass("modal");
			$("#Debit_Card_Details").attr("style","display:block");
			$("#Debit_Card_Details").modal({
						  escapeClose: false,
						  clickClose: false,
						  showClose: false
						});	
	
	timecheck();
	var custreferenceno = sess.CIF;
	var email = sess.step_1_email_verify;
	var mobile = sess.step_1_mobile_verify;
	$.post('http://localhost/101_php/101_uat/public/kyc_skip.php', {
		request_for: 'ecomkycskip',
		email: email,
		mobile: mobile,
		cif: custreferenceno,
	}, function(data, result) {
		console.log(data);
	});
	
}


function rm_details() {
	
	var email = sess.step_1_email_verify;
	
	$.post('http://localhost/101_php/101_uat/public/kyc_skip.php', {
			request_for: 'rmmail',
			emailID: email
		}, function(data, result) {
			console.log(data);				
				fund_acc();
				debitmsg();
		});
}


function fund_acc() {
	 var name=sess.cust_name;
	 var account=sess.account_no;	
	var email = sess.step_1_email_verify;
	
	$.post('http://localhost/101_php/101_uat/public/kyc_schedule.php', {
		request_for: 'fundmail',
		email: email,
		name: name,
		account: account
	}, function(data, result) {
		console.log(data);
	});
}

/////leadSubmit()//////////
function leadSubmit() {
	var mobile = $('#branchlead_number_mobile').val();
	var email = $('#branchlead_email').val();
	var fullname = $('#input_full_name').val();
	var branchlead_add1 = $('#branchlead_add1').val();
	var branchlead_add2 = $('#branchlead_add2').val();
	var branchlead_add3 = $('#branchlead_add3').val();
	var branchlead_pincode = $('#branchlead_pincode').val();
	var branchlead_city = $('#branchlead_city').val();
	var state = $('#branchlead_state').val();
	var pat1 = /^\d{6}$/;
	var address = /[0-9a-zA-Z\,\- ]/g;
	if(!fullname.match(/^[A-Za-z ]+$/)) {
		functionAlert("Please Enter Full Name");
		return false;
	}
	if(fullname.length > 50) {
		functionAlert("Name Field cannot be longer than 50 characters");
		return false;
	}
	if(branchlead_add1 == "") {
		functionAlert("Please Enter Address Line1");
		return false;
	}
	if(branchlead_add1.replace(address, '').length > 0) {
		functionAlert("Please Enter Valid Address Line1");
		return false;
	}
	if(branchlead_add2 == "") {
		functionAlert("Please Enter Address Line2");
		return false;
	}
	if(branchlead_add2.replace(address, '').length > 0) {
		functionAlert("Please Enter Valid Address Line2");
		return false;
	}
	if(branchlead_add3 != "") {
		if(branchlead_add3.replace(address, '').length > 0) {
			functionAlert("Please Enter Valid Address Line3");
			return false;
		}
	}
	if(branchlead_pincode == "") {
		functionAlert("Please Enter Valid Pincode");
		return false;
	}
	if(!pat1.test(branchlead_pincode)) {
		functionAlert("Pincode Should be 6 digits ");
		return false;
	}
	if(!branchlead_city.match(/^[A-Za-z ]+$/)) {
		functionAlert("Please Enter Valid City");
		return false;
	}
	if(state == "") {
		functionAlert("Please Select State");
		return false;
	}
	var currentdate = new Date();
	var leadtime = currentdate.getFullYear() + "-" + (currentdate.getMonth() + 1) + "-" + currentdate.getDate() + "  " + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();
	submitdistimer("button_submit_lead","button-inac","button-ac",10);
	$.post('http://localhost/101_php/101_uat/public/api.php', {
		request_for: 'leadSubmit',
		mobile: mobile,
		email: email,
		fullname: fullname,
		branchlead_add1: branchlead_add1,
		branchlead_add2: branchlead_add2,
		branchlead_add3: branchlead_add3,
		branchlead_pincode: branchlead_pincode,
		branchlead_city: branchlead_city,
		state: state,
		leadtime: leadtime
	}, function(data, result) {
		console.log(data);
		functionAlert("We Will Contact You Soon");
		window.location.reload();
	});
}

function pincodecheck() {
	var pincode = $('#kyc_comm_add_pincode').val();
	
	if(pincode.length==6)
	{
		$.post('http://localhost/101_php/101_uat/public/masterapi.php', {
			pincode: pincode,
		}, function(data, result) {
			console.log(data);
			if(data == "NO") {
				functionAlert("Sorry! We are not present in your location. You can try another location or contact us.");
				$.modal.close();
				kyc_skip();
			}
		});
	
	}
}



function whatsappCif(){
	
	var dt = new Date();
	var time = zeroPrefix(dt.getHours()) + ":" + zeroPrefix(dt.getMinutes()) + ":" + zeroPrefix(dt.getSeconds());	
	var currentTime = new Date();
	var month = currentTime.getMonth() + 1;
	var date = currentTime.getDate();
	var year = currentTime.getFullYear();
	var curdate=date + '-' + month + '-' + year;
	var mobile='91';
	mobile=mobile.concat(sess.step_1_mobile_verify);
	var customerid = sess.CIF;
	var name= sess.cust_name;
	first_name= sess.first_name;
	last_name= sess.last_name;
	var inserted_at=currentTime.getFullYear() + "-" + (currentTime.getMonth() + 1) + "-" + currentTime.getDate() + "  " + currentTime.getHours() + ":" + currentTime.getMinutes() + ":" + currentTime.getSeconds();
	
		$.post('http://localhost/101_php/101_uat/public/api.php', {
			request_for: 'whatsapp_cif',
			customerid: customerid,
			customer_name: name,
			date: curdate,
			time: time,
			mobile: mobile,
			
		}, function(data, result) {
			console.log(data);
			sessUpdate("CIF_Whatsapp","Sent");
			whatsappAcc();
			$.post('http://localhost/101_php/101_uat/public/apistoredata.php', {
				request_for: 'whatsappData',
				mobile: mobile,
				first_name: first_name,
				last_name: last_name,
				inserted_at: inserted_at
				
			}, function(data, result) {
				console.log(data);
	            
			});
		});
}

function zeroPrefix(intVal){

   return intVal <10 ? "0"+intVal :intVal;
} 

function whatsappAcc(){
	var prod='';
	if(sess.rd_product_select == "1021"){
		prod="101 First";
	}
	if(sess.rd_product_select == "1022"){
		prod="101 Priority";
	}
	
	var currentTime = new Date();
	var month = currentTime.getMonth() + 1;
	var date = currentTime.getDate();
	var year = currentTime.getFullYear();
	var curdate=date + '-' + month + '-' + year;
	var mobile='91';
	mobile=mobile.concat(sess.step_1_mobile_verify);
	var account_number = sess.account_no;
	var name=sess.cust_name;
	var account_type=prod;
	
		$.post('http://localhost/101_php/101_uat/public/api.php', {
			request_for: 'whatsapp_account',
			account_type: account_type,
			customer_name: name,
			date: curdate,
			account_number: account_number,
			mobile: mobile,
			
		}, function(data, result) {
			console.log(data);
			sessUpdate("Acc_Whatsapp","Sent");
			
		});
}

function newCIFcreation(creationflag='instant')
{
	$('#wait').show();
	document.getElementById("button_shownext_tnc").disabled = true;
	$('#button_shownext_tnc').addClass('button-inac');
			$('#button_shownext_tnc').removeClass('button-ac');
		
		sess.images.aadhaarEkyc=sess.images.aadhaarEkyc.replace('data:image/png;base64,','');
	sess.images.aadhaarEkyc=sess.images.aadhaarEkyc.replace('data:image/jpg;base64,','');
	sess.images.aadhaarEkyc=sess.images.aadhaarEkyc.replace('data:image/jpeg;base64,','');

    var Ref_no = sess.ref;
    var aadhar_no = sess.aadharRef;
	 var aadhar = cifaadhar;
    var prefix = sess.cust_prefix.toUpperCase();
    var cust_name = sess.cust_name.toUpperCase();
    var first_name = sess.first_name.toUpperCase();
    var middle_name = sess.middle_name.toUpperCase();
    var last_name = sess.last_name.toUpperCase();
    var cust_gender = sess.cust_gender.toUpperCase();
    var cust_dbo = sess.cust_dob;
    var annum_inc = sess.profile_info_annual_income;
    var occu = sess.profile_info_occupation;
    var marital_sts = sess.profile_info_marital_status;
    var mother_maiden = $('#profile_info_mother_maiden_name').val();
    var emailID = sess.step_1_email_verify.toUpperCase();
    var registered_mobile = sess.step_1_mobile_verify;
    var pan = sess.step_1_pan_verify.toUpperCase();
    var kyc_type = "Aadhaar-OTP";
    var added_by = "";
    var oit = "20";
    var oin = sess.oin;
    var m_f_n = sess.profile_info_mother_first_name.toUpperCase();
    var m_l_n = sess.profile_info_mother_last_name.toUpperCase();
    var address1 = sess.permenant_add1.toUpperCase();
    var address2 = sess.permenant_add2.toUpperCase();
    var address3 = sess.permenant_add3.toUpperCase();
    var city = sess.permenant_city.toUpperCase();
    var state = sess.permenant_state;
    var pincode = sess.permenant_pincode;
    var cadd1 = sess.comm_add_add1.toUpperCase();
	var cadd2 = sess.comm_add_add2.toUpperCase();
	var cadd3 = sess.comm_add_add3.toUpperCase();
	var ccity = sess.comm_add_city.toUpperCase();
	var cpin = sess.comm_add_pincode;
	var cstate = sess.comm_add_state;
	var boo= sess.profile_info_branch;
	var photo=sess.images.aadhaarEkyc;
    //var signature=sess.images.signature;
	var cum_id=sess.input_address_proof_type.toUpperCase();
	var cumm_id='';
	var cumm_type='';
	var address1_c='';
	var address2_c='';
	var address3_c='';
	var city_c='';
	var state_c='';
	var pincode_c='';
	var onboarding_flag="COAO";
	
	var add_flag="current_communication";
    if((sess.input_address_proof_type).length>0)
    {
        add_flag="current";
        cumm_type=sess.input_address_proof_type.toUpperCase();
		cumm_id=sess.input_address_proof_number.toUpperCase();
		//address_flag="communication";
		address1_c=sess.comm_add_add1.toUpperCase();
		address2_c=sess.comm_add_add2.toUpperCase();
		address3_c=sess.comm_add_add3.toUpperCase();
		city_c=sess.comm_add_city.toUpperCase();
		state_c=$("#select2-comm_add_state-container").text();
		pincode_c=sess.comm_add_pincode;
	}

	
    $.post('http://localhost/101_php/101_uat/public/api.php', {
        request_for : 'newCIFcreation',
		createFlag:creationflag,
        Ref_no : Ref_no,
        aadhar_no : aadhar_no,
		aadhar : aadhar,
        prefix : prefix,
        cust_name : cust_name,
        first_name : first_name,
        middle_name : middle_name,
        last_name : last_name,
        cust_gender : cust_gender,
        cust_dbo : cust_dbo,
        annum_inc : annum_inc,
        occu : occu,
        marital_sts : marital_sts,
        mother_maiden : mother_maiden,
        emailID : emailID,
        registered_mobile : registered_mobile,
        pan : pan,
        kyc_type : kyc_type,
        added_by : added_by,
        oit : oit,
        oin : oin,
        m_f_n : m_f_n,
        boo : boo,
        m_l_n : m_l_n,
        address1 : address1,
        address2 : address2,
        address3 : address3,
        city : city,
        state : state,
        pincode : pincode,
		photo: photo,
		cumm_type: cumm_type,
		cumm_id: cumm_id,
		add_flag: add_flag,
		address1_c: address1_c,
		address2_c: address2_c,
		address3_c: address3_c,
		city_c: city_c,
		state_c: state_c,
		pincode_c: pincode_c,
		onboarding_flag: onboarding_flag,

     }, function(data, result) {
		 $('#wait').hide();
		 if(data.includes('initiate')){
			var resp=data.split(",");
            var newresp=resp['1']+","+resp['2'];
            newresp=JSON.parse(newresp);
            if(newresp['status'] == "success" && newresp['error_message'] == "Success"){ 
			  create_account('initiate');
            }
            else if(newresp['status'] == "failed" && newresp['error_message'] == "Customer is already exists in CBS"){
               $("#cif_modal_div").removeClass("hide");
						$("#cif_modal_div").addClass("modal");
						$("#cif_modal_div").attr("style","display:block");
						$("#cif_modal_div").modal({
									  escapeClose: false,
									  clickClose: false,
									  showClose: false
									});
            }
            else if(newresp['status'] == "failed"){
            	$("#cif_modal_div").removeClass("hide");
						$("#cif_modal_div").addClass("modal");
						$("#cif_modal_div").attr("style","display:block");
						$("#cif_modal_div").modal({
									  escapeClose: false,
									  clickClose: false,
									  showClose: false
									});
            }
			
            return;
        }
		
		
       console.log(data);
		var resp = data;
		resp = JSON.parse(resp);
		var cif = resp.CIF;
		if (resp.status == "success") {
			sess["CIF"] = cif;
			create_account();
			sessUpdate("CIF","CIF Created");
			var f = document.getElementById("profile_info_occupation");
			var occup = f.options[f.selectedIndex].text;
			Moengage.add_user_attribute("Last Name", last_name); 
			Moengage.add_user_attribute("Savings Account Balance", "0" );
			Moengage.add_user_attribute("Mobile Banking Active", "FALSE");
			Moengage.add_user_attribute("Internet Banking Active","FALSE");
			Moengage.add_user_attribute("Type of Debit Card", "");
			Moengage.add_user_attribute("UPI Active", "FALSE");
			Moengage.add_user_attribute("Profession", occup);
			
			Moengage.track_event("101_Step 5_Account Created_Web KYC", {
			    "Last Name": last_name,
			    "Savings Account Number": sess.account_no,
				"Savings Account Balance": "0",
				"Mobile Banking Active": "FALSE",
				"Internet Banking Active": "FALSE",
				"Type of Debit Card": "",
				"UPI Active": "FALSE",
				"Profession": occup

			   });			
			Moengage.update_unique_user_id(sess["CIF"]);
		}
			else{
				//functionAlert("So sorry. We're facing technical difficulty to open your account. Please contact the Bank on 'Support Number 1800 3010 0111' and we will assist you immediately.");
				$("#cif_modal_div").removeClass("hide");
						$("#cif_modal_div").addClass("modal");
						$("#cif_modal_div").attr("style","display:block");
						$("#cif_modal_div").modal({
									  escapeClose: false,
									  clickClose: false,
									  showClose: false
									});
				sessUpdate("CIF Failure","CIF Not Created");
			//window.location.reload();
			 
			}
			//StoreSession();	
    });
	$('#wait').hide();
}

/*
var orderId='';
var payment_id='';
var request_id='';
function fund_amount(){
	var acc = sess.account_no;
	var amount=$('#input_fund_amount').val();
	
	if(amount<=90000 && amount>=1000){
		 $.post('http://localhost/101_php/101_uat/public/api.php', {
			request_for : 'fund',
			acc : acc,
			amount : amount,
		}, function(data, result) {
		   console.log(data);
		   orderId=JSON.parse(data).order_id;
		   request_id=JSON.parse(data).request_id;
		   funding();
		  });
	}	
    else{
		functionAlert("Minimum amount you can fund is  ₹ 1,000 and Maximum amount is ₹ 90,000.");
	}		
	sessUpdate("Fund_Amount",amount);
}

function razor_pay(){
	
	 $.post('http://localhost/101_php/101_uat/public/api.php', {
        request_for : 'fund_razor',
        payment_id : payment_id,
		orderId : orderId,
		request_id : request_id,
	}, function(data, result) {
		var refno=JSON.parse(data).ref_no;
		
		if(refno!=null || refno.length>0)
		{
			functionAlert("Congratulations. Your account is successfylly funded.");
			sessUpdate("Funding","Amount Funded Successfully");
		}
		else
		{
			functionAlert("Sorry the transaction couldn't get through. Any amount debited will be refunded in 3-5 business days.");
		}
		
       console.log(data);
	   console.log(JSON.parse(data).ref_no);
	  }); 
	
}


function funding(){
    var amt=$("#input_fund_amount").val();
	
  var options = {
    "key": "rzp_live_GfGD4An1CauIgU", 
    "amount": amt, 
    "currency": "INR",
    "name": "Fincare",
    "description": "Small Finance Bank",
    "image": "images/Logo.png",
    "order_id": orderId,
    "callback_url": 'http://localhost/101_php/101_uat/public/new.html',
	"redirect": false,
	"handler": function (response){
		 payment_id=response.razorpay_payment_id;
		console.log(response);
		razor_pay();
    },
    "prefill": {
        "name": sess.cust_name,
        "email": sess.step_1_email_verify,
        "contact": sess.step_1_mobile_verify
    },
    "notes": {
        "address": "note value"
    },
    "theme": {
        "color": "#F37254"
    }
};
var rzp1 = new Razorpay(options);
  
    rzp1.open();
    //e.preventDefault();
}
*/

//Razorpay new code

var orderId = '';
var payment_id = '';
var request_id = '';

function fund_amount() {
	
	var acc = sess.account_no;
	var amount=$('#input_fund_amount').val();
	
	 if(amount<=90000 && amount>=1000){
		 $.post('http://localhost/101_php/101_uat/public/api.php', {
		request_for: 'fund',
		acc: acc,
		amount: amount,
		}, function (data, result) {
			console.log(data);
			orderId = JSON.parse(data).order_id;
			request_id = JSON.parse(data).request_id;
			funding();
		});

	}	
    else{
		functionAlert("Minimum amount you can fund is  ₹ 1,000 and Maximum amount is ₹ 90,000.");
	}

}

function funding() {
	var amt =$("#input_fund_amount").val();

	var options = {
		"key": "rzp_live_GfGD4An1CauIgU",
		"amount": amt,
		"currency": "INR",
		"name": "Fincare",
		"description": "Small Finance Bank",
		"image": "images/Logo.png",
		"order_id": orderId,
		"callback_url": 'http://localhost/101_php/101_uat/public/index.html',
		"redirect": false,
		"handler": function (response) {
			payment_id = response.razorpay_payment_id;
console.log("funding");
			console.log(response);
			//razor_pay();
		},
		"prefill": {
			"name": sess.cust_name,
			"email": sess.step_1_email_verify,
			"contact": sess.step_1_mobile_verify
		},
		"notes": {
			"address": "note value"
		},
		"theme": {
			"color": "#F37254"
		}
	};
	var rzp1 = new Razorpay(options);

	rzp1.open();
	//e.preventDefault();
}


function photoupload(){
   sess.images.customer=sess.images.customer.replace('data:image/png;base64,','');
	sess.images.customer=sess.images.customer.replace('data:image/jpg;base64,','');
	sess.images.customer=sess.images.customer.replace('data:image/jpeg;base64,','');

	var custreferenceno = sess.ref;
	var base64 = sess.images.customer;
	var Doc_Type = "customer";
		var date = new Date()
			var h = date.getHours();
			var mn = date.getMinutes();
			var s = date.getSeconds();
			var cif = sess.CIF;
			var acc = sess.account_no;
			var ran = Math.floor(1000 + Math.random() * 9000);

			var leadtime = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + "  " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
			var partId = "" + h + mn + s + ran;
			var Doc_Part_Reference_Number = partId;

	$.post('http://localhost/101_php/101_uat/public/api.php', {

				request_for: 'dms_insert',
				customer_reference_number: custreferenceno,
				Doc_part_num: Doc_Part_Reference_Number,
				base64: base64,
				Doc_Type: Doc_Type,
				CIF: cif,
				Account: acc,
				uploaddate: leadtime

			}, function (data, result) {
				console.log(data);
				if( document.getElementById("file_customer_sign").files.length != 0 ){
				 signatureupload();
			  }
				
			});
}

function signatureupload(){
	 sess.images.signature=sess.images.signature.replace('data:image/png;base64,','');
	sess.images.signature=sess.images.signature.replace('data:image/jpg;base64,','');
	sess.images.signature=sess.images.signature.replace('data:image/jpeg;base64,','');

	var custreferenceno = sess.ref;
	var base64 = sess.images.signature;
	var Doc_Type = "sign";
	
			var date = new Date()
			var h = date.getHours();
			var mn = date.getMinutes();
			var s = date.getSeconds();
			var cif = sess.CIF;
			var acc = sess.account_no;
			var ran = Math.floor(1000 + Math.random() * 9000);

			var leadtime = date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate() + "  " + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds();
			var partId = "" + h + mn + s + ran;
			var Doc_Part_Reference_Number = partId;

	$.post('http://localhost/101_php/101_uat/public/api.php', {

				request_for: 'dms_insert',
				customer_reference_number: custreferenceno,
				Doc_part_num: Doc_Part_Reference_Number,
				base64: base64,
				Doc_Type: Doc_Type,
				CIF: cif,
				Account: acc,
				uploaddate: leadtime

			}, function (data, result) {
				console.log(data);
			});
}


function send_data(field_id,flag_id) {
   var msg='';
	if(flag_id == 'pan'){
      msg='Pan Entered';
	}
	if(flag_id == 'Aadhaar_Data_Submit'){
      msg='Aadhaat Data Submit Button Clicked';
	}
	if(flag_id == 'email'){
      msg='Email Entered';
	}
	if(flag_id == 'Aadhaar'){
      msg='Aadhaar Entered';
	}
	if(flag_id == 'Syntizen_OTP'){
      msg='Syntizen Aadhhar OTP generation API Called';
	}
	if(flag_id == 'Syntizen_OTP_Verify'){
      msg='Syntizen Aadhhar OTP Verification API Called';
	}
	 if(flag_id == 'KARZA_Email'){
      msg='KARZA_Email_Done';
	}
	if(flag_id == 'KARZA_Pan'){
      msg='KARZA_Pan_Done';
	}
	if(flag_id == 'KARZA_Name_Check'){
      msg='KARZA_Name_Done';
	}
	if(flag_id == 'Aadhaar_Consent'){
      msg='Aadhaar_Consent_Form_Submitted';
	}
	if(flag_id == 'Aadhaar_Consent_skip'){
      msg='Aadhaar_Consent_skip_Submitted';
	}
	if(flag_id == 'Step2'){
      msg='Step2_Communication_Address_Submitted';
	}
	if(flag_id == 'Step3'){
      msg='Step3_Profile_Information_Submitted';
	}
	if(flag_id == 'Step4'){
      msg='Step4_Product_Details_Submitted';
	}
	if(flag_id == 'Step5'){
      msg='Step5_Nominee_Details_Submitted';
	}
	if(flag_id == 'I_dont_have_PAN'){  
      msg='I_dont_have_PAN';
	}
	if(flag_id == 'Dedupe_button_clikced'){  
      msg='Dedupe_button_clikced';
	}
	
	var session_id = session;
	var additional_info = sess.browser_referer;
	var flag = flag_id;
	var input_val= $("#" + field_id).val();
	var currentdate = new Date();
	var leadtime = currentdate.getFullYear() + "-" + (currentdate.getMonth() + 1) + "-" + currentdate.getDate() + "  " + currentdate.getHours() + ":" + currentdate.getMinutes() + ":" + currentdate.getSeconds();

	$.ajax ({
      url: "apistore/failData",
      type: "POST",
      data: {
        //request_for: request_for,
       	sessid: session_id,
		input_val: input_val,
		msg: msg,
		flag: flag,
		additional_info: additional_info,
		inserted_at: leadtime,
		},
      success: function(data,result){
	
		console.log("Data Store Success");
		console.log(data);
	}
});
}

function karza_calls(){
	startLoader();
	var pan = $("#step_1_pan_verify").val();
    var name1 = $('#cust_name').val();
	var email_ver = $("#step_1_email_verify").val();
	var pattern = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	if(email_ver.length < 1) {
		stopLoader();
		submitdistimer("button_email_verify","button-ac","button-inac",3);
		functionAlert("Please Enter Email ID");
		$("#28__Step1__aadhaar_verify").hide();
		$("#27__Step1__email_verify").show();
		$("#button_email_verify").show();
		$("#aadhaar_info_display_div").hide();
		$("button[id='button_email_verify']").attr("disabled", false);
		$("#button_email_verify").removeClass('hide');
		$("#button_email_verify").addClass('button-ac');
		return false;
	} else if(!email_ver.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)) {
		stopLoader();
		submitdistimer("button_email_verify","button-ac","button-inac",3);
		functionAlert("Please Enter Valid Email ID");
		$("#28__Step1__aadhaar_verify").hide();
		$("#27__Step1__email_verify").show();
		$("#button_email_verify").show();
		$("#aadhaar_info_display_div").hide();
		$("button[id='button_email_verify']").attr("disabled", false);
		$("#button_email_verify").removeClass('hide');
		$("#button_email_verify").addClass('button-ac');
		return false;
	} else if(email_ver.match(pattern)) {
		request_for = 'emailcheck';
		
		$.post('http://localhost/101_php/101_uat/public/api.php', {
			request_for: request_for,
			pan: pan,
			name1: name1,
			email_ver: email_ver
		}, function(data, result) {
		
			var resp = JSON.parse(data);
			 var res=resp['error_msg'];
			  var panres=resp['res'];
          if(res == "Invalid response"){
          	 
          	  if(panres.status=="504"){
				  
          	  	stopLoader();
				submitdistimer("button_pan_verify","button-ac","button-inac",3);
				functionAlert("Please check and Reenter Your PAN number");
				$("#28__Step1__aadhaar_verify").hide();
				$("#31__Step1__pan_verify").show();
				$("#button_pan_verify").show();
				$("#aadhaar_info_display_div").hide();
				$("#27__Step1__email_verify").attr("style", "display:none");
				$('#27__Step1__email_verify').hide();
				$("button[id='button_pan_verify']").attr("disabled", false);
				$('#button_continue_aadhaar').removeClass('button-inac');
				$("#button_pan_verify").addClass('button-ac');
				$('#button_continue_aadhaar').addClass('hide');
				$("#button_pan_verify").removeClass('button-inac');
				$("#button_pan_verify").removeClass('hide');
				$("#button_pan_verify").addClass('button-ac');
          	  }
          	  else if(panres['status-code']=="102"){
					stopLoader();
				functionAlert("Please Try After Some Time");
				window.location.reload();
          	  }
			}
			else{
				 res=JSON.parse(res);
                var sta=res['statusCode']; //101
               var scr=res['result']['score'];
               panName=resp['pan_name'];
			}
		if(resp.status == "Failed" && resp.stage == "KarzaPan"){
			stopLoader();
				submitdistimer("button_pan_verify","button-ac","button-inac",3);
				functionAlert("Please check and Reenter Your PAN number");
				$("#28__Step1__aadhaar_verify").hide();
				$("#31__Step1__pan_verify").show();
				$("#button_pan_verify").show();
				$("#aadhaar_info_display_div").hide();
				$("#27__Step1__email_verify").attr("style", "display:none");
				$('#27__Step1__email_verify').hide();
				$("button[id='button_pan_verify']").attr("disabled", false);
				$("#button_pan_verify").removeClass('hide');
				$("#button_pan_verify").addClass('button-ac');
		}	
		
		else{
			sessUpdate("KARZA_PAN","Done" );
			send_data('cust_name','KARZA_Name_Check');
			if(res.statusCode == "101") {
				sessUpdate("KARZA_NAMECHECK","Done" );
				scr = parseFloat(res.result.score);
				console.log(scr);
				var cus_name = $("#cust_name").val();
				sessUpdate("Name_As_Per_Aadhaar",cus_name);
				sessUpdate("Name_As_Per_PAN",panName);
				sessUpdate("Namecheck_Score",+parseFloat(scr*100).toFixed(2));
				if(scr < 0.60) {
					$("#32__Step1__pan_img_capture").removeClass("hide");
					$("#32__Step1__pan_img_capture").addClass("span_on_img");
					pan_img_req = true;
					$("#aadhaar_info_display_div").hide();
					$("#Step1").show();
					$("#28__Step1__aadhaar_verify").show();
					$("#31__Step1__pan_verify").hide();
					$("#31__Step1__pan_verify").attr("style", "display:none");
					$("#button_continue_aadhaar").attr("style", "display:block");
					$("#button_continue_aadhaar").addClass("button-inac");
					$("#button_continue_aadhaar").removeClass("button-ac");
					steps_progress("S1");
					
				} else {
					var canvas = document.createElement("CANVAS");
					var ctx = canvas.getContext("2d");
					ctx.font = "20px calibre";
					ctx.fillText("Name As Per Aadhaar:", 10, 25);
					ctx.fillText($("#cust_name").val(), 20, 50);
					ctx.fillText("Name As Per PAN:", 10, 75);
					ctx.fillText(panName, 20, 100);
					ctx.fillText("Score : " + parseFloat(scr * 100).toFixed(2), 10, 125);
					sess.images.pan = canvas.toDataURL();
					$("#aadhaar_info_display_div").hide();
					$('#Step2').addClass('cont-div');
					$('#Step2').removeClass('hide');
					$('#28__Step1__aadhaar_verify').addClass('hide');
					$('#28__Step1__aadhaar_verify').removeClass('custom-labeltxt');
					$('#28__Step1__aadhapanNamear_verify').removeClass('forms-ac');
					$('#291__Step1__continue').addClass('hide');
					sessUpdate("Name_CheckScore_Canvas","CanvasImageCreated");
					fill_comm_addr();
				}
		  }
		  else{
		  	name_check();
		  }
		}
	  });
	} else {
		stopLoader();
		submitdistimer("button_email_verify","button-ac","button-inac",3);
		functionAlert("Incorrect Email ID. Please Check and Reenter");
		$("#28__Step1__aadhaar_verify").hide();
		$("#27__Step1__email_verify").show();
		$("#button_email_verify").show();
		$("#aadhaar_info_display_div").hide();
		$("button[id='button_email_verify']").attr("disabled", false);
		$("#button_email_verify").removeClass('hide');
		$("#button_email_verify").addClass('button-ac');
		return false;
	}
	stopLoader();
	
}

var panName = "";
function pan_check(){
	submitdistimer("button_pan_verify","button-inac","button-ac",3);
	startLoader();
	var pan_ver = $("#step_1_pan_verify").val();
	var pattern = /^([a-zA-Z]{3})([aA]|[bB]|[cC]|[eE]|[fF]|[gG]|[hH]|[lL]|[jJ]|[pP]|[tT]{1})([a-zA-Z]{1})(\d{4})([a-zA-Z]{1})/;
	if(pan_ver.length < 1) {
	submitdistimer("button_pan_verify","button-ac","button-inac",3);
		stopLoader();
		functionAlert("Please Enter PAN Number");
		$("#28__Step1__aadhaar_verify").hide();
		$("#31__Step1__pan_verify").show();
		$("#aadhaar_info_display_div").hide();
		$('#27__Step1__email_verify').hide();
		$("#button_pan_verify").show();
		$("#27__Step1__email_verify").attr("style", "display:none");
		$("button[id='button_pan_verify']").attr("disabled", false);
		$("#button_pan_verify").removeClass('hide');
		$("#button_pan_verify").addClass('button-ac');
		return false;
	} else if(pan_ver.length != 10) {
	submitdistimer("button_pan_verify","button-ac","button-inac",3);
		stopLoader();
		functionAlert("Please Enter Valid PAN number");
		$("#28__Step1__aadhaar_verify").hide();
		$("#31__Step1__pan_verify").show();
		$("#aadhaar_info_display_div").hide();
		$('#27__Step1__email_verify').hide();
		$("#27__Step1__email_verify").attr("style", "display:none");
		$("#button_pan_verify").show();
		$("button[id='button_pan_verify']").attr("disabled", false);
		$("#button_pan_verify").removeClass('hide');
		$("#button_pan_verify").addClass('button-ac');
		return false;
	} else if(pan_ver.match(pattern)) {
		request_for = 'pancheck';
		var name1 = $('#cust_name').val();
		var pan = document.getElementById("step_1_pan_verify").value;
		$.post('http://localhost/101_php/101_uat/public/api.php', {
			request_for: request_for,
			pan: pan,
			name1: name1
		}, function(data, result) {
	        var resp = JSON.parse(data);
			 var res=resp['error_msg'];
			  var panres=resp['res'];
          if(res == "Invalid response"){
          	 
          	  if(panres.status=="504"){
				  
          	  	stopLoader();
				submitdistimer("button_pan_verify","button-ac","button-inac",3);
				functionAlert("Please check and Reenter Your PAN number");
				$("#28__Step1__aadhaar_verify").hide();
				$("#31__Step1__pan_verify").show();
				$("#button_pan_verify").show();
				$("#aadhaar_info_display_div").hide();
				$("#27__Step1__email_verify").attr("style", "display:none");
				$('#27__Step1__email_verify').hide();
				$("button[id='button_pan_verify']").attr("disabled", false);
				$("#button_pan_verify").removeClass('hide');
				$("#button_pan_verify").addClass('button-ac');
          	  }
          	  else if(panres['status-code']=="102"){
					stopLoader();
				functionAlert("Please Try After Some Time");
				window.location.reload();
          	  }
			}
			else{
				 res=JSON.parse(res);
                var sta=res['statusCode']; //101
               var scr=res['result']['score'];
               panName=resp['pan_name'];
			}
			if(resp.status == "Failed" && resp.stage == "KarzaPan"){
				stopLoader();
					submitdistimer("button_pan_verify","button-ac","button-inac",3);
					functionAlert("Please check and Reenter Your PAN number");
					$("#28__Step1__aadhaar_verify").hide();
					$("#31__Step1__pan_verify").show();
					$("#button_pan_verify").show();
					$("#aadhaar_info_display_div").hide();
					$("#27__Step1__email_verify").attr("style", "display:none");
					$('#27__Step1__email_verify').hide();
					$("button[id='button_pan_verify']").attr("disabled", false);
					$("#button_pan_verify").removeClass('hide');
					$("#button_pan_verify").addClass('button-ac');
			}	
			
			else{
				sessUpdate("KARZA_PAN","Done" );
				//send_data('cust_name','KARZA_Name_Check');
				if(res.statusCode == "101") {
					sessUpdate("KARZA_NAMECHECK","Done" );
					scr = parseFloat(res.result.score);
					console.log(scr);
					var cus_name = $("#cust_name").val();
					sessUpdate("Name_As_Per_Aadhaar",cus_name);
					sessUpdate("Name_As_Per_PAN",panName);
					sessUpdate("Namecheck_Score",+parseFloat(scr*100).toFixed(2));
					if(scr < 0.60) {
						$("#32__Step1__pan_img_capture").removeClass("hide");
						$("#32__Step1__pan_img_capture").addClass("span_on_img");
						pan_img_req = true;
						$("#aadhaar_info_display_div").hide();
						$("#Step1").show();
						$("#28__Step1__aadhaar_verify").show();
						$("#31__Step1__pan_verify").hide();
						$("#31__Step1__pan_verify").attr("style", "display:none");
						$("#button_continue_aadhaar").attr("style", "display:block");
						$("#button_continue_aadhaar").addClass("button-inac");
						$("#button_continue_aadhaar").removeClass("button-ac");
						steps_progress("S1");
						
					} else {
						var canvas = document.createElement("CANVAS");
						var ctx = canvas.getContext("2d");
						ctx.font = "20px calibre";
						ctx.fillText("Name As Per Aadhaar:", 10, 25);
						ctx.fillText($("#cust_name").val(), 20, 50);
						ctx.fillText("Name As Per PAN:", 10, 75);
						ctx.fillText(panName, 20, 100);
						ctx.fillText("Score : " + parseFloat(scr * 100).toFixed(2), 10, 125);
						sess.images.pan = canvas.toDataURL();
						$("#aadhaar_info_display_div").hide();
						$('#Step2').addClass('cont-div');
						$('#Step2').removeClass('hide');
						$('#28__Step1__aadhaar_verify').addClass('hide');
						$('#28__Step1__aadhaar_verify').removeClass('custom-labeltxt');
						$('#28__Step1__aadhaar_verify').removeClass('forms-ac');
						$('#291__Step1__continue').addClass('hide');
						sessUpdate("Name_CheckScore_Canvas","CanvasImageCreated");
						fill_comm_addr();
					}
			  }
			  else{
				name_check();
			  }
			}
		});
	} else {
	   submitdistimer("button_pan_verify","button-ac","button-inac",3);
		stopLoader();
		functionAlert("Incorrect PAN. Please Check and Reenter");
		$("#28__Step1__aadhaar_verify").hide();
		$("#31__Step1__pan_verify").show();
		$("#aadhaar_info_display_div").hide();
		$('#27__Step1__email_verify').hide();
		$("#27__Step1__email_verify").attr("style", "display:none");
		$("#button_pan_verify").show();
		$("button[id='button_pan_verify']").attr("disabled", false);
		$("#button_pan_verify").removeClass('hide');
		$("#button_pan_verify").addClass('button-ac');
		return false;
	}
	stopLoader();
}


function generate_aadhaardedupe_otp(aadhar_field_id, channel_field_name) {
	cifaadhar = $("#aadharverify_number_aadhar").val();
      sessUpdate("dedupe_check","Clicked" );
	  send_data('Dedupe button Clicked','Dedupe_button_clikced');
	var radioValue = $("input[name='aadharverify_radio_otp_recieve']:checked").val();
	a_f_id = aadhar_field_id;
	c_f_name = channel_field_name;
	if(verify_number_aadhar(document.getElementById(aadhar_field_id))) {
		aadhaar = $("#" + aadhar_field_id).val();
		
		var ch = $("input[name='" + channel_field_name + "']:checked").val();
		if(ch == undefined || ch.length == 0) {
			functionAlert("Please Select Channel of Aadhaar OTP");
			return false;
		}
		if(aadhaar.length == 16) {
			send_otp("aadhaar", aadhar_field_id, ch);
		} else if(aadhaar.length == 12) {
			request_for = 'aadhaardedupe';
			submitdistimer("aadharverify_button_verify","btn-verify-inac custom-labeltxt","btn-verify",40);
			$.ajax ({
		      url: "api/aadhaardedupe",
		      type: "POST",
		      data: {
		       aadhaarNo: aadhaar,
			   ch: ch
		        },
		      success: function(data,result){
			
				if(result == 'success') {
					if (data == 'Customer exists') {
						$("#dedupe_modal_div").removeClass("hide");
						$("#dedupe_modal_div").addClass("modal");
						$("#dedupe_modal_div").attr("style","display:block");
						$("#dedupe_modal_div").modal({
									  escapeClose: false,
									  clickClose: false,
									  showClose: false
									});
						sessUpdate("dedupe_check","Old Customer" );
						
					}
					else
					{
						var txnid = JSON.parse(data).txnid;
			        console.log(txnid);
			
			         var resp=JSON.parse(data);
					// var resp=JSON.parse(data);
					 var a=JSON.parse(data).OTPSendTo;
					
                  if(result == 'success') {
					 if(JSON.parse(data).result == 'y'){
							console.log('success');
							var b=a.split(",");
					        var sendto=b[0];
							if(radioValue == '2'){
								 sendto=b[1];
							}
							var dt = "aadhaar@@" + aadhaar + "@@generate_aadhaardedupe_otp('" + a_f_id + "','" + c_f_name + "')@@" + txnid;
							$('#otp_model_hidden').html(dt);
							$('#otp_sent_to').html(sendto);
							$("#otp_modal_div").attr("style", "display:block");
							$("#otp_modal_div").modal({
								escapeClose: false,
								clickClose: false,
								showClose: false
							});
							$("#aadharverify_button_verify").removeClass("btn-verify");
							$("#aadharverify_button_verify").addClass("btn-vfd");
							$("#aadharverify_button_verify").html("Verified");
							document.getElementById("aadharverify_button_verify").disabled = true;
							$("#aadharverify_number_aadhar").attr("readOnly", true);
							document.getElementById("button_continue_aadhaar").className = "button-ac";
							$('.aadhaarverifytxn').val(txnid);
							$('.aadhaarno').val(aadhaar);
							sessUpdate("aadhaar_vfd_result","Yes");
					 }	
					 else if(JSON.parse(data).result == 'n') {
						if(JSON.parse(data).errMsg == 'Aadhaar number does not have verified mobile' || JSON.parse(data).errMsg == 'Aadhaar number does not have verified mobile/email' || JSON.parse(data).errCode=='111'){
							$("#aadharotp_modal_div").removeClass("hide");
								$("#aadharotp_modal_div").addClass("modal");
								$("#aadharotp_modal_div").attr("style","display:block");
								$("#aadharotp_modal_div").modal({
											  escapeClose: false,
											  clickClose: false,
											  showClose: false
											});
						}
						else if(JSON.parse(data).errMsg=='OTP Flooding error' || JSON.parse(data).errCode=='952'){
							$("#otpflood_modal_div").removeClass("hide");
								$("#otpflood_modal_div").addClass("modal");
								$("#otpflood_modal_div").attr("style","display:block");
								$("#otpflood_modal_div").modal({
											  escapeClose: false,
											  clickClose: false,
											  showClose: false
											});
						}
						else{
							$("#otpflood_modal_div").removeClass("hide");
								$("#otpflood_modal_div").addClass("modal");
								$("#otpflood_modal_div").attr("style","display:block");
								$("#otpflood_modal_div").modal({
											  escapeClose: false,
											  clickClose: false,
											  showClose: false
											});
						}
						sessUpdate("aadhaar_vfd_result","NO"+JSON.parse(data).errMsg);
					}
					else{
						functionAlert("Failed to Receive the Response from Aadhaar Server. Please try after some time");
						sessUpdate("aadhaar_vfd_result",result+"NO Response From Server");
					}
				} 
				else{
					functionAlert("Failed to Receive the Response from Aadhaar Server. Please try after some time");
					sessUpdate("aadhaar_vfd_result",result+"NO Response From Server");
				}
			}
		}
				
	  }
	});
	}
  }
}


// ===========================vdeo kyc======================


///for loader		
// $(document).ready(function() {
//     $(document).ajaxStart(function() {
//         $("#wait").show();
//     });
//     $(document).ajaxComplete(function() {
//         $("#wait").hide();
//     });

// });
//name field validation
function alphanumeric()
{ 
var inputtxt = $("#profile_info_mother_first_name").val();

$("#profile_info_mother_first_name").val(inputtxt.replace(/[^a-zA-Z ]/g, ""));
$("#input_full_name").val($("#input_full_name").val().replace(/[^a-zA-Z ]/g, ""));
$("#branchlead_city").val($("#branchlead_city").val().replace(/[^a-zA-Z ]/g, ""));
$("#comm_add_city").val($("#comm_add_city").val().replace(/[^a-zA-Z ]/g, ""));

}

//ecom radio button

// var element = document.getElementsByName('rd-same_as_curr_address_yn');
// element.checked = true;

//validation for upper cases
function uppercasesFunction() {
    var x = document.getElementById("fname");
    x.value = x.value.toUpperCase();
  }
var videoref = '';

function storeVideodata() {
    videoref = sess.ref;
    //videoref=sess;
}

// function sendImages(){
// 	var ref = sess.ref;
// 	request_for = 'crtfolder';
// 	$.post('http://localhost/101_php/101_uat/public101_CR_vkyc/videodata.php', {
// 		request_for: request_for,
// 		ref: ref
// 	}, function(data, result) {
// 		console.log(data);
// 		var res=data;
// 		res=JSON.parse(res);

// 	});
// }


function storeImageOnServer() {
	
		$("#wait").show();
        var img_keys = Object.keys(sess.images);
        for (var i = 0; i < img_keys.length; i++) {
            if (img_keys[i] == 'aadhaarEkyc' ) {

                var img_b64 = sess.images[img_keys[i]];
                img_b64 = img_b64.replace('data:image/png;base64,', '');
                img_b64 = img_b64.replace('data:image/jpg;base64,', '');
                img_b64 = img_b64.replace('data:image/jpeg;base64,', '');

                var custreferenceno = sess.ref;
                var base64 = img_b64;
                var Doc_Type = img_keys[i];
                $.post(domainpapiurl, {
                    request_for: 'storeImageOnServer',
                    customer_reference_number: custreferenceno,
                    base64: base64,
                    Doc_Type: Doc_Type

                }, function(data, result) {
					$("#wait").hide();
                    sess.linkArray.aadhaarEkyc = data;
                    console.log(data);
					 newCIFcreation('initiate'); 
					 video_kycstart();  
                });

            } 
			else{
			$("#cif_modal_div").removeClass("hide");
						$("#cif_modal_div").addClass("modal");
						$("#cif_modal_div").attr("style","display:block");
						$("#cif_modal_div").modal({
									  escapeClose: false,
									  clickClose: false,
									  showClose: false
									});
		  }
        }
    
}

function createProfileIdForVideoKYC() {
	$("#wait").show();
	
	//domainurl = 'http://localhost/101_php/101_uat/public101_Prod';
	//domainpapiurl = domainurl + '/api.php';//vkyc
    var custPhoto = sess.linkArray.aadhaarEkyc;
    var sig = sess.linkArray.signature;

    console.log(custPhoto);
    // var pan = sess.linkArray.pan;
    var customer_reference_number = sess.ref;
    var first_name = sess.first_name.toUpperCase();
    var last_name = sess.last_name.toUpperCase();
    var middle_name = sess.middle_name.toUpperCase();
    var emailID = sess.step_1_email_verify.toUpperCase();
    var registered_mobile = sess.step_1_mobile_verify;
    var pan = sess.step_1_pan_verify.toUpperCase();
    var cust_dbo = sess.cust_dob;
    var mother_maiden = sess.profile_info_mother_maiden_name.toUpperCase();

    var address1 = sess.permenant_add1.toUpperCase();
    var address2 = sess.permenant_add2.toUpperCase();
    var address3 = sess.permenant_add3.toUpperCase();
    var city = sess.permenant_city.toUpperCase();
    var state = sess.permenant_state;
    var pincode = sess.permenant_pincode;
    var cum_id = sess.input_address_proof_type.toUpperCase();
    var cum_id = "";
    var cumm_type = "Permenant";
    if ((sess.input_address_proof_type).length > 0) {
        add_flag = "current";
        cumm_type = sess.input_address_proof_type.toUpperCase();
        cumm_id = sess.input_address_proof_number.toUpperCase();
        address1 = sess.comm_add_add1.toUpperCase();
        address2 = sess.comm_add_add2.toUpperCase();
        address3 = sess.comm_add_add3.toUpperCase();
        city = sess.comm_add_city.toUpperCase();
        state = sess.comm_add_state;
        pincode = sess.comm_add_pincode;
    }

    $.post(domainpapiurl, {
        request_for: 'createProfileId',
        custPhoto: custPhoto,
        doc_sig: sig,
        // doc_pan : pan,
        customer_reference_number: customer_reference_number,
        first_name: first_name,
        last_name: last_name,
        middle_name: middle_name,
        emailID: emailID,
        registered_mobile: registered_mobile,
        pan: pan,
        cust_dbo: cust_dbo,
        mother_maiden: mother_maiden,
        address1: address1,
        address2: address2,
        address3: address3,
        city: city,
        state: state,
        pincode: pincode,
        cum_id: cum_id,
        cumm_type: cumm_type

    }, function(data, result) {
		//console.log(data);
		$("#wait").hide();
        var res = JSON.parse(data);
        if (res.profile_id.length != 0) {
            sess.profileId = res.profile_id;
            getLinkForVideoKYC();
        } else {
            alert("Unable to genrate Profile ID, Try again later");
        }

    });

}

function getLinkForVideoKYC() {
	$("#wait").show();
	//domainurl = 'http://localhost/101_php/101_uat/public101_Prod';
	//domainpapiurl = domainurl + '/api.php';//vkyc
    var profileId = sess.profileId;
    $.post(domainpapiurl, {
        request_for: 'getLink',
        profileId: profileId,
    }, function(data, result) {
		$("#wait").hide();
        //console.log(data);
        var res = JSON.parse(data);
        if (res.capture_link.length != 0) {
            sess.videoKYCLink = res.capture_link;
            video_kycsession();
        } else {
            alert("Unable to genrate Capture Link, Try again later");
        }

    });
}


function video_kycsession(){
	$("#video_kycsatrt_div").removeClass("hide");
	$("#video_kycsatrt_div").addClass("modal");
	$("#video_kycsatrt_div").attr("style","display:block");
	  //call fuunction autoomatically;
	setTimeout(() => {
		//console.log('timer started....')
		launchToVideoKYC();
	}, 4000);

	$("#video_kycsatrt_div").modal({
				  escapeClose: false,
				  clickClose: false,
				  showClose: false
				});

}


function launchToVideoKYC() {
	console.log('launchToVideoKYC called.....');
	domainurl = 'http://localhost/101_php/101_uat/public';
	domainpapiurl = domainurl + '/api.php';//vkyc
    var customer_ref = sess.ref;
    var videoLink = sess.videoKYCLink;
    var update_profileId = sess.profileId;
    //var redirect_URL = 'http://localhost/101_php/101_uat/public101_CR_vkyc/index_rating.html?ref=' + customer_ref;
    var redirect_URL = domainurl + '/rating.html?ref=' + customer_ref + '%26profileId=' + update_profileId;

    var final_URL = videoLink + "&redirect_uri=" + redirect_URL;

    $.post(domainurl + '/apistoredata.php', {
        request_for: 'insertVideoLinkData',
        customerRef: customer_ref,
        profile_id: update_profileId,
        capture_link: final_URL
    }, function(data, result) {
        //console.log(data);
        window.location.href = final_URL;

    });
    //window.location.href = final_URL;
}



function confermetion(){
	$.modal.close();
	$.confirm({
        title: 'Confirm!',
        content: 'To keep your account balance more than 1 Lakh, complete your FULL KYC. <br> ' +
        'Do you want verify KYC Now ?.',
		buttons: {
			confirm: function () {
                //for test purpose as of now not working api//
				newCIFcreation('initiate');
				
				video_kyc();

			},
			cancel: function () {

				//alert('no');

				newCIFcreation();

                google_step4_tnc();
                
                full_kyc();
			  
			},
		   
		}
	});

}


function debitmsg(){
   var mobile = sess.step_1_mobile_verify;
	
	$.post('http://localhost/101_php/101_uat/public/kyc_skip.php', {
		request_for: 'debitcardmsg',
		mobile: mobile
		
	}, function(data, result) {
		console.log(data);
	});
}

function pan_dedupe(){
	var pan = document.getElementById("step_1_pan_verify").value;
	
  if(pan.match(/^([a-zA-Z]{3})([aA]|[bB]|[cC]|[eE]|[fF]|[gG]|[hH]|[lL]|[jJ]|[pP]|[tT]{1})([a-zA-Z]{1})(\d{4})([a-zA-Z]{1})/)){ 
		
			request_for = 'pandedupe';
			$.ajax ({
		      url: "apistore/pandedupe",
		      type: "POST",
		      data: {
		        //request_for: request_for,
		        pan: pan
		        },
		      success: function(data,result){
	        
				   console.log(data);
                   if(data=="failed"){
                   	 return true;
                   }
                   else{
                   	  functionAlert("You already tried opening account with us with the given PAN number. Your Account creation process is under progress.Please contact the Bank on 'Support Number 1800 313 313' and we will assist you immediately.");
					  return false;
						  
                   }
				}
			});
		} 
}
//// redirection code

//redirection code

var Namecheck_Score = "";
function redirection()
{
	var redirection_mob = document.getElementById('step_1_mobile_verify').value ;

	$.ajax ({
      url: "api/redirection",
      type: "POST",
      data: {
        //request_for: request_for,
       mobile: redirection_mob
        },
      success: function(data,result){
	
				data = JSON.parse(data);
				console.log(data);
				if(data.sts_code == "A01")
	{
		
		$('#27__Step1__email_verify').addClass('forms-ac');
		//$('#27__Step1__email_verify').removeClass('forms-inac');
		$('#27__Step1__email_verify').removeClass('hide');
		$('#27__Step1__email_verify').addClass('new-width');
		
		$('#31__Step1__pan_verify').addClass('forms-inac');
		//$('#31__Step1__pan_verify').removeClass('forms-inac');
		$('#31__Step1__pan_verify').removeClass('hide');
		$('#31__Step1__pan_verify').addClass('new-width');
		
		
		$('#34__Step1__continue').removeClass('hide');
		
		
	}
	else
	{
		
		//mob , email , pan available 
		var mobile = document.getElementById("step_1_mobile_verify").value;
		var email = data.email;
		var pan = data.pan;
		$("#step_1_email_verify").val(email);
		$("#step_1_pan_verify").val(pan);
		document.getElementById("26__Step1__mobile_verify").className="hide";
		document.getElementById("27__Step1__email_verify").className="hide";
		document.getElementById("31__Step1__pan_verify").className="hide";
		document.getElementById("34__Step1__continue").className="hide";
			
		//function datasubmit
		document.getElementById("aadharverify_radio_otp_recieve").checked = true;
		submitdistimer("button_continue","button-inac","button-ac",4);
		$("#kyc_state").val("");
		$("#comm_add_state").val("");
		$("#branchlead_state").val("");
		StoreSession();
		sessUpdate("Channel",data.mode);
		sessUpdate("step_1_mobile_verify" , mobile);
		sessUpdate("step_1_email_verify" , email);
		sessUpdate("step_1_pan_verify" , pan);
		
		gtag('event','steps',{'event_category':'create account','event_label':'step1_details'});
		
		if(data.sts_code == "A02")
		{
			open_imp_popup();
			$('#alink').hide();
			
		}
		else
		{
			dms_retrieve_redirection(data);
			//close_imp_popup
			//Moengage.track_event("101_Step 2_E-KYC Initiated");
			send_data('Aadhaar Consent Button Clicked','Aadhaar_Consent');
			document.getElementById("26__Step1__mobile_verify").className="hide";
			document.getElementById("27__Step1__email_verify").className="hide";
			document.getElementById("31__Step1__pan_verify").className="hide";
			document.getElementById("34__Step1__continue").className="hide";
			
			
			$('#360__Step1__banner1_det').hide();
			$('#361__Step1__banner2_det').hide();
			$('#362__Step1__banner3_det').hide();
			$('#363__Step1__banner4_det').hide();
			$('#364__Step1__banner5_det').hide();
			$('#365__Step1__benefit_details').hide();
			$('#alink').hide();
			$('#alink').addClass('hide');
			
			//generate aadhaar otp function
			sessUpdate("dedupe_check","Clicked" );
			send_data('Dedupe button Clicked','Dedupe_button_clikced');
			$("#aadharverify_number_aadhar").val(data.uid);
			cifaadhar = $("#aadharverify_number_aadhar").val();
			var aadhaar = data.uid;
			$('.aadhaarno').val(aadhaar);
			sessUpdate("aadhaar_vfd_result","Yes");
			
			//ekyc_otp
			ekyc_redirection(data);
			aadhar_det_redirection(data);
			
			document.getElementById('28__Step1__aadhaar_verify').style.display = 'none' ;
			document.getElementById('aadhaar_info_display_div').style.display = 'none' ;
			
				
				
				if(data.sts_code == "A03")
				{
					
					$('#Step2').addClass('cont-div');
					$('#Step2').removeClass('hide');
				}
				else
				{
					address_redirection(data);
					if(data.sts_code == "A04")
					{
						$('#Step3').addClass('cont-div');
						$('#Step3').removeClass('hide');
					}
					else
					{
						profile_det_fil_redirection(data);
						
						if(data.sts_code =="A05")
						{
							$('#Step4').addClass('cont-div');
							$('#Step4').removeClass('hide');
							$("#68__Step4__account_nominee_yn").addClass("new-width");
						}
						else{
							document.getElementById('27__Step1__email_verify').style.display = "block" ;
							document.getElementById('31__Step1__pan_verify').style.display = "block" ;
							document.getElementById('34__Step1__continue').style.display = "block" ;
							
						}
						
					}
					
				}
				
			
			
			
		}
		
	}
}
	
		
	});
	
}



var sta = '';
function ekyc_redirection(data)
{
	send_data('Aadhaar OTP verification API','Syntizen_OTP_Verify');
	$("#cust_name").val(data.poi_name);
	$("#cust_relation_type").val();
	$("#cust_relation_name").val((data.poa_co != "NA") ? (data.poa_co) : "");
	$("#cust_dob").val(data.poi_db);
	$("#cust_gender").val(data.poi_gender);
	sess["oin"] = data.code;
	sess["aadhaar"] = data.uid;
	sess["aadharRef"] = data.uuid;
	
	var kycstr = data.poi_name.split(" ");
					if($("#cust_gender").val() == "M") {
						$('#cust_prefix').val("MR.");
					}
					if($("#cust_gender").val() == "F") {
						$('#cust_prefix').val("MS.");
					}
					if(kycstr.length == 1) {
						sess['first_name'] = '';
						sess["last_name"] = kycstr[0];
						sess["middle_name"] = '';
					}
					if(kycstr.length == 2) {
						sess['first_name'] = kycstr[0];
						sess["last_name"] = kycstr[1];
						sess["middle_name"] = '';
					}
					if(kycstr.length == 3) {
						sess['first_name'] = kycstr[0];
						sess["middle_name"] = kycstr[1];
						sess["last_name"] = kycstr[2];
					}
					if(kycstr.length == 4) {
						sess['first_name'] = kycstr[0];
						sess["middle_name"] = kycstr[1];
						sess["last_name"] = kycstr[2] + " " + kycstr[3];
					}
					if(kycstr.length == 5) {
						sess['first_name'] = kycstr[0];
						sess["middle_name"] = kycstr[1];
						sess["last_name"] = kycstr[2] + " " + kycstr[3]+ " " + kycstr[4];
					}
					if(kycstr.length == 6) {
						sess['first_name'] = kycstr[0];
						sess["middle_name"] = kycstr[1];
						sess["last_name"] = kycstr[2] + " " + kycstr[3]+ " " + kycstr[4]+ " " + kycstr[5];
					}
					var aadfulladdr = ((data.house != "NA") ? (data.house) + " " : "") + ((data.street != "NA") ? (data.street) + " " : "") + ((data.lm != "NA") ? (data.lm) + " " : "") + ((data.loc != "NA") ? (data.loc) + " " : "") + ((data.vtc != "NA") ? (data.vtc) + " " : "") + ((data.subdist != "NA") ? (data.subdist) + " " : "") + ((data.dist != "NA") ? (data.dist) + " " : "") + ((data.po != "NA") ? (data.po) + ", " : "");
					var splittedAddr = splitAadhaarAddress(aadfulladdr, 39);
					$("#permenant_add1").val(lines[0]);
					$("#permenant_add2").val(lines[1]);
					$("#permenant_add3").val(lines[2]);
					$("#permenant_pincode").val((data.pc != "NA") ? (data.pc) : "");
					$("#permenant_city").val((data.dist != "NA") ? (data.dist) : "");
					sta = data.state;
					$("#permenant_state option:contains(" + sta + ")").attr('selected', 'selected');
				
					//$("#aadhaarEkyc").attr('src', "data:image/jpg;base64," + result.Pht)
					
					sessUpdate("aadhaar_vfd","Aadhaar Verified");
					sess.aadharverify_number_aadhar_mask = sess.aadharRef;
	                sess.aadharverify_number_aadhar = sess.aadharRef;
					sessUpdate("aadharverify_number_aadhar",sess.aadharRef);
					StoreSession();

					$(".select2-container").addClass("off");	

}

function aadhar_det_redirection(data)
{
	send_data('cust_dob','Aadhaar_Data_Submit');
	//call_aml()
	re = getCurTime();
	sess['ref'] = re;
	if(amlrefid=="")
	{	
		amlrefid=re;
		ReferenceNumber = re;
	}
	sessUpdate("AML Call","success");
	amlst="Success";
	sessUpdate("ReferenceNumber",sess.ref);
	//karza email
	send_data('step_1_email_verify','KARZA_Email');
	sessUpdate("KARZA_EMAIL","Valid" );
	//pan and name check karza
	sessUpdate("KARZA_PAN","Done" );
	$('#31__Step1__pan_verify').css('display','none');
					
	//name check()
	send_data('cust_name','KARZA_Name_Check');
	sessUpdate("KARZA_NAMECHECK","Done" );
	var panName = data.Name_As_Per_PAN;
	var scr = data.Namecheck_Score ;
	sessUpdate("Name_As_Per_PAN",panName);
	sessUpdate("Namecheck_Score",scr);
		
	if(data.pan_sts == "canvas")
	{
		var canvas = document.createElement("CANVAS");
		var ctx = canvas.getContext("2d");
		ctx.font = "20px calibre";
		ctx.fillText("Name As Per Aadhaar:", 10, 25);
		ctx.fillText($("#cust_name").val(), 20, 50);
		ctx.fillText("Name As Per PAN:", 10, 75);
		ctx.fillText(panName, 20, 100);
		ctx.fillText("Score : " +scr, 10, 125);
		sess.images.pan = canvas.toDataURL();
						
		$("#aadhaar_info_display_div").hide();
		$('#28__Step1__aadhaar_verify').addClass('hide');
		$('#28__Step1__aadhaar_verify').removeClass('custom-labeltxt');
		$('#28__Step1__aadhaar_verify').removeClass('forms-ac');
		$('#291__Step1__continue').addClass('hide');
		sessUpdate("Name_CheckScore_Canvas","CanvasImageCreated");
	}
	if(data.pan_sts == "upload")
	{
		sessUpdate("PAN_Image_Click","Clicked");
		sessUpdate("PAN_Image_upload","Uploaded");
	}
	
	
	
		google_step1_aadhar_detail();
	
	steps_progress("S1");
	fill_comm_addr();			
}

function address_redirection(data)
{
	var addr_type_re = data.comm_add_type;
	var add_ar = data.Communication_Address ;
								var add_arr = add_ar.split("@@");
								
								document.getElementById("comm_add_add1").value = add_arr[0];
								document.getElementById("comm_add_add2").value = add_arr[1];
								document.getElementById("comm_add_add3").value = add_arr[2];
								document.getElementById("comm_add_pincode").value = add_arr[3];
								document.getElementById("comm_add_city").value = add_arr[4];
								 var ta_sta = add_arr[5];
								console.log(ta_sta);
								$("#comm_add_state option:contains(" + ta_sta + ")").attr('selected', 'selected');
								
	var cust_name = sess.cust_name.toUpperCase();
	var first_name = sess.first_name.toUpperCase();
	
	if(addr_type_re == "same")
	{
		document.getElementById("rd-same_as_comm_address_yn").value = "Same_As_Permenant_Address" ;
		
		
		
		sess['comm_add_state']=$("#select2-comm_add_state-container").text();
		sessUpdate("comm_add_state",sess.permenant_state);
		sessUpdate("comm_add_type","same");
		StoreSession();
		$('#Step2').addClass('hide');
		$('#Step2').removeClass('cont-div'); 
	}
	if(addr_type_re == "different")
	{
		
		document.getElementById("rd-same_as_comm_address_yn").value = "Other" ;
		document.getElementById("input_address_proof_number").value = data.comm_add_num;
		document.getElementById("input_address_proof_type").value = data.comm_add_numtype ;
		//$("#comm_add_state option:contains(" + ta_sta + ")").attr('selected', 'selected');
		
		
		
					
		sess['comm_add_state']=ta_sta;
		
		sess['input_address_proof_number'] = data.comm_add_num ;
			sessUpdate("comm_add_type","different");
			sessUpdate("comm_add_num" ,data.comm_add_num );
			sessUpdate("comm_add_numtype" ,data.comm_add_numtype  );
			
			StoreSession();
			$('#Step2').addClass('hide');
		$('#Step2').removeClass('cont-div'); 
		
	}	
	
	
	sessUpdate("Communication_Address",data.Communication_Address);
   $('#profile_info_branch').find('option').remove().end().append('<option value="10349">SARJAPUR - FINCARE SMALL FINANCE BANK LTD.5TH FLOOR, BREN MERCURY, KAIKONDANAHALLI, SARJAPUR MAIN ROAD, BANGALORE</option>');
   get_branchdetails();
   searchProfile();
	
   steps_progress("S2");
   send_data('Step2 Communication Address','Step2');
   google_step2();
	
	
}

function profile_det_fil_redirection(data)
{
	console.log("prof");
	var prof_vals = data.Profile_Details ;
	var prof_val_arr = prof_vals.split("@@");
	
	var maritalstatus = prof_val_arr[0];
	sess['profile_info_marital_status'] = maritalstatus ;
	
	var income = prof_val_arr[1];
	$('#select2-profile_info_annual_income-container').text(income) ;
	$("#profile_info_annual_income option:contains(" + income + ")").attr('selected', 'selected');
	
	var occ = prof_val_arr[2];
	$('#select2-profile_info_occupation-container').text(occ) ;
	$("#profile_info_occupation option:contains(" + occ + ")").attr('selected', 'selected');
	
	var mname_fill = prof_val_arr[3];
	$('#profile_info_mother_first_name').val(mname_fill);
	$('#profile_info_mother_maiden_name').val(mname_fill);
	
	var branch = prof_val_arr[4];
	$('#select2-profile_info_branch-container').text(branch)  ;
	$("#profile_info_branch option:contains(" + branch + ")").attr('selected', 'selected');
	sess.selected_branch_name=$('#select2-profile_info_branch-container').text();
	
	var mlast_name = prof_val_arr[5];
	$('#profile_info_mother_last_name').val(mlast_name) ;
	
	var b_valu = prof_val_arr[6];
	$('#profile_info_branch').val(b_valu) ;
	sess['profile_info_branch'] = b_valu;
	//document.getElementById('profile_info_branch').value = b_valu ;
	
	//sessUpdate("Profile_Details","Done");
	sessUpdate("Profile_Details",prof_vals);
	
	
	
	jQuery("input[value='1021']").attr('checked', true);
    StoreSession();
	steps_progress("S3");
	send_data('Step3 Profile Information','Step3');  
	sessUpdate("Second_dedupe_check","New Customer" );	
		
			$('#Step3').addClass('hide');
			$('#Step3').removeClass('cont-div');
			$('#28__Step1__aadhaar_verify').css('display', 'none');
			$('#32__Step1__pan_img_capture').css('display', 'none');
		
		google_step3();
		
	
	
}

function dms_retrieve_redirection(data)
{
	var aadhaarEkyc_id = data.aadhaarEkyc_id ;
	var ref_val_ekyc = aadhaarEkyc_id.split("@@");
	var refno_ekyc = ref_val_ekyc[1] ;
	
	if(data.address_back_id || data.address_front_id)
	{
		if(data.address_front_id)
		{
			var address_front_id = data.address_front_id ;
			var ref_val_add = address_front_id.split("@@");
			var refno_add = ref_val_add[1] ;
			
		}
		else{
		var address_back_id = data.address_back_id ;
		var ref_val_add = address_back_id.split("@@");
		var refno_add = ref_val_add[1] ;
		}
		
		console.log(refno_ekyc);
		console.log(refno_add);
		if(refno_ekyc == refno_add )
		{
			console.log("single");
			dms_fetch_redirection(refno_ekyc);
			
		}
		else{
			console.log("diff");
			dms_fetch_redirection(refno_ekyc);
			dms_fetch_redirection(refno_add);
		}
	}
	else{
		dms_fetch_redirection(refno_ekyc);
	}
		sessUpdate("aadhaarEkyc_id",data.aadhaarEkyc_id) ;
		sessUpdate("aadhaar_front_id",data.aadhaar_front_id) ;
		sessUpdate("pan_id",data.pan_id) ;
		if(data.address_front_id)
			sessUpdate("address_front_id",data.address_front_id) ;
		if(data.address_back_id)
			sessUpdate("address_back_id",data.address_back_id) ;
		
			
		
	
	
	
}

function dms_fetch_redirection(custom_ref_num)
{
	var custreferenceno = custom_ref_num ;
	$.post('http://localhost/101_php/101_uat/public/dms_api_redirection.php', {

					request_for : 'retrieve',
					customer_reference_number : custreferenceno

				}, function(data, result) {
				   console.log(data);
				   if(JSON.parse(data).Status == "Success")
				   {
					   var obj = JSON.parse(data) ;
					   function pbaseimg(obj)
					   {
						   for(var k in obj){
							   if(obj[k] instanceof Object){
								   if(obj[k]['doc_type'] == "aadhaarEkyc")
								   {
									   sess.images.aadhaarEkyc = obj[k]['base64'] ;
									   $("#aadhaarEkyc").attr('src', sess.images.aadhaarEkyc);
								   }
								   if(obj[k]['doc_type'] == "aadhaar_front")
								   {
									   sess.images.aadhaar_front = obj[k]['base64'] ;
								   }
								   if(obj[k]['doc_type'] == "pan")
								   {
									   sess.images.pan = obj[k]['base64'] ;
								   }
								   if(obj[k]['doc_type'] == "address_front")
								   {
									   sess.images.address_front = obj[k]['base64'] ;
								   }
								   if(obj[k]['doc_type'] == "address_back")
								   {
									   sess.images.address_back = obj[k]['base64'] ;
								   }
							   }
							}
						 }
						  
						pbaseimg(obj) ;		
					}
				   
				});
}


// CIF_ACCOUNT_AML_CALLS

function acccif(creationflag='instant') {
	
	sess.images.aadhaarEkyc=sess.images.aadhaarEkyc.replace('data:image/png;base64,','');
	sess.images.aadhaarEkyc=sess.images.aadhaarEkyc.replace('data:image/jpg;base64,','');
	sess.images.aadhaarEkyc=sess.images.aadhaarEkyc.replace('data:image/jpeg;base64,','');
	
	
	var Ref_no = sess.ref;
     var aadhar_no = sess.aadharRef;
	 var aadhar = cifaadhar;
    var prefix = sess.cust_prefix.toUpperCase();
    var cust_name = sess.cust_name.toUpperCase();
    var first_name = sess.first_name.toUpperCase();
    var middle_name = sess.middle_name.toUpperCase();
    var last_name = sess.last_name.toUpperCase();
    var cust_gender = sess.cust_gender.toUpperCase();
    var cust_dbo = sess.cust_dob;
    var annum_inc = sess.profile_info_annual_income;
    var occu = sess.profile_info_occupation;
    var marital_sts = sess.profile_info_marital_status;
    var mother_maiden = $('#profile_info_mother_maiden_name').val();
    var emailID = sess.step_1_email_verify.toUpperCase();
    var registered_mobile = sess.step_1_mobile_verify;
    var pan = sess.step_1_pan_verify.toUpperCase();
    var kyc_type = "Aadhaar-OTP";
    var added_by = "";
    var oit = "20";
    var oin = sess.oin;
    var m_f_n = sess.profile_info_mother_first_name.toUpperCase();
    var m_l_n = sess.profile_info_mother_last_name.toUpperCase();
    var address1 = sess.permenant_add1.toUpperCase();
    var address2 = sess.permenant_add2.toUpperCase();
    var address3 = sess.permenant_add3.toUpperCase();
    var city = sess.permenant_city.toUpperCase();
    var state = sess.permenant_state;
    var pincode = sess.permenant_pincode;
    var cadd1 = sess.comm_add_add1.toUpperCase();
	var cadd2 = sess.comm_add_add2.toUpperCase();
	var cadd3 = sess.comm_add_add3.toUpperCase();
	var ccity = sess.comm_add_city.toUpperCase();
	var cpin = sess.comm_add_pincode;
	var cstate = sess.comm_add_state;
	var boo= sess.profile_info_branch;
	var photo=sess.images.aadhaarEkyc;
    //var signature=sess.images.signature;
	var cum_id=sess.input_address_proof_type.toUpperCase();
	var cumm_id='';
	var cumm_type='';
	var address1_c='';
	var address2_c='';
	var address3_c='';
	var city_c='';
	var state_c='';
	var pincode_c='';
	var onboarding_flag="COAO";
	
	var add_flag="current_communication";
    if((sess.input_address_proof_type).length>0)
    {
        add_flag="current";
        cumm_type=sess.input_address_proof_type.toUpperCase();
		cumm_id=sess.input_address_proof_number.toUpperCase();
		//address_flag="communication";
		address1_c=sess.comm_add_add1.toUpperCase();
		address2_c=sess.comm_add_add2.toUpperCase();
		address3_c=sess.comm_add_add3.toUpperCase();
		city_c=sess.comm_add_city.toUpperCase();
		state_c=$("#select2-comm_add_state-container").text();
		pincode_c=sess.comm_add_pincode;
	}
	
	
	product_type = sess.rd_product_select;
	branch_code = sess.profile_info_branch;
	//CustomerNumber = sess.CIF;
	name_on_card = sess.cust_name.substr(0, 20);
	nom_name = sess.input_nominee_name || "";
	nom_add1 = sess.nominee_add1 || "";
	nom_add2 = sess.nominee_add2 || "";
	nom_add3 = sess.nominee_add3 || "";
	nom_rel = sess.dropdown_nominee_relation || "";
	nom_min_gur_name = sess.text_minor_nominee_name || "";
	nom_dob = sess.date_nominee_dob || "";
	nom_min_gur_age = sess.input_minor_nominee_age || "";
	nom_min_gur_rel = sess.input_minor_nominee_relation || "";
	
	request_for = 'accountcif';
	$.post('http://localhost/101_php/101_uat/public/api.php', {
		request_for: request_for,
		
		createFlag : creationflag,
		//for cif creation
		Ref_no : Ref_no,
         aadhar_no : aadhar_no,
		aadhar : aadhar,
        prefix : prefix,
        cust_name : cust_name,
        first_name : first_name,
        middle_name : middle_name,
        last_name : last_name,
        cust_gender : cust_gender,
        cust_dbo : cust_dbo,
        annum_inc : annum_inc,
        occu : occu,
        marital_sts : marital_sts,
        mother_maiden : mother_maiden,
        emailID : emailID,
        registered_mobile : registered_mobile,
        pan : pan,
        kyc_type : kyc_type,
        added_by : added_by,
        oit : oit,
        oin : oin,
        m_f_n : m_f_n,
        boo : boo,
        m_l_n : m_l_n,
        address1 : address1,
        address2 : address2,
        address3 : address3,
        city : city,
        state : state,
        pincode : pincode,
		photo: photo,
		cumm_type: cumm_type,
		cumm_id: cumm_id,
		add_flag: add_flag,
		address1_c: address1_c,
		address2_c: address2_c,
		address3_c: address3_c,
		city_c: city_c,
		state_c: state_c,
		pincode_c: pincode_c,
		onboarding_flag: onboarding_flag,
		
		
		product_type: product_type,
		branch_code: branch_code,
		//CustomerNumber: CustomerNumber,
		name_on_card: name_on_card,
		nom_name: nom_name,
		nom_add1: nom_add1,
		nom_add2: nom_add2,
		nom_add3: nom_add3,
		nom_rel: nom_rel,
		nom_min_gur_name: nom_min_gur_name,
		nom_dob: nom_dob,
		nom_min_gur_age: nom_min_gur_age,
		nom_min_gur_rel: nom_min_gur_rel
		
		
	}, function(data, result) {
		console.log(data);
		
		if(JSON.parse(data).CIF_status == "success") {
			
			if(JSON.parse(data).Flag == "instant")
			{
			
				if(JSON.parse(data).CIF_status == "success")
				{
					console.log(data);
					sess["CIF"] = JSON.parse(data).CIF_no;
					sessUpdate("CIF","CIF Created");
					if(JSON.parse(data).Acc_creation_stats == "Successfully account created")
					{
						
						
						if(JSON.parse(data).Acc_no != "") 
						{
				
							sess["account_no"] = JSON.parse(data).Acc_no;
							$('#account_sucess_page').removeClass('hide');
							$.modal.close();
							$('#Step4').addClass('hide');
							full_kyc();
							rm_details();
							$("#label_cust_name").html(sess.cust_prefix + " " + sess.cust_name);
							$("#input_account_name").val(sess.cust_name);
							$("#input_customer_id").val(sess.CIF);
							$("#input_account_no").val(sess.account_no);
							$("#input_branch").val(sess.selected_branch_name);
							$("#label_ifsc_code").val("FSFB0000001");
							StoreSession();
							sessUpdate("Account","Account Created");
							clearInterval(myVar);
							dms_inicialize();
							facebook();
							twitter();
							var acc=$("#input_account_no").val();
							 var cif=$("#input_customer_id").val();
							 var cifaccDetails = cif+"@@"+acc;
							 sessUpdate("CIFACC",cifaccDetails);
							 Moengage.add_user_attribute("Savings Account Number",sess.account_no );
						}
						else 
						{
							$("#cif_modal_div").removeClass("hide");
										$("#cif_modal_div").addClass("modal");
										$("#cif_modal_div").attr("style","display:block");
										$("#cif_modal_div").modal({
													  escapeClose: false,
													  clickClose: false,
													  showClose: false
													});
							sessUpdate("Account Failure","Account Not Created");
							
						}
					}
					else{
						$("#cif_modal_div").removeClass("hide");
										$("#cif_modal_div").addClass("modal");
										$("#cif_modal_div").attr("style","display:block");
										$("#cif_modal_div").modal({
													  escapeClose: false,
													  clickClose: false,
													  showClose: false
													});
					sessUpdate("Account Failure","Account Not Created");
					}
				}
				else
				{
					$("#cif_modal_div").removeClass("hide");
										$("#cif_modal_div").addClass("modal");
										$("#cif_modal_div").attr("style","display:block");
										$("#cif_modal_div").modal({
													  escapeClose: false,
													  clickClose: false,
													  showClose: false
													});
					sessUpdate("CIF Failure","CIF Not Created");
				}
			}
			
			if( JSON.parse(data).Flag == "initiate")
			{
				if(JSON.parse(data).CIF_status == "success" && JSON.parse(data).Acc_creation_stats == "success" )
				{
					sessUpdate("CIFACC","CIF and Account initiated successfully");
				}
				
				
				
			}
		}
		else if(JSON.parse(data).CIF_status == "Failed")
		{
			
					$("#cif_modal_div").removeClass("hide");
										$("#cif_modal_div").addClass("modal");
										$("#cif_modal_div").attr("style","display:block");
										$("#cif_modal_div").modal({
													  escapeClose: false,
													  clickClose: false,
													  showClose: false
													});
					sessUpdate("CIF Failure","CIF Not Created");
					return false;
		}
		else {
			functionAlert("Please Retry.");
			$('#button_shownext_tnc').removeClass('button-inac');
			$('#button_shownext_tnc').addClass('button-ac');
			document.getElementById("button_shownext_tnc").disabled = false;
			
		}
		
		
		
	});
}

function accCls(){
	
	var emailID='sowmya.d@fincarebank.com';
			
			request_for = 'accClose';

	        $.post('http://localhost/101_php/101_uat/public/kyc_skip.php', {
				pan: pan,
				request_for: request_for,
				email: emailID
			}, function(data, result) {
				   console.log(data);
                
				});
}
