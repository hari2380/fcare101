<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'api/sessId','apistore/dedupedata'
    ];
	
    public function handle($request, \Closure $next)
    {

        if (
            $this->isReading($request) ||
            $this->runningUnitTests() ||
            //$this->shouldPassThrough($request) ||
            $this->tokensMatch($request)
        ) {
            return $this->addCookieToResponse($request, $next($request));
        }

        // Dumps and dies with tokens upon mismatch
        dd(
            $sessionToken = $request->session()->token(),
            $request->input('_token') ?: $request->header('X-CSRF-TOKEN')
        );

        throw new TokenMismatchException;
    }
}
