<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Session\Store;
use DB;

class apiController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private $appUrl;
    private $appId;
    private $appUser;
    private $appPassword;
    private $apiKey;
    private $apiNumber;
    private $environment;
    private $sessionID;
    private $tsessid;
    private $aadhar_api_num;
    private $dms_api_num;
    private $aadhaar_app_Name;
    private $tot_time; 
    private $dest_url;
    private $http_code;    
    private $resp_data;

    public function __construct(Request $request)
    {
        ini_set('session.gc_maxlifetime', 3600);
        ini_set("memory_limit", "-1");
        set_time_limit(0);
        ini_set('max_execution_time', '600'); 
        date_default_timezone_set('Asia/Kolkata');

        $this->appUrl = 'https://uat-esb02.fincarebank.in';
        $this->appId = '7';
        $this->appUser = 'cashops';
        $this->appPassword = '0987ytfdxc90p';
        $this->apiKey = '2018120517071266';
        $this->apiNumber = '10354_0_2';
        $this->environment = 'D';
        $this->sessionID = '';
        $this->tsessid='';
        $this->aadhar_api_num = '511253_2_5';
        $this->dms_api_num = '10354_0_1';
        $this->aadhaar_app_Name = 'aof_uat';
        $this->tot_time=""; 
        $this->dest_url="";
        $this->http_code="";        
        $this->resp_data = array();


    }

    public function print_jsonoutput($input)
    {
        return json_encode($input,true);
        
    }

    function push_to_db()
    {
        //global $acc;
        global $request;
        global $response;
        global $err;
        global $tot_time;
        global $http_code;
        global $dest_url;



        $sessid = session_id();
        $sql="insert into AOF.accrequest(request,response,error_resp,time_taken,http_status,inserted_at,url,sessid) values('".$request."','".$response."','".$err."','".$tot_time."','".$http_code."',now(),'".$dest_url."','".$sessid."')";
        $results=DB::insert("insert into aof.accrequest(request,response,error_resp,time_taken,http_status,inserted_at,url,sessid) values(?,?,?,?,?,now(),?,?)",[$request,$response,$err,$tot_time,$http_code,$dest_url,$sessid]);
        //$query = mysqli_query($acc,$sql);

        $hour = date("YmdH");

        if(!$results)
        {
            $fp=fopen("log/accreq_failed_".$hour.".txt","a+");
            fwrite($fp,$sql."\n-----------------------------------\n");
            fclose($fp);

        }
        else
        {
            $fp=fopen("log/accreq_suc_".$hour.".txt","a+");
            fwrite($fp,$sql."\n-----------------------------------\n");
            fclose($fp);

        }
    }  

    public function fund(Request $request)
    {
        $request->session()->put('token',$request->input('_token') ?: $request->header('X-CSRF-TOKEN'));
        \Session::save();
        $amount = $request->amount;

          if($amount <= 90000) {
            
                $acc = $_POST['acc'];
                $amount = $_POST['amount']*100;
                
                 $curl = curl_init();

                    curl_setopt_array($curl, array(
                      CURLOPT_URL => "https://uat-esb02.fincarebank.in/razorpay/order_initiate",
                      CURLOPT_RETURNTRANSFER => true,
                      CURLOPT_ENCODING => "",
                      CURLOPT_MAXREDIRS => 10,
                      CURLOPT_TIMEOUT => 180,
                      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                      CURLOPT_CUSTOMREQUEST => "POST",
                      CURLOPT_POSTFIELDS => "{\r\n\"APIAuthentication\": {\r\n\t  \"ApplicationID\": \"5\",\r\n      \"AppUserID\": \"NanoBanking\",\r\n      \"AppPassword\": \"V2Vsdy29t2UAxMjM=\",\r\n      \"APIKey\": \"2018120517071237\",\r\n\t  \"API_Number\": \"10354_0_1\",\r\n\t  \"Environment\": \"$environment\",\r\n\t  \"MAC_ID\": \"\",\r\n\t  \"SessionID\":\"\" \t\r\n\t},\r\n\t\"Operation\":\"DMS_INITIALIZE\",\r\n\t\"AccountNumber\":\"$acc\",\r\n\t\"Amount\":\"$amount\"\r\n}",
                      CURLOPT_HTTPHEADER => array(
                        "Cache-Control: no-cache",
                        "Content-Type: application/json",
                        "Postman-Token: 8ad9cbb6-7cd9-3ce2-2292-91bc371d2488"
                      ),
                    ));
                   $request="{\r\n\"APIAuthentication\": {\r\n\t  \"ApplicationID\": \"5\",\r\n      \"AppUserID\": \"NanoBanking\",\r\n      \"AppPassword\": \"V2Vsdy29t2UAxMjM=\",\r\n      \"APIKey\": \"2018120517071237\",\r\n\t  \"API_Number\": \"10354_0_1\",\r\n\t  \"Environment\": \"$environment\",\r\n\t  \"MAC_ID\": \"\",\r\n\t  \"SessionID\":\"\" \t\r\n\t},\r\n\t\"Operation\":\"DMS_INITIALIZE\",\r\n\t\"AccountNumber\":\"$acc\",\r\n\t\"Amount\":\"$amount\"\r\n}";
                    $response = curl_exec($curl);
                    $err = curl_error($curl);
                

                $tot_time=(curl_getinfo($curl)['total_time']); $dest_url=(curl_getinfo($curl)['url']);
                $http_code=(curl_getinfo($curl)['http_code']);
                curl_close($curl);

                if ($err) {
                  echo "cURL Error #:" . $err;
                } else {
                  echo $response;
                }
            }
            else
            echo "Amount exceeded";
    } 

    public function sendotp(Request $request)
    {
        $request->session()->put('token',$request->input('_token') ?: $request->header('X-CSRF-TOKEN'));
        \Session::save();

        $sessid=Session::get('sessid');
        //$sessid=$_SESSION["sessid"];
        //$tsessid=$sessid;
        $mobileNo = $request->mobile;
        $tsessid = $request->session;
        $sessid = Session::getId();
        $tsessid = Session::getId();
        
        if(Session::get('mobileotp') == null)
        {
           //Session::set('mobileotp',0);
           session()->put('mobileotp', 0);
        }
        session()->put('mobileotp', Session::get('mobileotp')+1);
        //Session::set('mobileotp',Session::get('mobileotp')+1);
        //$_SESSION["mobileotp"] +=1;
        \Session::save();
        
        if(Session::get('mobileotp') > 210)
        {
            $resp_data['status']      = 'Failed';
            $resp_data['error_msg']   = 'Reached Maximum attempt';
            $resp_data['stage']       = "MobileOTP"; 
        
            echo $this->print_jsonoutput($resp_data);
            die; 
        }
        if(Session::get('mobileotp') < 210){

            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $this->appUrl . "/101sendOTPSMS",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 180,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "{\r\n\"APIAuthentication\":\r\n{\r\n\"ApplicationID\":\"$this->appId\",\r\n\"AppUserID\":\"$this->appUser\",\r\n\"AppPassword\":\"$this->appPassword\",\r\n\"APIKey\":\"$this->apiKey\",\r\n\"API_Number\":\"$this->apiNumber\",\r\n\"Environment\":\"$this->environment\",\r\n\"MAC_ID\":\"\",\r\n\"SessionID\":\"a6f1b2f6-a6c9-11e9-93eb-fa163eb56d8d\"\r\n},\r\n\"Operation\":\"OTPSMS\",\r\n\"mob\":\"$mobileNo\"\r\n}",
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json",
                    //"Postman-Token: 85f3df48-c5cc-41dd-a6a3-3e592fc97b3e",
                    "cache-control: no-cache"
                ) ,
            ));
            
            $response = curl_exec($curl);
            $err = curl_error($curl);
            $tot_time=(curl_getinfo($curl)['total_time']); $dest_url=(curl_getinfo($curl)['url']);
            $http_code=(curl_getinfo($curl)['http_code']);
            curl_close($curl);

            if ($err)
        {
           
           
          $response = $err;
          $resp_data['status']      = 'Failed';
          $resp_data['error_msg']   = $err;
          $resp_data['stage']       = "MobileOTP";
          $this->push_to_db();
            
          echo $this->print_jsonoutput($resp_data);
          die;
        }
        else
        {   

          $response = json_decode($response,true);
          if($response['status']=='Success')
           {
            $resp_data['status']      = 'Success';
            $resp_data['error_msg']   = '';
            $resp_data['stage']       = "MobileOTP";
            $resp_data['res']         = $response;
            \Session::put('mob', $mobileNo);
            \Session::put('mob_ref_no', $response["ReferenceNumber"]);
            //session()->put('mob', $mobileNo);
            //session()->put('mob_ref_no', $response["ReferenceNumber"]);
            
            //dd(session()->all());
            \Session::save();
             echo $this->print_jsonoutput($resp_data);
            die; 
        
            }
            else
            {
            $resp_data['status']      = 'Failed';
            $resp_data['error_msg']   = 'Invalid response';
            $resp_data['stage']       = "MobileOTP"; 
            $resp_data['res']         = $response; 

            echo $this->print_jsonoutput($resp_data);
            die; 
            }
          } 
        
        }
    }

    public function verifyotp(Request $request)
    {
        $request->session()->put('token',$request->input('_token') ?: $request->header('X-CSRF-TOKEN'));
        \Session::save();
        
        $sessid=$request->session()->get('sessid');
        $sessid = Session::getId();
        
        //$tsessid=$sessid;
        $otp = $request->otp;
        $refNo = $request->refno;
        $tsessid = $request->session;
        $tsessid = Session::getId();
        
        if($request->session()->get('mobileotpverify') == null)
        {
            $request->session()->put('mobileotpverify', 0);
        }
        $request->session()->put('mobileotpverify',$request->session()->get('mobileotpverify')+1);
            
        \Session::save();
            if($request->session()->get('mobileotpverify') > 50)
               {
                $resp_data['status']      = 'Failed';
                $resp_data['error_msg']   = 'Reached Maximum attempt';
                $resp_data['stage']       = "MobileOTPverify"; 
            
                echo $this->print_jsonoutput($resp_data);
                die; 
              }
              //dd(session()->all());
              
              if($refNo != $request->session()->get('mob_ref_no'))
             {
                $resp_data['status']      = 'Failed';
                $resp_data['error_msg']   = 'Invalid Reference Number';
                $resp_data['stage']       = "MobileOTPverify"; 
                $resp_data['monref']       = $refNo; 
                $resp_data['sessref']       = $request->session()->get('mob_ref_no'); 
            
                echo $this->print_jsonoutput($resp_data);
                die; 
              }  

        if(Session::get('mobileotpverify') < 50){
        
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://uat-esb02.fincarebank.in/VerifyOTP",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 180,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "{\r\n\"APIAuthentication\":\r\n{\r\n\"ApplicationID\":\"$this->appId\",\r\n\"AppUserID\":\"$this->appUser\",\r\n\"AppPassword\":\"$this->appPassword\",\r\n\"APIKey\":\"$this->apiKey\",\r\n\"API_Number\":\"$this->apiNumber\",\r\n\"Environment\":\"$this->environment\",\r\n\"MAC_ID\":\"\",\r\n\"SessionID\":\"a6f1b2f6-a6c9-11e9-93eb-fa163eb56d8d\"\r\n},\r\n    \"Operation\":\"OTPVerification\",\r\n    \"otp\":\"$otp\",\r\n    \"ReferenceNumber\":\"$refNo\"\r\n}",
                 CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/json",
                "Postman-Token: 1f961190-f6a0-30d2-9254-c543471f51a1"
              ),
            ));
            
            $response = curl_exec($curl);
            $err = curl_error($curl);
            $tot_time=(curl_getinfo($curl)['total_time']); $dest_url=(curl_getinfo($curl)['url']);
            $http_code=(curl_getinfo($curl)['http_code']);
            curl_close($curl);
            if ($err)
            {
                echo "cURL Error #:" . $err;
            }
            else
            {
                $dt=json_decode($response,true);
                if($dt['status']=="success")
                {
                    session()->put('mob_verified','Yes');
                }
                $dt['refno']=Session::get('mob_ref_no');
                echo json_encode($dt);
            }
        }
    }

    public function redirection(Request $request)
    {
        $request->session()->put('token',$request->input('_token') ?: $request->header('X-CSRF-TOKEN'));
        \Session::save();
        $mobile = $request->mobile;
//$mobile = 9100470988 ;


        $sql = "SELECT CAST(JSON_REMOVE(session_vals,'$.b_i') as char) as val FROM obj_design.session_vals WHERE id = (SELECT id FROM obj_design.session_vals WHERE app_id = '3' and inserted_at > '2020-08-10 14:40:00' and session_vals like '%".$mobile."%' ORDER BY inserted_at DESC limit 1) ";
        //$sql ="SELECT CAST(JSON_REMOVE(session_vals,'$.b_i') as char) as val FROM obj_design.session_vals WHERE id = '56266' ";
        $results = DB::select($sql);
       

        if (count($results) > 0) {

            $response = array() ;
            $response = json_decode($results[0]->val,true);
            $res_out['val'] = $results[0]->val ;

            //echo $response ;

            if($response['step_1_pan_verify'] && $response['step_1_email_verify'])
            {

                $res_out['email'] = $response['step_1_email_verify'] ;
                $res_out['pan'] = $response['step_1_pan_verify'] ;
                $res_out['mode'] = $response['Channel'] ;
                
                if(array_key_exists("aadhaar_vfd",$response))
                {
                if($response['aadhaar_vfd'] == "Aadhaar Verified"  && $response['KARZA_EMAIL'] == "Done" && $response['KARZA_PAN'] == "Done" && $response['KARZA_NAMECHECK'] == "Done" && ( $response['Name_CheckScore_Canvas'] == "CanvasImageCreated" || ($response['PAN_Image_Click'] == "Clicked" && $response['PAN_Image_upload'] == "Uploaded" )) )
                {

                    $res_out['cifaadhar'] = $response['cifaadhar'];
                    $res_out['aadhar_no'] = $response['redirection_uuid'];
                    $aad_no = $res_out['aadhar_no'] ;
                    $res_out['ReferenceNumber'] = $response['ReferenceNumber'] ;
                    $res_out['Name_As_Per_PAN'] = $response['Name_As_Per_PAN'];
                    $res_out['Namecheck_Score'] = $response['Namecheck_Score'];
                    if($response['AML_Call'])
                        $res_out['AML_Call'] = $response['AML_Call'];
                    
                    //from_ekyc($aad_no);
                    
                    $port='3306';
            
                    

                    $sql = "SELECT ifnull(AES_DECRYPT(Poi_name,'Fincare'),'NA') as poi_name,ifnull(AES_DECRYPT(Poa_co,'Fincare'),'NA') as poa_co,ifnull(AES_DECRYPT(Poi_dob,'Fincare'),'NA') AS poi_db ,ifnull(AES_DECRYPT(Poi_gender,'Fincare'),'NA') as poi_gender,AES_DECRYPT(code , 'Fincare') as code,AES_DECRYPT(uuid,'Fincare') as uuid ,AES_DECRYPT(uid,'Fincare') AS uid,ifnull(AES_DECRYPT(Poa_house,'Fincare'),'NA') as house ,ifnull(AES_DECRYPT(Poa_street,'Fincare'),'NA') as street , ifnull(AES_DECRYPT(Poa_lm,'Fincare'),'NA') AS lm, ifnull(AES_DECRYPT(Poa_loc , 'Fincare'),'NA') AS loc ,ifnull(AES_DECRYPT(Poa_vtc,'Fincare'),'NA') as vtc ,ifnull(AES_DECRYPT(Poa_subdist , 'Fincare'),'NA') AS subdist,ifnull(AES_DECRYPT(Poa_po,'Fincare'),'NA') AS po,ifnull(AES_DECRYPT(Poa_pc,'Fincare'),'NA') AS pc ,ifnull(AES_DECRYPT(Poa_state ,'Fincare' ),'NA') AS state , ifnull(AES_DECRYPT(Poa_dist ,'Fincare'),'NA') as dist  FROM sms.aadhar_data WHERE AES_DECRYPT(uuid,'Fincare')  LIKE '%".$aad_no."%' OR AES_DECRYPT(uid,'Fincare') LIKE '%".$aad_no."%' ORDER BY inserted_at DESC LIMIT 1 ;";
                    $results = DB::select($sql);
                    $row=$results[0];

                    $res_out['poi_name'] = $row['poi_name'] ;
                    $res_out['poa_co'] = $row['poa_co'] ;
                    $res_out['poi_db'] = $row['poi_db'] ;
                    $res_out['poi_gender'] = $row['poi_gender'] ;
                    $res_out['code'] = $row['code'] ;
                    $res_out['uuid'] = $row['uuid'] ;
                    $res_out['uid'] = $row['uid'] ;
                    
                    $res_out['house'] = $row['house'] ;
                    $res_out['street'] = $row['street'] ;
                    $res_out['lm'] = $row['lm'] ;
                    $res_out['loc'] = $row['loc'] ;
                    $res_out['vtc'] = $row['vtc'] ;
                    $res_out['subdist'] = $row['subdist'] ;
                    $res_out['po'] = $row['po'] ;
                    $res_out['pc'] = $row['pc'] ;
                    $res_out['state'] = $row['state'] ;
                    $res_out['dist'] = $row['dist'] ;
                    
                    //mysqli_close($con_sms);
                    
                    if($response['Namecheck_Score'] < 60 )
                        $res_out['pan_sts'] = "upload" ;
                    else
                        $res_out['pan_sts'] = "canvas" ;
                    
                        
                        if($response['Communication_Address'])
                        {
                            $res_out['comm_add_state'] = $response['comm_add_state'];
                            $res_out['comm_add_type'] = $response['comm_add_type'];
                            if($res_out['comm_add_type'] == "different")
                            {
                                $res_out['comm_add_num'] = $response['comm_add_num'] ;
                                $res_out['comm_add_numtype'] = $response['comm_add_numtype'] ;
                            }
                            $res_out['Communication_Address'] = $response['Communication_Address'];
                                
                            if($response['Profile_Details'])
                            {
                                if($response['Second_dedupe_check'] == "New Customer")
                                {
                                $res_out['Profile_Details'] = $response['Profile_Details'] ;
                                $res_out['sts_code'] = 'A05';
                                if($response['CIF'] == "CIF Created" && $response['Account'] == "Account Created")
                                    $res_out['sts_code'] = 'A07';
                                }
                                else
                                    $res_out['sts_code'] = 'A04';
                            }
                            else{
                                    $res_out['sts_code'] = 'A04';
                                }
                        }
                        else
                        {
                                $res_out['sts_code'] = 'A03';
                        }
                }
                else
                {
                    
                    $res_out['sts_code'] = 'A02' ;  
                }
            }
            }
            else
                $res_out['sts_code'] = 'A01' ;
            


            //imagesavealpha
            if(array_key_exists("aadhaarEkyc_id",$response))
            {
                if($response['aadhaarEkyc_id'])
                    $res_out['aadhaarEkyc_id'] = $response['aadhaarEkyc_id'] ;
                if($response['aadhaar_front_id'])
                    $res_out['aadhaar_front_id'] = $response['aadhaar_front_id'] ;
                if($response['pan_id'])
                    $res_out['pan_id'] = $response['pan_id'] ;
                if($response['address_front_id'])
                    $res_out['address_front_id'] = $response['address_front_id'] ;
                if($response['address_back_id'])
                    $res_out['address_back_id'] = $response['address_back_id'] ;
            }
            
        }  

         else {
         $res_out['sts_code'] = "A01" ;
         $res_out['err'] = "Error - no records";
        }




        //mysqli_close($sms);
        //header('Content-Type: application/json');
        //echo json_encode($res_out);
        echo $this->print_jsonoutput($res_out);
        die; 
    }

    public function sessUpdate(Request $request)
    {
        $request->session()->put('token',$request->input('_token') ?: $request->header('X-CSRF-TOKEN'));
        \Session::save();
        //$session_id = $_SESSION["sessid"];//$_POST['session_id'];
       $inKey = $request->inKey;
       $inVal = $request->inVal;
       
       $ikey="$.".$inKey;
       $session_id = Session::getId();
        
        DB::update("update obj_design.session_vals set session_vals=JSON_SET(session_vals,?,?) where session_id=?",[$ikey,$inVal,$session_id]);
        $sql="update obj_design.session_vals set session_vals=JSON_SET(session_vals,'$.".$inKey."','".$inVal."') where session_id='".$session_id."'";
            //mysqli_query($acc,$sql);
            
        
        $fp1=fopen("log/session_update.txt","a+");
        fwrite($fp1,"--------".$session_id."<--->(".Session::getId().")"."--------\n");
        fwrite($fp1,$sql."--------\n");
        fwrite($fp1,"--------\n");

        $ikeysess="$.".$inKey."_date";
        DB::update("update obj_design.session_vals set session_vals=JSON_SET(session_vals,?,now()) where session_id=?",[$ikeysess,$session_id]);
        $sql="update obj_design.session_vals set session_vals=JSON_SET(session_vals,'$.".$inKey."_date',now()) where session_id='".$session_id."'";
        //mysqli_query($acc,$sql);
        fwrite($fp1,"--------".$session_id."<--->(".Session::getId().")"."--------\n");
        fwrite($fp1,$sql."--------\n");
        fwrite($fp1,"--------\n");
        fclose($fp1);   
        
            echo'<?xml version="1.0" encoding="UTF-8"?>
    <ns1:Responses xmlns:ns1="http://www.fiorano.com/services/rest" xmlns:ns2="http://www.fiorano.com/fesb/activity/DB1/Response" xmlns:ns4="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
      <Response>
        <Representations>
          <Representation>
            <Element>{"status": "success"}</Element>
          </Representation>
        </Representations>
      </Response>
    </ns1:Responses>';
    }

    public function sessId(Request $request)
    {
        $request->session()->put('token',$request->input('_token') ?: $request->header('X-CSRF-TOKEN'));
        \Session::save();
        $leadtime = $request->leadtime;
       $values = $request->values;
       $text = str_replace('"', "'", $values);
 
        
        $msg=str_replace("'",'"',$text);
        $sessid=date("YmdHis").uniqid();
        
        //$request->session()->regenerate();
        $sessid= Session::getId();
        
        $now = date("Y-m-d H:i:s");
        $fp1=fopen("log/session_insert.txt","a+");
        fwrite($fp1,"--------".$sessid."--".$now."--------\n");
        fwrite($fp1,"_______".$msg."_______\n");
        fclose($fp1);
        
        $sessid = Session::getId();
        $results=DB::insert("insert into obj_design.session_vals(app_id,session_id,session_vals,inserted_at) values(3,?,?,now())",[$sessid,$msg]);

        $sql="insert into obj_design.session_vals(app_id,session_id,session_vals,inserted_at) values('3','".$sessid."','".$msg."',now())";
        //$res=mysqli_query($acc,$sql);
        
        $fp1=fopen("log/session_vals_insert.txt","a+");
        fwrite($fp1,"--------".$sessid."<--->(".Session::getId().")"."--------\n");
        fwrite($fp1,$sql."--------\n");
        fwrite($fp1,"--------\n");
        fclose($fp1);
        //$_SESSION["sessid"]=$sessid;
          $tsessid=$sessid;
          
          $tsessid = $request->session;
        
        echo '<?xml version="1.0" encoding="UTF-8"?>
        <ns1:Responses xmlns:ns1="http:www.fiorano.com/services/rest" xmlns:ns2="http:www.fiorano.com/fesb/activity/DB1/Response" xmlns:ns4="http:www.w3.org/2001/XMLSchema" xmlns:xsi="http:www.w3.org/2001/XMLSchema-instance">
          <Response>
            <Representations>
              <Representation>
                <Element>{"app_id": "3", "fields": {"workflow": ""}, "session_id": "'.$sessid.'", "cur_workflow": ""}</Element>
              </Representation>
            </Representations>
          </Response>
        </ns1:Responses>';
    }

    public function aadhaardedupe(Request $request)
    {
        $request->session()->put('token',$request->input('_token') ?: $request->header('X-CSRF-TOKEN'));
        \Session::save();
        $aadhaarNo = $request->aadhaarNo;
        
        $ch = $request->ch;

        $cust_flg = "Customer exists";
        
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://uat-esb02.fincarebank.in/v2/customer_search",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 180,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\n    \"APIAuthentication\": {\n        \"ApplicationID\": \"$this->appId\",\n        \"AppUserID\": \"$this->appUser\",\n        \"AppPassword\": \"$this->appPassword\",\n        \"APIKey\": \"$this->apiKey\",\n        \"API_Number\": \"$this->apiNumber\",\n        \"Environment\": \"$this->environment\",\n        \"MAC_ID\": \"\",\n        \"SessionID\": \"\"\n    },\n    \"Operation\": \"CustomerSearch\",\n    \"ReferenceNumber\": \"\",\n    \"AadharNumber\": \"$aadhaarNo\",\n    \"Pan\": \"\",\n    \"VoterID\": \"\",\n    \"MobileNumber\": \"\",\n    \"EmailID\": \"\",\n    \"PassPortNumber\": \"\",\n    \"DrivingLicense\": \"\",\n    \"custid\": \"\",\n    \"Account\": \"\"\n}",
             CURLOPT_HTTPHEADER => array(
            "Cache-Control: no-cache",
            "Content-Type: application/json",
            "Postman-Token: 40654a5c-9235-df94-2bc7-6d0d509e5eca"
          ),
        ));
        
        $response = curl_exec($curl);

        $err = curl_error($curl);
         

        $tot_time=(curl_getinfo($curl)['total_time']); $dest_url=(curl_getinfo($curl)['url']);
        $http_code=(curl_getinfo($curl)['http_code']);
        curl_close($curl);
        
        
        $this->push_to_db();
    
    
            $channel = $ch;

            $sql="SELECT mac_id FROM bc_registration.device_master ORDER BY RAND() LIMIT 1";
            $results = DB::select($sql);
            $mac=$results[0]->mac_id;
            //$res=mysqli_query($acc,$sql);
            //$row=mysqli_fetch_assoc($res);
            session()->put('mac_id', $results[0]->mac_id);
        //Session::set('mobileotp',Session::get('mobileotp')+1);
        //$_SESSION["mobileotp"] +=1;
            \Session::save();
            

            $curl = curl_init();

            curl_setopt_array($curl, array(
              CURLOPT_URL => "https://uat-esb02.fincarebank.in/aadhaar_kyc/generateOTP",
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 180,
              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
              CURLOPT_CUSTOMREQUEST => "POST",
              CURLOPT_POSTFIELDS => "{\r\n\t\"APIAuthentication\": {\r\n\t \"ApplicationID\": \"7\",\r\n\"AppUserID\": \"cashops\",\r\n\"AppPassword\": \"0987ytfdxc90p\",\r\n\"APIKey\": \"2018120517071266\",\r\n\"API_Number\": \"511253_2_5\",\r\n\"Environment\": \"D\",\r\n\"MAC_ID\": \"\",\r\n\"SessionID\":\"1234567\"     \t\r\n\t},\r\n\t\"Operation\":\"AADHAR_OTP_GEN\",\r\n\t\"appName\":\"aof_uat\",\r\n\t\"uid\":\"$aadhaarNo\",\r\n\t\"channel\":\"$channel\"\r\n}",
              CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/json",
                "Postman-Token: 60935d2b-bd38-9ea6-c9ab-4e6ac46d9787"
              ),
            ));
        $request="{\r\n\t\"APIAuthentication\": {\r\n\t \"ApplicationID\": \"$this->appId\",\r\n\"AppUserID\": \"$this->appUser\",\r\n\"AppPassword\": \"$this->appPassword\",\r\n\"APIKey\": \"$this->apiKey\",\r\n\"API_Number\": \"$this->aadhar_api_num\",\r\n\"Environment\": \"$this->environment\",\r\n\"MAC_ID\": \"$mac\",\r\n\"SessionID\":\"\"     \t\r\n\t},\r\n\t\"Operation\":\"AADHAR_OTP_GEN\",\r\n\t\"appName\":\"aof\",\r\n\t\"uid\":\"$aadhaarNo\",\r\n\t\"channel\":\"$channel\"\r\n}";
            $response = curl_exec($curl);

            $err = curl_error($curl);
            $tot_time=(curl_getinfo($curl)['total_time']); $dest_url=(curl_getinfo($curl)['url']);
            $http_code=(curl_getinfo($curl)['http_code']);
            curl_close($curl);
            $this->push_to_db();
        
    
     if ($err) {
              echo "cURL Error #:" . $err;
            } else {
              echo $response;
            }

         
    }

    public function aadhaarotpverify(Request $request)
    {
        $request->session()->put('token',$request->input('_token') ?: $request->header('X-CSRF-TOKEN'));
        \Session::save();
        $aadhaarNo = $request->aadhaarNo;
        $otp = $request->otp;
        $mac=\Session::get('macid');
        //authentication
        $txn=$request->txn;
        
         $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://uat-esb02.fincarebank.in/aadhaar_kyc/ekyc",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 180,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "{\r\n\"APIAuthentication\": {\r\n\"ApplicationID\": \"$this->appId\",\r\n\"AppUserID\": \"$this->appUser\",\r\n\"AppPassword\": \"$this->appPassword\",\r\n\"APIKey\": \"$this->apiKey\",\r\n\"API_Number\": \"$this->aadhar_api_num\",\r\n\"Environment\": \"$this->environment\",\r\n\"MAC_ID\": \"$mac\",\r\n\"SessionID\":\"\" \t\r\n},\r\n\"appName\": \"aof\",\r\n\"Operation\" : \"AADHAR_EKYC_2_5\",\r\n\"uid\": \"$aadhaarNo\",\r\n\"ekyctype\": \"OTP\",\r\n\"pid\":\"$otp\",\r\n\"txn\":\"$txn\"\r\n}\r\n",
          CURLOPT_HTTPHEADER => array(
            "Cache-Control: no-cache",
            "Content-Type: application/json",
            "Postman-Token: cbe4cb02-1b19-53f0-77b6-824d686b8e45"
          ),
        ));
        // $request="{\r\n\"APIAuthentication\": {\r\n\"ApplicationID\": \"$this->appId\",\r\n\"AppUserID\": \"$this->appUser\",\r\n\"AppPassword\": \"$this->appPassword\",\r\n\"APIKey\": \"$this->apiKey\",\r\n\"API_Number\": \"$this->aadhar_api_num\",\r\n\"Environment\": \"$this->environment\",\r\n\"MAC_ID\": \"$mac\",\r\n\"SessionID\":\"\" \t\r\n},\r\n\"appName\": \"aof\",\r\n\"Operation\" : \"AADHAR_EKYC_2_5\",\r\n\"uid\": \"$aadhaarNo\",\r\n\"ekyctype\": \"OTP\",\r\n\"pid\":\"$otp\",\r\n\"txn\":\"$txn\"\r\n}\r\n";
        
            $response = curl_exec($curl);
            $err = curl_error($curl);
            $tot_time=(curl_getinfo($curl)['total_time']); $dest_url=(curl_getinfo($curl)['url']);
            $http_code=(curl_getinfo($curl)['http_code']);
            curl_close($curl);

            if ($err) {
              echo "cURL Error #:" . $err;
            } else {
              //echo $response;
                echo '{"SynKycRes":[{"ret":"Y","reason":"success","code":"5fa29e018fd94de985144e51069c654d","Poa":{"loc":"NA","lm":"OPP KOLIMI","subdist":"NA","pc":"521185","vtc":"Nandigama","vtcCode":"NA","street":"T B ROAD","dist":"Krishna","state":"Andhra Pradesh","co":"S/O Venkateswara Rao","house":"19-59","po":"NA"},"custom":{"addressLine1":"19-59 T B ROAD OPP KOLIMI"},"txn":"UKC:00982975daabd03aa6514ed6beebd1f003d99cfd","Poi":{"gender":"M","phone":"NA","dob":"08-12-1992","name":"Vutukuri Aravind Kumar","email":"NA"},"ttl":"2021-08-17T12:12:22","uuid":"636803971724133942","UidData":{"uid":"779122708154","tkn":"01001265nazmh3o9lM0+b7nt5yWCTuvL5tL8Cs7j+eS4+aqv8lBepqrlJU+iUhyFtrjhSEBw"},"Pht":"/9j/4AAQSkZJRgABAgAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCADIAKADASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwDpc03NFFSAZ4pCTRSUDAmkNOIpMCgBtJT8D1pMc0ANpKdijAoAbSc07FGKBDc0nNPxRigYyg5p+KTFICM5pKkIppFADKaRTyKaRmgDQ20baWlpiG7aNtLS54oGM20m01IaSgCPaaQrUtJigCPbTSBUuBSEA0CIvwoqXANG2gZDiipNtG0UAR0lSYpMUAMNNNSGkoAiNNNSkCmkUhF7FGKdjFFAxuKMU4mk5pgJikpaQkAEk4AGSTQAhIUFicAdTXD6/wDEe106drbT4RdyIcNIzYjHsMcn9PxrD8Z+NH1Jjp+mSFbRG+eZDgzEenov864XyXbJ2k0/UDfv/Huv3zki6+zocfJANoH48n9ayH1vVJPv39yR/wBdW/xqJLKVuQhxSmzkB5U8e1HMg5WXLPxTrVi4aDUbgYGNrOXH/fJyK7LRficzSRQatbrtJANxFxj3K9/wI+lef/ZJCelI9s8Z6Z/ClzJhys+hLS7gv7VLm2lSWFx8rIcj0P61NivCvD/iG80C+SWF2aLOJIS2FcH19+Ote16VqVtq+nxXlq4ZHHIB5U9wfcUAWscUhp+KQgUgIjSVIVFMIoAaaZmpMU0rQBf5oxS0UwE/CgjilooGJ+Fea/EPxIkwbQ7XJKspuH7eoUfoTXeaxfjTNIubw4/dISM+vbuO+K8NjH2q8eWTqzFm5J5NF7aisTWWmh1EkpwOoFa8VtCEwFX8qh3YAA6CpY2PocVhJtnTCKRL5KAYUVBLAH6L81TFwKUzInKjcO/FSapIyim1yMUySMEcjNXJ3VpdyrgHtVaWQkjINCB2Kc1msgyvBrQ8LeI5fDepHzEMltJxKvce4qsDzVe7iD4bHOK2jI5Zx6nvME0dxBHNEwaORQyspyCDTq4P4a6rJPb3OmzSljDh4tzZO3oQB6A4/Ou9qjMaRTDTzTT0oAYaaacaSgC/SYpc0tMBMUYpaDQM5vx0D/wiN4FAOSmcjoNwOf8APevJLNTknHevb9btDf6Je2oC7pYWC7um7HH64rxyyVRZI/8AeJOfxqZbDjuSrknGKsRqR24qqbqKNsdT7UHVokGwqfqKws+h1K3U0VtjIOF5PSkbT5FUnjHrniqMeswhcB8fUVdg1QshBYMhGD6UrPqWmuhINPLRbjxgdxVKS2B5p1xqmyMjfke1Z0mr8/u1LN9KOV9Acorce8BGcD8ahKFuO9Bu7hh80ZXPqKEn5+cVorowlZ7G98PkMfiaZmU4WBhnHQkj8u9erDkV5x4Rj8uae5B4kZV/If8A169EiOYx9K1TMOo40w08mmEigBhpKUmkzQBo7TS7DTxR+NADNppCpzTyaSmBVvLqCxtXubmTy4kGWbBP6Dk/QV49qKW4mnWxlEls0ztEwjZMKTkDBAIxnH4V6Z4ziSXwtd7+NpjII6g71HH54/GvNIYsoo64J5/Gs5ysawjfUx3kaOQRpHyTgZ6VJfJc2bmN5Th4w0JjhDLI2RkE5yABuOeeQBjnI2zp6v8ANwPehLAI4Pm7ccjFZqa6mzg2tGZMunxRLCZJFZ3QFggyUbHQ0+ziBkKBT+Bq/cRBUIVic9STS2KrGxKjNJyuVGNjIu4vLbaQT2y1P+zI2nzS20gSZMYjdeXHcjPH4dau6lCXG7aRg9KrQKpUE5I9fSnGWmpM1qU4beWSK4l3PbxBj5UcjDJHocAZ4xzgc9u1NgEjHLg8etaLWsbE7WOKDCEHB4pud2TyNI1vDupLb3lvYtGSZmyGB7/T8K9QiGIwPavKPDgT+37aVjzGG/Hgj+terxNujU+orSJhJWYpFMYVIaYaokjNNNPNNoA0s0Z4puaUUAOpQKQcinouWHpTHYxPFkDzeGL1UwNqiQ59FZWP6A15dGwVCAejGvbXiWRGjdQyspDAjOQRyK8W1C3+xX1zbKCBFIyfMQTwcHOO/FRNGsNiSKfHfj3qUvuPJ/KsxZRjrU0UxLAZrntqdKeg6ZXeRUXHJ4q1akQNtciopjiMujbWA4zzWI11mfMsz+YDwwPH5U0rg2k7s6TUBCozvBBGayILcq2c5VugqnNMZ8C4l+QHIVCealtZSxDK+FU8Lknn6k01HlJclJl3ATrkVXlfJIB4pJ7kEZ7mq28kChIJS0sbnhuMvqqsRlVU5PuSMfyNepxjEa49K808Kxk3TOR8pKgH3Gf8a9Li+4PpW8Tje440wmnmmGmIYabTjTaAL4pcZpB0p6jNMY4DipIxgZ/z2pgPtU3SP3zQMkgQyNgVw/xD0O2SBtTt4VWUSATupIDBiQDjpndgds5716FpxVMvkDjr+X+NRXmlW+tWOoWVw0ai5hZFcoGCN8xVwpIBKnBHHUdaGUj54Yjfj0pklz5I45NSXUMtpdywTp5c0TmORMg7WBwRkcHBHas+8yQNtY8upopaEj38kvBbAHWmRvbFsyJn6tTEhjABIDepz1q7Dc2kLfPCWX0FPQcVfcrzG2YfJuOO5b9KphzERsfFbVxqFhMxaG0MXsCT/MVQcRN/AB+FC0HOK3TIluC59ferIzkccVUKKkuVHWrkYLyqoAz0pmV31Ou8MggxLjrzXoCNhRXE+HYv34wOFFdmDgVaM+pKWzTSaaTSZoAUmkpCaQkUAX161JnA4qMHjpSg96YyVeT/AJ96mLDIA6Z/rUKnA+v/ANen7sken/1xTAuRTHZt3dh/If4VIlwUZlUjnjGOv3hVJDhSCe39K4zxhqkFxptwn2k5ViIoo5v9aOQdy915Y+nyA+1GpStfUzPiTbafcXP9q2NxDJP5nk3KREEZA4JI6EYwR9OmOfOpTvGO4rRlYRLJGzsS2CQDxu9Me3NZUqsuWUZHcelTKNnqN2+zsPiBB65FaUMUTqd2OOeaxknHrzUq3JC9azaKjI0yYjkR9cVQnAHTqKgW42kkcE0xrjcSTRYHJB/HnNaelyI07FioIU4JPFYrSF2wvSr9ki+aiOcKWwTmtEuhkeneG4cRGUjr0NdJmuC8KXE6Xskf2vbGDuMTf8tOucZ6YyDxjOMV3SsGXIp2a0Kkkno7j80maaaSgkUmkzSZpKBGlninA1WluIof9bIq5BIBPJ+g7/hWdceIIlhZ7WHzyhHDSCMMD6dT69vyoQ0boamyTRwJvllSNM/edgB29a5XyvEmqRIzX9vZQvnKW6HLof8AaOSDjuMU+6ifSLFIoZ5HupM/6bJh5gBju+4c/THtVWXcVybxF4njtdCkewd2uJz5MDiNgMngkEgZx2xnnFcUNA1JvCp1FowkMe+WTe2JMbjuO3HPfOSD9c1rXEryXK3uoahLO8a7FecqqpnGcAAAE4HNaNp4qtv7De2tV86UM8ZYghQDzz6/e7enXpnTm5I6DjTlUlojzN5pZd0rnl/lAB7DGf1x/kUgGRT5bZ7eQRsWKKSqknOOeR+ZNKE2muScm3qbRhbQpTWgY7hkH2qsbeVf4uPcVt+XkcVG0eOopKY3TMYwSnjcPwFKtm5PJrVaPilEXHFPnJ9mUUtxGOOvrQ5Krx161bdOTxVWQc0KWoONtDdgVrxVW3HmSTSBQqA/MBx3553A8/j0rrdAup9NkayvIZ1jY5iPlkgEDkA9+MYA965LQJ/sEsN0IzIEJJUttBJyBzz711l54jju7KMxk294jbkBwedpHynoeuOR+FdLndJMyVKVnJbI6EajZsyKLqEM52qpcBifTB5z7VYyc1ybWy+I7dkuQivFjDquC3GBk59j+dU/sOvaMqCDU/8ARVAUk/MEGegUg/oKSiu+orvsdvRWBDrk6GMTQpJv4HlMN34j/wDVUqeKNKebyXuDDIGKkSIQAR1y3Qfiamw7PscpquuS2sCrDBbpNLIZJJAGZmOMclmJ9O/asU6/qROftOPYRr/hXda/oemfa0jFsMCPP+sbg5Pv9KyIvC+nzTxoDKu51XhvU1ke1Sq4dR+E0Bq2peUirdEKgAXCKMY+grmtc1G9a9QteTk+Xj/WEDqe2a9Qi8O6WI8NCzn1MjA/oRXN63p1jDqbxraQ7VCgZXd29TWlNanFPEUltE4CTzfs8Kk7lctIoHJJPy/+y10vhnQ9WuRIi2xWLcCWZlG3OQSQTnt6VcW1H2tLkqN23y146ev86tw+IRo1462sDXd75ZTyd+1EJIO5z2xjp7kHHWtJK6I+tcr91E3iDwr9isI51LSgnM2T/F/s8cD/AA964yW2KMV645B9RU15rWqaprKXeoaojtEx22sDYjUc5AXPoSMnJ96uzwtNKGH3SAQ4+6c1m4c8LdUZc0oy5pdTKjXHXipCgap5INp54IpoXjpXGdJVaI+lRkYFXSoxVWTrgCmmBVYZPNReSZHAFXEtpJclVOByW9KswwTRSRrHFuBPzOVJA+vpW9Km5PyMKs1H1NrQ9FXUraUtuiSJR5fGQSc8n16Vm+IdOu9OjBliZR5nDDlT1wc/hTrVn1LUEayuXjktFbbPF90kkdPUcfQ/StnUdSkv7WOC7j8u4jDCVACUcHADL7deO35E7S+MmlWlCNuhyOla1fxXgK3DhipUYAx69MY7VtSa3qTwyI84lDKRho1xnseAKfY2tpC8DtbxEIVJ/djJHftXWyaBpkpy9oo9lJX+RqZrU2hXpbOJ5c2t32fvqvsFFb1lr093YxLcwWtwYsqPOiDY+n4VY1bw/YQX0ka25UNyp3nv6c+uas+HtD0/z5YZVlkBXcAz8eh6Y9RUHZOpRlDmsYuua5qMmqXAN0drbeAoGBgcA4zVbSL29XUInS7mJXLfO5cDA9DkVev9Bll1S5Ms6LhivyAscjjHb0rW8M+HbR9TKyyTMfJYnBAB+Zfb39a35oI43F+yv5D4/Eurwkh7reCf4o14/IVz2s+ItQn1KVhMF6cqg9B65r0i48KabIBtWWL3R/8AHNcTeeG7CS7mMU0+0uxVmZTxnjtWtOdNvY8+XMZ9trd4+j3i+Zm6Dp5TjClQ2QxGB7L/AI1HYBbZccs7feZuSTWtpfhVX1BES5Yo6sGyvbBP88V0i+CbRhzLMv0Yf4UpTipDSbR5ZqCbNRmwed24EH15/rWnpmoTbPJEvlSZJDAZDcdweK3NW8KWcOqTIJpiBt7r/dHtU2kaLp0WqWyzxF42badzH5iwIHT3IrJ1Ino+z5qfyMyOaV5wtyiNuYrmP5NpxkDvnOMD/GnNE2CyBivq2P6E10mv+EpFgMlgXfsAD8ynORz7HHNZa7niil2tC7qpIHPOOR+fem6cKiucSqzjoZJLOMAc1UkvUtrjyY7cXFx05bCK307/AJitu+08QWZQMY7i42tGi5LRx8856fMR+Q98Clo/hu8kaWSECRchN3QY785/TnqDWdKjGN3I0qVm9IlO41PVo7fzIpreNQpBCQqwYE9PmB4wcD2rEvL7Ub8Bbq7d4xwEUBV/IYFdtq3hu7i0uZzLCeVz8x/vD2rAHh8Febkj/tn/APXrS8V1HSp86vYbpV9dWFgot5dgOc/KG7n1qT7Xdy3iTSTyPyQ2TxjHOB0Fbuh+F47i3y9w5RXKkKoB7Hrk+vpWleeELeOxmaKabIQk7iDxjnt6VXtIGM4tSaPOftl2BgXU4HtK3+NdNY6jfG1jIvrptyjJadjyPqaiHh20z96fH+8P8K6bQvDVjPYHdJc4Ryow6+x/u+9aSnCxkk7nM6rrN+lxFI8olGzYA6jtz2we9VrbxNfRXUbr5UeDjcq5IB47k10+t+DlFo8y3pVI2BCtFk4Jx1z7jtXODw4o63h/78//AF6yvTO2lzyhY6TULiFb+d3mRFeVmG4gdSas+HdUsYdUctcqwEDfc+b+JfSiiuY7/ZqVM6GbxNYw7yyTMiDJdVGP1INefDxFaLhRFPgDA+Vf8aKK2pdTleHhY2PD3iSz/tF8w3DYiJGFXjke9dMPE9p1+z3P5J/8VRRU1PiKjh4WOO1nxNZnVrgiGfqP4V/uj3pum+ItPOo2zSF41WVD8y+jD0zRRWR6kcNDkR30evaZMDtuOg5DRsufzFc7qR046nEI7yL7PcHcyhjuGCN4GORxz7c0UVpBtM8meHhYxLq5bUdQmuFSTy5GJEgjwpAO0DcBgkAAdc8V1mg2oTS1Ix+8cswXsQdv/stFFaT+E47ask1uAnRrhiQqqFJJ/wB4Vw5urRAc3MIx23jNFFYHoYSKaZt+Hdb09baePzSSrBsBDk5Hv9K05Nfs5oXie3ugsilCSq8A8f3qKKZrVw0OZs4ceILQ9YZh+A/xrd8O+J7SNp42juDnaQMDA65PX3FFFdE/hOaOHhzHQTa1p9zZSxGUozoygPGxwex4BriP7WspDtE4z/tAj+YoormOyjh4xTsf/9k=","ts":"2020-08-17T12:12:22.723+05:30","info":"01001265nazmh3o9lM0+b7nt5yWCTuvL5tL8Cs7j+eS4+aqv8lBepqrlJU+iUhyFtrjhSEBw"}]}';
            }
    }

    public function aadhaarverify(Request $request)
    {
        $request->session()->put('token',$request->input('_token') ?: $request->header('X-CSRF-TOKEN'));
        \Session::save();
        $aadhaarNo = $_POST['aadhaarNo'];
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://uat-esb02.fincarebank.in/v2/customer_search",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 180,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\n    \"APIAuthentication\": {\n        \"ApplicationID\": \"$this->appId\",\n        \"AppUserID\": \"$this->appUser\",\n        \"AppPassword\": \"$this->appPassword\",\n        \"APIKey\": \"$this->apiKey\",\n        \"API_Number\": \"$this->apiNumber\",\n        \"Environment\": \"$this->environment\",\n        \"MAC_ID\": \"\",\n        \"SessionID\": \"\"\n    },\n    \"Operation\": \"CustomerSearch\",\n    \"AadharNumber\": \"$aadhaarNo\",\n    \"Pan\": \"\",\n    \"MobileNumber\": \"\",\n    \"cif\": \"\",\n    \"AccountNumber\": \"\"\n}",
             CURLOPT_HTTPHEADER => array(
            "Cache-Control: no-cache",
            "Content-Type: application/json",
            "Postman-Token: 40654a5c-9235-df94-2bc7-6d0d509e5eca"
          ),
        ));
        //$request="{\n    \"APIAuthentication\": {\n        \"ApplicationID\": \"$appId\",\n        \"AppUserID\": \"$appUser\",\n        \"AppPassword\": \"$appPassword\",\n        \"APIKey\": \"$apiKey\",\n        \"API_Number\": \"$apiNumber\",\n        \"Environment\": \"$environment\",\n        \"MAC_ID\": \"\",\n        \"SessionID\": \"\"\n    },\n    \"Operation\": \"CustomerSearch\",\n    \"AadharNumber\": \"$aadhaarNo\",\n    \"Pan\": \"\",\n    \"MobileNumber\": \"\",\n    \"cif\": \"\",\n    \"AccountNumber\": \"\"\n}";
        $response = curl_exec($curl);
        $err = curl_error($curl);
        $tot_time=(curl_getinfo($curl)['total_time']); $dest_url=(curl_getinfo($curl)['url']);
        $http_code=(curl_getinfo($curl)['http_code']);
        curl_close($curl);
        if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {
            echo $response;
        }
    }
    public function amlverification(Request $request)
    {
        $request->session()->put('token',$request->input('_token') ?: $request->header('X-CSRF-TOKEN'));
        \Session::save();
        $aadharNumber = $request->aadharnumber;
        $referenceNumber = $request->ReferenceNumber;
        $firstName = $request->First_Name;
        $middleName = $request->Middle_Name ? $request->Middle_Name : "";
        $lastName = $request->Last_Name;
        $add1 = $request->add1;
        $add2 = $request->add2;
        $add3 = $request->add3;
        $city = $request->city;
        $state = $request->state;
        $pin = $request->pin;
        $dob = $request->dob;
        $pan = $request->pan;

            $curl = curl_init();

            curl_setopt_array($curl, array(
            CURLOPT_URL => "https://uat-esb02.fincarebank.in/v2/amlrequest",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 130,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\n    \"APIAuthentication\": {\n        \"ApplicationID\": \"$this->appId\",\n        \"AppUserID\": \"$this->appUser\",\n        \"AppPassword\": \"$this->appPassword\",\n        \"APIKey\": \"$this->apiKey\",\n        \"API_Number\": \"$this->apiNumber\",\n        \"Environment\": \"$this->environment\",\n        \"MAC_ID\": \"\",\n        \"SessionID\": \"\"\n    },\n    \"Operation\": \"NameScreening\",\n    \"ReferenceNumber\": \"$referenceNumber\",\n    \"First_Name\": \"$firstName\",\n    \"Middle_Name\": \"$middleName\",\n    \"Last_Name\": \"$lastName\",\n    \"Date_of_Birth\": \"$dob\",\n    \"AADHAR\": \"$aadharNumber\",\n    \"PAN\": \"$pan\",\n    \"Passport_Number\": \"\",\n    \"Issuing_Country\": \"\",\n    \"Expiry_Date\": \"\",\n    \"Address1\": \"$add1\",\n    \"Address2\": \"$add2\",\n    \"Address3\": \"$add3\",\n    \"City\": \"$city\",\n    \"State\": \"$state\",\n    \"Country\": \"\",\n    \"Zipcode\": \"$pin\",\n    \"Identification_Type\": \"\",\n    \"Identification_Number\": \"\"\n}",
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "Content-Type: application/json",
                "Postman-Token: c56bb243-b405-49ea-ae2d-fecbd0950fff"
            ) ,
        ));
        //$request="{\n    \"APIAuthentication\": {\n        \"ApplicationID\": \"$this->appId\",\n        \"AppUserID\": \"$this->appUser\",\n        \"AppPassword\": \"$this->appPassword\",\n        \"APIKey\": \"$this->apiKey\",\n        \"API_Number\": \"$this->apiNumber\",\n        \"Environment\": \"$this->environment\",\n        \"MAC_ID\": \"\",\n        \"SessionID\": \"\"\n    },\n    \"Operation\": \"NameScreening\",\n    \"ReferenceNumber\": \"$referenceNumber\",\n    \"First_Name\": \"$firstName\",\n    \"Middle_Name\": \"$middleName\",\n    \"Last_Name\": \"$lastName\",\n    \"Date_of_Birth\": \"$dob\",\n    \"AADHAR\": \"$aadharNumber\",\n    \"PAN\": \"$pan\",\n    \"Passport_Number\": \"\",\n    \"Issuing_Country\": \"\",\n    \"Expiry_Date\": \"\",\n    \"Address1\": \"$add1\",\n    \"Address2\": \"$add2\",\n    \"Address3\": \"$add3\",\n    \"City\": \"$city\",\n    \"State\": \"$state\",\n    \"Country\": \"\",\n    \"Zipcode\": \"$pin\",\n    \"Identification_Type\": \"\",\n    \"Identification_Number\": \"\"\n}";
        $response = curl_exec($curl);
        //die;
        $err = curl_error($curl);
        $tot_time=(curl_getinfo($curl)['total_time']); $dest_url=(curl_getinfo($curl)['url']);
        $http_code=(curl_getinfo($curl)['http_code']);
        curl_close($curl);

        /*if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {
            echo $response;
        }*/
        echo "success";
    }

    public function panverify(Request $request)
    {
        $request->session()->put('token',$request->input('_token') ?: $request->header('X-CSRF-TOKEN'));
        \Session::save();
        $pan = $request->pan;

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://uat-esb02.fincarebank.in/karzaPan",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 180,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\n    \"APIAuthentication\": {\n        \"ApplicationID\": \"5\",\n        \"AppUserID\": \"NanoBanking\",\n        \"AppPassword\": \"V2Vsdy29t2UAxMjM=\",\n        \"APIKey\": \"2018120517071237\",\n        \"API_Number\": \"10354_0_2\",\n        \"Environment\": \"D\",\n        \"MAC_ID\": \"\",\n        \"SessionID\": \"716e7fbd-a487-11e9-b92c-fa163eb56d8d\"\n    },\n    \"Operation\": \"KARZA_PAN\",\n    \"PAN\": \"$pan\"\n}",
           CURLOPT_HTTPHEADER => array(
            "Cache-Control: no-cache",
            "Content-Type: application/json",
            "Postman-Token: 8f7deca5-9139-ef63-63db-49f84e594001"
          ),
        ));

    //$request="{\n    \"APIAuthentication\": {\n        \"ApplicationID\": \"5\",\n        \"AppUserID\": \"NanoBanking\",\n        \"AppPassword\": \"V2Vsdy29t2UAxMjM=\",\n        \"APIKey\": \"2018120517071237\",\n        \"API_Number\": \"10354_0_2\",\n        \"Environment\": \"D\",\n        \"MAC_ID\": \"\",\n        \"SessionID\": \"716e7fbd-a487-11e9-b92c-fa163eb56d8d\"\n    },\n    \"Operation\": \"KARZA_PAN\",\n    \"PAN\": \"$pan\"\n}";
        $response = curl_exec($curl);
        $err = curl_error($curl);
        $tot_time=(curl_getinfo($curl)['total_time']); $dest_url=(curl_getinfo($curl)['url']);
        $http_code=(curl_getinfo($curl)['http_code']);
        curl_close($curl);

        if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {
            echo $response;
        }
    }

    public function namecheck(Request $request)
    {
        $panName = $request->name1;
        $aadharName = $request->name2;
        //include("apinew.php");
       //namecheck();
       $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://uat-esb02.fincarebank.in/101_name_check",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "{\"APIAuthentication\": {\"ApplicationID\": \"9\",\"AppUserID\": \"cibApp\",\"AppPassword\": \"AK45qwerty\",\"APIKey\": \"2018120517071299\",\"API_Number\": \"16838_1_3\",\"Environment\": \"D\",\"MAC_ID\": \"\",\"SessionID\":\"716e7fbd-a487-11e9-b92c-fa163eb56d8d\"  }, \"Operation\":\"Account_enquiry\",\"name1\": \"$aadharName\",\"name2\": \"$panName\",\"type\": \"individual\",\"preset\": \"L\" } ",
          CURLOPT_HTTPHEADER => array(
            "Cache-Control: no-cache",
            "Content-Type: application/json",
            "Postman-Token: 0d19dfb8-3150-b469-66b4-6d2492d24d84"
          ),
        ));
        //$request="{\"APIAuthentication\": {\"ApplicationID\": \"9\",\"AppUserID\": \"cibApp\",\"AppPassword\": \"AK45qwerty\",\"APIKey\": \"2018120517071299\",\"API_Number\": \"16838_1_3\",\"Environment\": \"D\",\"MAC_ID\": \"\",\"SessionID\":\"716e7fbd-a487-11e9-b92c-fa163eb56d8d\"  }, \"Operation\":\"Account_enquiry\",\"name1\": \"$aadharName\",\"name2\": \"$panName\",\"type\": \"individual\",\"preset\": \"L\" } ";

        $response = curl_exec($curl);
        $err = curl_error($curl);
        
        ////////////
        $tot_time=(curl_getinfo($curl)['total_time']); 
        $dest_url=(curl_getinfo($curl)['url']);
        $http_code=(curl_getinfo($curl)['http_code']);
        curl_close($curl);
        //////////////////////////////
        $this->push_to_db();

   

        if ($err) {
        echo "cURL Error #:" . $err;
        } else {
          echo $response;
        }
    }

    public function dms_inicialize()
    {
        $appId = '5';
        $appUser = 'NanoBanking';
        $appPassword = 'V2Vsdy29t2UAxMjM=';
        $apiKey = '2018120517071237';
        $apiNumber = '10354_0_2';
        $environment = 'D';
        $sessionID = '';
        $tsessid='';
        $aadhar_api_num = '511253_2_5';
        $dms_api_num = '10354_0_1';

        $DocRefno = $_POST['Doc_Reference_Number'];
        $custReferenceNo = $_POST['customer_reference_number'];
        $Number_of_Documents = $_POST['Number_of_Documents'];

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://10.43.250.90/DMS/image_operations_direct.php",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 180,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\r\n    \"APIAuthentication\": {\r\n        \"ApplicationID\": \"$appId\",\r\n        \"AppUserID\": \"$appUser\",\r\n        \"AppPassword\": \"$appPassword\",\r\n        \"APIKey\": \"$apiKey\",\r\n        \"API_Number\": \"$dms_api_num\",\r\n        \"Environment\": \"$environment\"\r\n    },\r\n    \"Doc_Reference_Number\": \"$DocRefno\",\r\n    \"Customer_Reference_Number\": \"$custReferenceNo\",\r\n    \"Operation\": \"DMS_INITIALIZE\",\r\n    \"Number_of_Documents\": \"$Number_of_Documents\",\r\n    \"RequestXML\": \"\"\r\n}",
            CURLOPT_HTTPHEADER => array(
            "Cache-Control: no-cache",
            "Content-Type: application/json",
            "Postman-Token: fea9e0e9-8917-e9bd-f9c0-b5607d39434a"
          ),
        ));
        echo $request="{\r\n    \"APIAuthentication\": {\r\n        \"ApplicationID\": \"$appId\",\r\n        \"AppUserID\": \"$appUser\",\r\n        \"AppPassword\": \"$appPassword\",\r\n        \"APIKey\": \"$apiKey\",\r\n        \"API_Number\": \"$dms_api_num\",\r\n        \"Environment\": \"$environment\"\r\n    },\r\n    \"Doc_Reference_Number\": \"$DocRefno\",\r\n    \"Customer_Reference_Number\": \"$custReferenceNo\",\r\n    \"Operation\": \"DMS_INITIALIZE\",\r\n    \"Number_of_Documents\": \"$Number_of_Documents\",\r\n    \"RequestXML\": \"\"\r\n}";
        die;
        $response = curl_exec($curl);
        $err = curl_error($curl);
        $tot_time=(curl_getinfo($curl)['total_time']); $dest_url=(curl_getinfo($curl)['url']);
        $http_code=(curl_getinfo($curl)['http_code']);
        curl_close($curl);

        if ($err)
        {
            echo "cURL Error #:" . $err;
        }
        else
        {
            echo $response;
        }
    }
}
