<html class=" js no-touch cssanimations csstransitions">

<head>
    <style type="text/css" id="alertifyCSS">
        /* style.css */
    </style>
    <title>Open Digital Savings Account |Earn up to 7% on Savings Account Balance | Fincare 101 Account</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Cache-Control" content="no-cach<bodye, no-store">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Expires" content="0">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
        <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
    
    <script
   src="https://code.jquery.com/jquery-2.2.4.min.js"
   integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
    crossorigin="anonymous"></script>
    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
   
    <script type="text/javascript" async="" src="https://www.google-analytics.com/analytics.js"></script>
    <script type="text/javascript" async="" src="https://www.googleadservices.com/pagead/conversion_async.js"></script>
    <script type="text/javascript" charset="utf-8" async="" data-requirecontext="_" data-requiremodule="apps/FIN10/scripts/common.js" src="js/common.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="sass/obj_design.css" media="all">
    <link rel="stylesheet" type="text/css" href="sass/fincare101.css" media="all">
    <link rel="stylesheet" type="text/css" href="sass/jquery.modal.min.css" media="all">
    <link rel="stylesheet" type="text/css" href="sass/select2.min.css" media="all">
    <link rel="stylesheet" type="text/css" href="sass/datestyles.css" media="all">
    <link rel="stylesheet" type="text/css" href="sass/bootstrap.css" media="all">
    <link rel="stylesheet" type="text/css" href="sass/style.css" media="all">
    <script type="text/javascript">
      $( document ).ready(function() {
        alert($('meta[name="csrf-token"]').attr('content'));
          $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
          loadData();
          
      });
    </script>
    <script src="js/obj_design.js"></script>

    <script src="js/jquery.modal.min.js"></script>
    <script src="js/select2.min.js"></script>
    <script src="js/jquery_validate.js"></script>
    <script src="js/jquery.date-dropdowns.min.js"></script>
    <script src="https://www.googletagmanager.com/gtag/js?id=UA-103174234-1"></script>
    <script src="https://www.googletagmanager.com/gtag/js?id=AW-736579055"></script>
    <script src="https://googleads.g.doubleclick.net/pagead/viewthroughconversion/736579055/?random=1571638447698&amp;cv=9&amp;fst=1571638447698&amp;num=1&amp;bg=ffffff&amp;guid=ON&amp;resp=GooglemKTybQhCsO&amp;u_h=768&amp;u_w=1366&amp;u_ah=728&amp;u_aw=1366&amp;u_cd=24&amp;u_his=2&amp;u_tz=330&amp;u_java=false&amp;u_nplug=3&amp;u_nmime=4&amp;gtm=2ouaa0&amp;sendb=1&amp;ig=1&amp;data=event%3Dgtag.config&amp;frm=0&amp;url=https%3A%2F%2F103.210.75.185%2FAOF%2F&amp;tiba=Open%20Digital%20Savings%20Account%20%7CEarn%20up%20to%207%25%20on%20Savings%20Account%20Balance%20%7C%20Fincare%20101%20Account&amp;async=1&amp;rfmt=3&amp;fmt=4"></script>
    <script src="https://googleads.g.doubleclick.net/pagead/viewthroughconversion/736579055/?random=1571638447704&amp;cv=9&amp;fst=1571638447704&amp;num=1&amp;bg=ffffff&amp;guid=ON&amp;resp=GooglemKTybQhCsO&amp;u_h=768&amp;u_w=1366&amp;u_ah=728&amp;u_aw=1366&amp;u_cd=24&amp;u_his=2&amp;u_tz=330&amp;u_java=false&amp;u_nplug=3&amp;u_nmime=4&amp;gtm=2ouaa0&amp;sendb=1&amp;ig=1&amp;data=event%3Dgtag.config&amp;frm=0&amp;url=https%3A%2F%2F103.210.75.185%2FAOF%2F&amp;tiba=Open%20Digital%20Savings%20Account%20%7CEarn%20up%20to%207%25%20on%20Savings%20Account%20Balance%20%7C%20Fincare%20101%20Account&amp;async=1&amp;rfmt=3&amp;fmt=4"></script>
    <script src="js/custom-api.js"></script>
    
    
    <!-- ==============new added=======for vkyc confermation=========== -->
       <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
       <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
       <link rel="stylesheet" type="text/css" href="css/vkyc.css" media="all">
       <style>
        .jconfirm-box {
        background: #961d1d !important;
        }


    </style>
    
       
</head>

<body class="pri" onload=
"capture_browser_info();starttimer();filladdress(this);searchble_dd_comm_add();searchDropdown();" >

<div class="backalert" id = "Alertdiv">
      <div id ="confirm" class="confirm-msg">
        <div class="alert-msg" id="alert-msg"></div>
         <button class ="yes">OK</button>
      </div>
  </div>
  <div class="hide" id="loader_div" >
    <img src="images/loader.gif" id="spinner" style="display:none"/>
</div>
    <div id="pt-main">
        <div id="page_1">
            <div id="scr__FIN10__FirstPage__main" class="rolepage">
                <section id="page-body" class="pagebody pnb-body pnt-body etw-100" style="padding-top: 0px; padding-bottom: 0px;">
                    <div id="FIN10__FirstPage__gr_row_1" class="grb">
                        <div id="FIN10__FirstPage__gr_col_1" class=" gcb-col12    " style="">
                            <div id="FIN10__FirstPage__pl_pnl_1_div" style="" class="plt-simp ">
                                <div id="FIN10__FirstPage__ps_pls_1" class="pst-simp pri " style="">
                                    <div id="FIN10__FirstPage__ct_frm_1" class="crt-form  hor pri"> <span id="FIN10__FirstPage__sc_row_1" class="srb pri">
                                    <span id="FIN10__FirstPage__sc_col_1" class=" pri scb-col100">
                                       <form id="smart_forms" onsubmit="return false">
                                          <div id="308__Menu_container__menu_items" class="nav hide" style="">
                                             <span id="nav_menu" class="nav_menu">
                                                <ul class="navbar-nav responsive" id="myTopnav">
                                                   <li>
                                                      <p><a class="bttn" onclick="openAccount();">Open Account</a></p>
                                                   </li>
                                                   <li>
                                                      <a class="openSubMenu">Products <b class="caret"></b></a>
                                                      <ul class="subnav" id="submenuNav" hidden="">
                                                         <li><a class="submenu" onclick="priority_menu();">101 Priority</a></li>
                                                         <li><a class="submenu" onclick="first_menu();">101 First</a></li>
                                                      </ul>
                                                   </li>
                                                   <li><a onclick="offers_menu();">Offers</a></li>
                                                   <li><a onclick="about();">About 101</a></li>
                                                   <li><a onclick="faq();">FAQs</a></li>
                                                </ul>
                                                <ol class="mobile-second-menu">
                                                   <li> <a onclick="terms();"> <small> Terms and Conditions </small> </a></li>
                                                   <li> <a onclick="privacy();"> <small> Privacy Policy </small> </a></li>
                                                </ol>
                                                <span id="footer" class="footer" style="bottom:0">
                                                   <p>© 2019 | Fincare Small Finance Bank Limited</p>
                                                </span>
                                        </span>
                                    </div>
                                    <div id="319__Menu_container__about" class="hide" style="">
                                        <img src="images/end-button.png" class="close-img" onclick="closeModal();"><span class="modal-phd" style="text-align: left">About 101</span>
                                        <span id="about_content">
                                                <p>101 accounts are designed to achieve ‘anytime, anywhere, anyone’ banking.</p>
                                                <p>101 is an instant paperless account opening service that is your gateway to high interest rates of up to 7% p.a. on savings account and 7.5% on fixed deposits. What’s more? As a digital customer, you get flexibility to explore ‘pay as you go’ banking with the 101 First Account or ‘exclusive banking’ with the 101 Priority account where you can earn ‘100% returns’ with offers from marquee brands and free domestic airport lounge access!</p>
                                                <p>We believe banking should be when you want, where you want! Get a free physical debit card with exciting offers and instant access to Fincare’s superior mobile banking application to transact on the go!</p>
                                             </span>
                                    </div>
                                    <div id="320__Menu_container__fee" class="hide" style="">
                                        <img src="images/end-button.png" class="close-img" onclick="closeModal();"><span class="modal-phd" style="text-align: left">Fee and Charges</span>
                                        <span id="fee_content">
                                                <iframe frameborder="0" class="" src="101account/fee-content.html" style="width: 100%; height:100vh; overflow-x: hidden; padding: 0">
                                                   <p>Your browser does not support iframes.</p>
                                                </iframe>
                                             </span>
                                    </div>
                                    <div id="322__Menu_container__products" class="hide" style="">
                                        <img src="images/end-button.png" class="close-img" onclick="closeModal();"><span class="modal-phd" style="text-align: left">Products</span>
                                        <span id="products_content">
                                                <button class="md-button" onclick="tab(this);" id="defaultProductOpen">101 Priority</button> <button class="md-button" onclick="tab(this);" id="btn_product_first">101 First</button>
                                                <div id="101_product_priority" class="tabcontent product active" style="color: #000">
                                                   <ol>
                                                      <li>
                                                         <strong>Priority privilege</strong>
                                                         <p>Join the privilege club with 101 priority account. Maintain an average monthly balance of INR 25000 and enjoy superior interest rate on your account. Enjoy up to 7% on your savings balance and also,   open an FD from your 101 priority account to earn up to 7.5% p.a.. If FD balances exceed INR 1 Lakh, AMB charges will be waived off</p>
                                                         <p>Get RuPay Platinum debit card with unlimited benefits like free access to domestic airport lounges, personal insurance cover of INR 2 lakh and multiple offers across travel, e-commerce and entertainment with no debit card maintenance charges</p>
                                                         <p>Unlimited online Fund transfer</p>
                                                      </li>
                                                      <li>
                                                         <strong>101 Priority</strong>
                                                         <p>Join the privilege club with 101 Priority Account.</p>
                                                         <p>Unlimited online Fund Transfer.</p>
                                                      </li>
                                                      <li>
                                                         <strong>Join the Unlimited Club</strong>
                                                         <p>Enjoy unlimited online transactions.</p>
                                                         <p>Access to more than 2, 00,000 ATMs and 25, 00,000 POS all over the country.</p>
                                                      </li>
                                                      <li>
                                                         <strong>101 Priority Token</strong>
                                                         <p>Access to 30+ domestic airport lounges across the country</p>
                                                         <p>24/7 concierge services on your free platinum Rupay Debit Card Unlimited offers from marquee brands.</p>
                                                      </li>
                                                   </ol>
                                                </div>
                                                <div id="101_product_first" class="tabcontent product" style="color: #000">
                                                   <ol>
                                                      <li>
                                                         <strong>The Zero Edge</strong>
                                                         <p>Experience Smart Banking with zero balance requirements!</p>
                                                         <p>Open an Fixed Deposit online and earn up to 7.5% p.a. and enjoy up to 7% p.a. on your savings balance.</p>
                                                      </li>
                                                      <li>
                                                         <strong>The 101 Card Bonus</strong>
                                                         <p>Delve into the debit card delight with your free Gold Card (RuPay Classic) and pay no maintenance charges for the first year!</p>
                                                         <p>Whats more enjoy offers across travel, e-commerce, and entertainment brands with personal insurance cover of INR 1 Lakh.</p>
                                                      </li>
                                                      <li>
                                                         <strong>Transact on the GO!</strong>
                                                         <p>ATM Ahoy! - Access to more than 2,00,000 ATMs and 25,00,000 PoS in the country.</p>
                                                         <p>Bank on your phone! - Online FD opening, bill pay, flexible fund transfers, and much more on the tap of Button with Fincare mobile banking.</p>
                                                      </li>
                                                   </ol>
                                                </div>
                                             </span>
                                    </div>
                                    <div id="323__Menu_container__offers" class="hide" style="">
                                        <img src="images/end-button.png" class="close-img" onclick="closeModal();"><span class="modal-phd" style="text-align: left">Offers</span>
                                        <span id="offers_content">
                                                <button class="md-button" onclick="tabOffer(this);" id="defaultOfferOpen">101 Priority</button> <button class="md-button" onclick="tabOffer(this);" id="btn_offer_first">101 First</button>
                                                <div id="101_offer_first" class="tabcontent offer" style="color: #000">
                                                   <p class="pad-top"><strong>Rupay Classic Offers</strong></p>
                                                   <ol>
                                                      <li>Personal insurance of Rs.100,000 and more</li>
                                                      <li>Entertainment</li>
                                                      <li>Travel</li>
                                                      <li>Shopping</li>
                                                   </ol>
                                                </div>
                                                <div id="101_offer_priority" class="tabcontent offer" style="color: #000">
                                                   <p class="pad-top"><strong>Rupay Platinum Offers</strong></p>
                                                   <ol>
                                                      <li>Personal insurance of Rs.100,000 and more</li>
                                                      <li>24*7 Concierge service</li>
                                                      <li>Travel</li>
                                                      <li>Shopping</li>
                                                      <li>Entertainment</li>
                                                   </ol>
                                                </div>
                                             </span>
                                    </div>
                                    <div id="332__Menu_container__faq" class="hide" style="">
                                        <img src="images/end-button.png" class="close-img" onclick="closeModal();"><span class="modal-phd" style="text-align: left">FAQs</span>
                                        <span id="faq_content">
                                                <iframe frameborder="0" class="" src="https://101.fincarebank.com/faqs-homecontent.html" style="width: 100%; height:100vh; overflow-x: hidden; padding: 0">
                                                   <p>Your browser does not support iframes.</p>
                                                </iframe>
                                             </span>
                                    </div>
                                    <div id="333__Menu_container__101priority" class="hide" style="">
                                        <img src="images/end-button.png" class="close-img" onclick="closeModal();"><span class="modal-phd" style="text-align: left">101 Priority account</span>
                                        <span id="101priority_content">
                                                <p class="prod-hd">Average Monthly Minimum Balance</p>
                                                <p 'style':="" 'text-align:="" center'="">INR 25,000</p>
                                                <p class="prod-hd">Superior Interest Rates on the 101 Priority account</p>
                                                <ol>
                                                   <li>Enjoy 6% p.a. on your balances up to INR 1 lakh</li>
                                                   <li>For balances above INR 1 Lakh*, enjoy higher interest rates with 6% p.a. up to INR 1 Lakh, and 7% p.a. for the remaining balance over and above INR 1 Lakh</li>
                                                   <li>Open an FD from your 101 Priority account and earn up to 7.5% p.a.</li>
                                                   <li>If customer linked FD balances are or exceed INR 1 Lakh, AMB charges will be waived off</li>
                                                </ol>
                                                <p class="prod-hd">RuPay Platinum Debit Card</p>
                                                <p class="prod-hd">Complimentary Lounge Program:</p>
                                                <p>Access to 30+ domestic lounges two times per calendar quarter per card.</p>
                                                <p class="prod-hd">Comprehensive Insurance Cover up to Rs.2 Lakhs:</p>
                                                <p>Personal&nbsp;Accident&nbsp;Insurance and&nbsp;Permanent&nbsp;Total&nbsp;Disability cover up to Rs.2 Lakhs.</p>
                                                <p class="prod-hd">24/7 Concierge Services:</p>
                                                <p>Avail host of referral services from travel assistance to hotel reservations to Consultancy services.</p>
                                                <p class="prod-hd">Transactions @ Zero Charge!</p>
                                                <ol>
                                                   <li>100 free transactions per month across NEFT, IMPS, RTGS</li>
                                                   <li>Free debit card issuance</li>
                                                   <li>Zero debit card maintenance charges</li>
                                                   <li>15 free ATM transactions (financial/non-financial) per month</li>
                                                </ol>
                                                <p class="prod-hd">Fund Transfers</p>
                                                <ol>
                                                   <li>100 transactions per month free across IMPS, NEFT, RTGS* and across any of the bank’s channels (online, branch, etc.)</li>
                                                   <li>
                                                      Charges if transactions exceed 100 transactions for the month:
                                                      <p class="prod-hd">IMPS:</p>
                                                      <br>
                                                      <table class="table">
                                                         <tbody>
                                                            <tr>
                                                               <td>Up to INR 25000</td>
                                                               <td>INR 2.5 per transaction</td>
                                                            </tr>
                                                            <tr>
                                                               <td>INR 25000 - INR 1 Lakh</td>
                                                               <td>INR 5 per transaction</td>
                                                            </tr>
                                                            <tr>
                                                               <td>INR 1 Lakh - INR 2 Lakh</td>
                                                               <td>INR 15 per transaction</td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                      <p class="prod-hd">NEFT:</p>
                                                      <br>
                                                      <table class="table">
                                                         <tbody>
                                                            <tr>
                                                               <td>Up to INR 10,00</td>
                                                               <td>INR 2.25 per transaction</td>
                                                            </tr>
                                                            <tr>
                                                               <td>INR 10,000 - INR 1 Lakh</td>
                                                               <td>INR 4.75 per transaction</td>
                                                            </tr>
                                                            <tr>
                                                               <td>INR 1 Lakh - INR 2 Lakh</td>
                                                               <td>INR 14.75 per transaction</td>
                                                            </tr>
                                                            <tr>
                                                               <td>Above INR 2 Lakhs</td>
                                                               <td>INR 24.75 per transaction</td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                      <p class="prod-hd">RTGS*:</p>
                                                      <br>
                                                      <table class="table">
                                                         <tbody>
                                                            <tr>
                                                               <td>INR 2 Lakhs - INR 5 Lakhs</td>
                                                               <td>INR 17 per transaction</td>
                                                            </tr>
                                                            <tr>
                                                               <td>Above INR 5 Lakhs</td>
                                                               <td>INR 44 per transaction</td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                      <br>
                                                      <p>*RTGS available only after conversion to full KYC account</p>
                                                   </li>
                                                </ol>
                                             </span>
                                    </div>
                                    <div id="334__Menu_container__101first" class="hide" style="">
                                        <img src="images/end-button.png" class="close-img" onclick="closeModal();"><span class="modal-phd" style="text-align: left">101 First account</span>
                                        <span id="101first_content">
                                                <p class="prod-hd">Zero Balance account</p>
                                                <p 'style':="" 'text-align:="" center'="">No minimum average balance requirement</p>
                                                <p class="prod-hd">Superior Interest Rates on the 101 First account</p>
                                                <ol>
                                                   <li>Enjoy up to 7% p.a. on your savings account</li>
                                                   <li>Open an FD from your 101 First account and earn up to 7.5% p.a.</li>
                                                </ol>
                                                <p class="prod-hd">RuPay Classic Debit Card</p>
                                                <p class="prod-hd">The Zero Edge!</p>
                                                <ol>
                                                   <li>Free debit card issuance</li>
                                                   <li>Zero debit card maintenance charges for the first year</li>
                                                   <li>8 free ATM transactions (financial/non-financial) per month</li>
                                                </ol>
                                                <p class="prod-hd">Fund Transfers</p>
                                                <p class="prod-hd">IMPS:</p>
                                                <table class="table">
                                                   <tbody>
                                                      <tr>
                                                         <td>Up to INR 25000</td>
                                                         <td>INR 2.5 per transaction</td>
                                                      </tr>
                                                      <tr>
                                                         <td>INR 25000 - INR 1 Lakh</td>
                                                         <td>INR 5 per transaction</td>
                                                      </tr>
                                                      <tr>
                                                         <td>INR 1 Lakh - INR 2 Lakh</td>
                                                         <td>INR 15 per transaction</td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                                <p class="prod-hd">NEFT:</p>
                                                <table class="table">
                                                   <tbody>
                                                      <tr>
                                                         <td>Up to INR 10,000</td>
                                                         <td>INR 2.25 per transaction</td>
                                                      </tr>
                                                      <tr>
                                                         <td>INR 10,000 - INR 1 Lakh</td>
                                                         <td>INR 4.75 per transaction</td>
                                                      </tr>
                                                      <tr>
                                                         <td>INR 1 Lakh - INR 2 Lakh</td>
                                                         <td>INR 14.75 per transaction</td>
                                                      </tr>
                                                      <tr>
                                                         <td>Above INR 2 Lakhs</td>
                                                         <td>INR 24.75 per transaction</td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                                <p class="prod-hd">RTGS*:</p>
                                                <table class="table">
                                                   <tbody>
                                                      <tr>
                                                         <td>INR 2 Lakhs - INR 5 Lakhs</td>
                                                         <td>INR 17 per transaction</td>
                                                      </tr>
                                                      <tr>
                                                         <td>Above INR 5 Lakhs</td>
                                                         <td>INR 44 per transaction</td>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                                <p>*RTGS available only after conversion to full KYC account</p>
                                             </span>
                                    </div>
                                   
                                   <div id="311__Terms_menu__terms" class="modal" style="display: none;">
                                    <img src="images/end-button.png" class="close-img" onclick="closeModal();"><span class="modal-phd" style="text-align: left">Terms and Conditions</span>
                                    <span id="menu_terms" class="menu_terms">
                                                                                <ol class="trems-list">
                                                                                   <li>By providing his/her details, customer agrees to the terms and conditions of the Bank as listed in hyperlink</li>
                                                                                   <li>Bank shall not be liable for any connectivity/ signal/data issues leading to non-compliance or false/incomplete information being provided by the customer leading to the incompletion of his/her application</li>
                                                                                   <li>Customer agrees that the name in the account will be the customer name as per his/her Aadhaar card</li>
                                                                                   <li>Customer herewith agrees that customer has not opened nor will open any other OTP e-KYC based account with Fincare Small Finance Bank or any other Bank.</li>
                                                                                   <li>If opened through UIDAI based OTP, it will be limited KYC account and you will be deemed a limited KYC customer (your unique customer number will be tagged as a limited KYC customer number). For limited KYC customers, the aggregate balance of all deposit accounts linked to any such customer should not exceed INR 1 Lakh. Similarly, the aggregate of all credits linked to accounts mapped to any such customer shall not exceed INR 2 Lakhs. Also, if such customer wishes to avail a term loan, the term loan(s) cannot exceed INR 60,000 in a year. These limits shall be placed for such a customer till full KYC is done.</li>
                                                                                   <li>The monthly Average Minimum Balance (AMB) to be maintained for the 101 First Account is zero</li>
                                                                                   <li>The customer herewith agrees to complete his/her full KYC by visiting the nearest Fincare Small Finance Bank/requesting a visit from the bank representative and providing biometric details within 12 months of opening such an account</li>
                                                                                   <li>The customer agrees that failure to complete full KYC within 12 months will lead to his account being closed by the bank. He/ She can proceed to withdraw funds/ use his/her account for any transaction only upon completion of full KYC</li>
                                                                                   <li>The 101 Account opening process is not available for customers who are FATCA reportable</li>
                                                                                   <li>This application is available for opening a savings account by Resident Indian individuals only</li>
                                                                                   <li>The customer agrees that such an account will be subject to regular scrutiny and that the bank shall reserve the right to completely freeze/debit freeze/ close the account at its sole discretion</li>
                                                                                   <li>vide full KYC for any reason, customer understands and agrees that the bank reserves the right to close the account and the customer will have to contact the branch withdrawing any leftover amount in his/her account</li>
                                                                                   <li>Under these Terms &amp; Conditions, the customer hereby provides consent and understands that part or full information may be shared with select partners to enable the provision of offers, discounts, etc. that the customer is eligible for.</li>
                                                                                   <li>It is expected that during the launch phase the Bank may take three months or such other reasonable period to enable customers to become Full KYC and remove restrictions on their account(s). The geography at which the customer is located will determine the ability for the bank to enable the customers to become Full KYC. In case of urgency the customers may visit a Bank Branch as listed on our website along with original Aadhaar card to become Full KYC.</li>
                                                                                </ol>
                                                                             </span>
                                </div>
                                   <div id="318__Privacy_menu__privacy" class="modal" style="display: none;">
                                    <img src="images/end-button.png" class="close-img" onclick="closeModal();"><span class="modal-phd" style="text-align: left">Privacy Policy</span>
                                    <span id="menu_privacy" class="menu_privacy">
                                                                                <h4>1. Definitions</h4>
                                                                                <p>The use of the term “Bank” is in reference to the following organization: FINCARE SMALL FINANCE BANK (FSFB). WEBSITE means website of the Bank with URL: //www.fincarebank.com and other sites that can be directly reached by Bank owned URL .USER YOU VISITOR refer to all persons who is accessing Banks various Websites.</p>
                                                                                <p>CUSTOMER refers to person doing any transactions with Bank directly or indirectly. PERSONAL INFORMATION include but not limited to , financial information including Banking related information, financial and credit information, ethnicity, caste, race or religion, health related details of the individuals, sexual orientation, medical records and history, biometric information, email, address, PAN, TAN, payment card information, photographs; provided that, any information that is freely available or accessible in public domain or furnished under the Right to Information Act, 2005 or any other law for the time being in force shall not be regarded as sensitive personal data or information for the purposes of this policy. PRIVACY POLICY refers to privacy commitment of the BANK Services as specified in this document. THIRD PARTY refers to an entity that is not the Bank or its direct customer.</p>
                                                                                <h4>2. Scope</h4>
                                                                                <p>This policy covers all users who interact with Bank and those whose personal information is collected or received or transmitted or processed or stored or dealt with or handled by Bank. This policy covers the “sensitive personal data or information” of the persons handled by Bank in any form or mode. Please note that this Privacy Policy does not extend to third party websites linked to Bank Websites, if any. Any information that is freely available or accessible in public domain or furnished under the Right to Information Act, 2005 or any other law for the time being in force shall not be regarded as personal information for the purposes of this Policy.</p>
                                                                                <h4>3. Introduction</h4>
                                                                                <p>The Bank is committed to respect the privacy and use of personal information responsibly. The Bank is guided by the regulations and best practices in the area of privacy. If customer is providing certain information with which that customer can be identified personally, then customer can be assured that it will only be used in accordance with this privacy policy.</p>
                                                                                <h4>4. Requirement for information collection</h4>
                                                                                <p>Bank collects and uses the financial information and other personal information from its customers as is required under various regulations and guidelines including the KYC Norms of the RBI. Such information is collected and used for specific business purposes or for other related purposes designated by Bank or for a lawful purpose to comply with the applicable laws and regulations.</p>
                                                                                <h4>5. Consent</h4>
                                                                                <p>By providing the personal information, the persons provide consent to the Bank to use personal information for the usage of the information for the product or services requested or applied or shown interest in and/or to enable Bank for personal verification and or process applications, requests, transactions and/or maintain records as per internal or legal or regulatory requirements and shall be used to provide the persons with the best possible services or products as also to protect interests of Bank.</p>
                                                                                <h4>6. Usage of information</h4>
                                                                                <p>Bank may use Personal Information: To allow customers to apply for products or services and evaluate customer eligibility for such products or services; To verify customer identity and/or location in order to allow access to accounts, conduct online transactions and to maintain measures aimed at preventing fraud and protecting the security of account and Personal Information; For risk control, for fraud detection and prevention, to comply with laws and regulations and to comply with other legal process and law enforcement requirements; To inform customer about important information regarding the Site, products or services for which customer apply or may be interested in applying for, or in which customer are already enrolled, changes to terms, conditions, and policies and/or other administrative information; For business purposes, including data analysis, audits, developing and improving products and services, enhancing the Site, identifying usage trends and determining the effectiveness of promotional campaigns; To allow customer to utilize features within our sites by granting Bank access to information from customer device such as contact lists, or geo-location when customer request certain services. To respond to customer inquiries and fulfill customer requests; To deliver marketing communications that we believe may be of interest to customer, including, ads or offers tailored to customer; To personalize customer experience on the site; To allow customer to use some site financial planning tools, information that customer enter into one of these planning tools may be stored for future access and use. Customer will have the option not to save the information; Bank will not be held responsible for content, information usage and privacy policies of linked site which will not be under the control of the Bank. The authenticity of the personal information provided by the customer will not be the responsibility of Bank.</p>
                                                                                <h4>7. Information protection</h4>
                                                                                <p>Bank customers have access to a broad range of products and services such as basic Banking Products, ATM, Online Banking, etc. To deliver products and services effectively and conveniently, it is extremely important that the Bank uses technology to manage and maintain certain customer information while ensuring that customer information is kept confidential and protected.Bank is committed in ensuring that the information is secure. In order to prevent unauthorised access or disclosure, Bank has put in place reasonable physical, electronic and managerial procedures to safeguard and secure the information that Bank collects.</p>
                                                                                <h4>8. Information sharing</h4>
                                                                                <p>The Information shall be shared with any external organisation or persons to enable Bank to provide services or to enable the completion or compilation of a transaction, credit reporting, or the same is necessary or required pursuant to applicable norms or pursuant to the terms and conditions applicable to such Information as agreed to with Bank or pursuant to any requirement of law or regulations or any Government or court or other relevant authority’s directions or orders. Information may be disclosed without obtaining persons prior consent, with government agencies mandated under the law where disclosure is necessary for compliance to legal obligations. Any Information may be required to be disclosed to any third party by Bank by an order under the law for the time being in force. It may be necessary to disclose the information to one or more agents and contractors of Bank and their sub-contractors, but such agents, contractors, and sub-contractors will be required to agree to use the information obtained from Bank only for specific assigned purposes and will be disposing the information in a secured manner consistent with Bank’s policies. The persons authorises Bank to exchange, share, part with all information related to the details and transaction history of the Covered Persons to its Affiliates or Banks or financial institutions or credit bureaus or agencies or participation in any telecommunication or electronic clearing network as may be required by law, customary practice, credit reporting, statistical analysis and credit scoring, verification or risk management or any of the aforesaid purposes and shall indemnify Bank for use or disclosure of this information.</p>
                                                                                <h4>9. Cookies Policy</h4>
                                                                                <p>Our site uses cookies that are text files containing small amounts of Information (It does not include personal sensitive information), which are downloaded to your device when you visit a website in order to provide a personalised browsing experience. Cookies do lots of different jobs, like allowing users to navigate between pages efficiently, remembering their preferences, and generally improving their browsing experience. These cookies collect information analytics about how users use a website, for instance often visited pages. All information collected by third party cookies is aggregated and therefore anonymous. By using our website, user agree that these types of cookies can be placed on his/her device. User is free to disable/delete these cookies by changing his/her web browser settings. FSFB is not responsible for cookies placed in the device of user by any other website and information collected thereto.</p>
                                                                                <h4>10. IP addresses</h4>
                                                                                <p>We may collect information about your computer (or mobile device), including where available your IP address, operating system, browser type etc., for system administration or for our own commercial purposes. This is statistical data about our users’ browsing actions and patterns, and does not identify any individual.</p>
                                                                                <h4>11. Retention and disposal</h4>
                                                                                <p>Bank’s current policy to retain information for so long as it is needed by the business. Since most information is in continuous use, much is retained on an indefinite basis or for such period to satisfy legal, regulatory or accounting requirements. When Bank finds that information collected or stored or transferred is no more in use and if there is no legal obligation to retain such information, Bank will determine appropriate means to dispose or to de-identify personally identifiable information in a secure manner in keeping with its legal obligations.</p>
                                                                                <h4>12. Amendments</h4>
                                                                                <p>This policy will be displayed through a link which will be embedded in Website. The customers are advised to visit the site and go through the privacy policy as Bank modifies the policy from time to time as, Bank constantly absorbs advanced technology and redefines processes.</p>
                                                                                <h4>13. Contact Us</h4>
                                                                                <p>If you have any questions about this Privacy Policy please use the <a class="contact-link">linked form</a> to contact us.</p>
                                                                             </span>
                                </div>
                                    <div id="4__top_load__top_container" class="navbar logo-pos" style="">
                                        <img src="images/1O1 Logo.svg">
                                        <img id="menu" src="images/hamburger menu icon.svg" style="float:right" onclick="togglemenu('308__Menu_container__menu_items');">
                                    </div>
                                     <div class="banner"><img src="images/101-home-banner.png" style="width:100%; height:auto;" alt=""></div>
                                    <div id="5__top_load__top_banner" class="header" style=""> 
                                    <!--<span id="label_top_banner_head" onclick="mobile_view();">Let's get started !!</span>-->
                                         <span id="img_banner_head" onclick="mobile_view();">
                                                <div class="hilights">
                                                   <div class="set1">
                                                      <div class="img-svg"><img src="images/Interest-Mobile.svg" alt=""></div>
                                                      <div class="hilights-txt">7% on savings &amp; 7.5% on FD</div>
                                                   </div>
                                                   <div class="set1">
                                                      <div class="img-svg"><img src="images/zero-maintenance.svg" alt=""></div>
                                                      <div class="hilights-txt">Zero Minimum Balance Reqd</div>
                                                   </div>
                                                   <div class="set1">
                                                      <div class="img-svg"><img src="images/Instant-Account-Opening-Mobile.svg" alt=""></div>
                                                      <div class="hilights-txt">Anytime Mobile Banking</div>
                                                   </div>
                                                </div>
                                             </span>
                                    </div>
                                    <div id="Step1" class="cont-div">
                                        <div id="273__top_progress__top_progress_steps" class="" style=""> <span id="span_top_progress_steps" onclick="mobile_view();">
                                                   <div class="step-bar">
                                                      <table border="0" cellspacing="1" cellpadding="1">
                                                         <tbody>
                                                            <tr>
                                                               <td class="steps current" id="S1">1</td>
                                                               <td class="pro-line"></td>
                                                               <td class="steps" id="S2">2</td>
                                                               <td class="pro-line"></td>
                                                               <td class="steps" id="S3">3</td>
                                                               <td class="pro-line"></td>
                                                               <td class="steps" id="S4">4</td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </div>
                                                </span>
                                        </div>
                                        <div id="275__top_progress__top_progress_timer" class="container" style="">
                                            <img src="images/TimmerIcon.svg" class="timer" style="width: auto" onclick="mobile_view();"> <span class="timetick" onclick="mobile_view();">
                                                   <label id="minutes">00</label>:<label id="seconds">00</label><script>settimer();</script>
                                                </span>
                                        </div>
                                        <div id="26__Step1__mobile_verify" class="forms-ac new-width" style=""> <span style="text-align: left" onclick="mobile_view();">
                                                   Mobile Number <script>window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag('js', new Date());gtag('config', 'AW-736579055');</script>
                                                </span>
                                            <input type="number" id="step_1_mobile_verify" name="step_1_mobile_verify" class="forms-w90" style="color : #333 !important" maxlength="10" placeholder="Enter your Mobile Number" onclick="mobile_view();" onblur="mobcheck();"  oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength); enableVerify('mobile1');">
                                            <button id="step_1_number_mobile_verify" name="step_1_number_mobile_verify" class="btn-verify-inac custom-labeltxt" disabled="true" onclick="send_otp('mobile','step_1_mobile_verify');">Verify</button>
                                        </div>
                                        <div id="27__Step1__email_verify" class="hide" style=""><span style="text-align: left" onclick="mobile_view();">Email Address</span>
                                            <input type="text" id="step_1_email_verify" name="step_1_email_verify" class="" style="color : #333 !important" maxlength="55" placeholder="Enter your Email Address" onclick="mobile_view();" onblur="send_data('step_1_email_verify','email');" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength); enableVerify('email1');">
                                            <button id="button_email_verify" name="button_email_verify" class="hide" disabled="true" onclick="email_check()">VERIFY</button>
                                        </div>
                                        <div id="31__Step1__pan_verify" class="hide" style=""><span style="text-align: left">PAN Number</span>
                                            <input type="text" id="step_1_pan_verify" name="step_1_pan_verify" class="" style="color : #333 !important;" maxlength="10" placeholder="Enter your PAN Number" onblur="send_data('step_1_pan_verify','pan'); pan_dedupe();" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);verify_number_pan(this);">
                                            <button id="step_1_idonthavepan_verify" name="step_1_idonthavepan_verify" class="btn" onclick="checklistyesno(this)">Or&nbsp;&nbsp;&nbsp;<u>I Don't have PAN</u>
                                            </button>
                                            <button id="button_pan_verify" name="button_pan_verify" class="hide" disabled="true" onclick="verify_pan();">VERIFY</button>
                                        </div>
                                        <div id="34__Step1__continue" class="hide" style="">
                                            <button id="button_continue" name="button_continue" class="button-inac" disabled="true" onclick="datasubmit(); google_step1_details();mobile_view();">CONTINUE</button>
                                        </div>
                                        <div id="tc_modal" class="hide modal-tc" style="display:none">
                                            <div id="289__Step1__pop_up" class="modal-top" style=""> <span class="otp-li-header">Important!</span>
                                                <span id="text_popup_text" name="text_popup_text">
                                                      <ol>
                                                         <li>I confirm that I don’t have an existing Aadhaar OTP based e-KYC account and that I will not open another OTP based e-KYC account until Full KYC of this account is done. Also, I confirm that I’m carrying the mobile number for OTP verification that is registered or linked with my Aadhaar number by UIDAI.</li>
                                                         <li>
                                                            I’ m opening my 101 Account instantly using Aadhaar OTP based e - KYC.As per RBI regulations,my account will have the following restrictions till I get my Full KYC done: 
                                                            <ol>
                                                               <li> Aggregate balance cannot exceed INR 90,000. </li>
                                                               <li>Aggregate credits cannot exceed INR 1,90,000.</li>
                                                            </ol>
                                                         </li>
                                                         <li> I understand that I can not avail any other borrowal account other than a term loan not exceeding INR 60,000 in a year </li>
                                                         <li> I understand that I have to schedule my Full KYC appointment with the Bank to remove the aforementioned restrictions on my account.</li>
                                                         <li> I understand that failure to get my Full KYC done within 1 year of opening my 101 account with the Bank shall invite account closure as per RBI KYC guidelines” </li>
                                                         <li>FATCA- I confirm that I am not a citizen of any country other than India (dual/multiple, including Greencard) and not a tax resident of any other country other India. I also confirm that my country of birth is India and I am not a Power of Attorney mandate holder or have any address/telephone number outside India</li>
                                                         <li>I indemnify Fincare Small Finance Bank against all losses arising due to difference in my name on Aadhaar and PAN card, if any</li>
                                                         <li>Political Exposure- I confirm that I’m not a politically exposed person (PEP) or related to one. I understand that PEPs are individuals who are or have been entrusted with prominent public function in a foreign country, e.g., Heads of States/Governments, senior politicians, senior government/judicial/military officers, senior executives of state-owned corporations, important political party officials etc. </li>
                                                      </ol>
                                                   </span>
                                            </div>
                                            <div id="296__Step1__pop_up_btns" class="bttn-tc-group" style="">
                                                <button id="button_shownext_popup" name="button_shownext_popup" class="button-tc btm-space20" onclick="close_imp_popup()">I AGREE</button>
                                                <button id="button_shownext_popup" name="button_shownext_popup" class="btn-tc btm-space20" onclick="checklistyesno(this)">I Don't Agree</button>
                                            </div>
                                        </div>
                                        <div id="28__Step1__aadhaar_verify" class="hide" style=""> <span id="channel_otp" name="channel_otp" class="custom-labeltxt" style="">Where would you like to recieve the OTP for Aadhaar e-KYC?</span>
                                            <fieldset id="spanaadharverify_radio_otp_recieve">
                                                <input type="radio" id="aadharverify_radio_otp_recieve" name="aadharverify_radio_otp_recieve" class="input-rd forms-inac new-width" style="margin 6px 3px -3px -2px;"  value="1">
                                                <label>Aadhaar Registered Mobile Number</label>
                                            </fieldset>
                                            <fieldset id="spanaadharverify_radio_otp_recieve">
                                                <input type="radio" id="aadharverify_radio_otp_recieve" name="aadharverify_radio_otp_recieve" class="input-rd forms-inac new-width" style="margin 6px 3px -3px -2px;"  value="2">
                                                <label>Aadhaar Registered EmailID</label>
                                            </fieldset> <span style="">Aadhaar Number / VID</span>
                                            <input type="number" id="aadharverify_number_aadhar_mask" name="aadharverify_number_aadhar_mask" class="forms-w90" maxlength="16" placeholder="Enter your Aadhaar/VID Number" onblur="mask_aadhaar(this);add_aadhaar();" onfocus="unmask_aadhaar()" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength); enableVerify('aad');">
                                            <input type="number" id="aadharverify_number_aadhar" name="aadharverify_number_aadhar" class="hidden" maxlength="16" placeholder="Enter your Aadhaar/VID Number" onfocus="unmask_aadhaar()" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength); enableVerify('aad');">
                                            <button id="aadharverify_button_verify" name="aadharverify_button_verify" class="btn-verify-inac custom-labeltxt" required="true" onclick="generate_aadhaardedupe_otp('aadharverify_number_aadhar','aadharverify_radio_otp_recieve');"" disabled>Verify</button>
                                        </div>
                                        <div id="branch_lead_div" class="" style="display:none">
                                            <div id="199__Branch_lead_101__lead_heading" class="nopan-head" style=""><span id="label_lead_heading"><label id="consno"></label></span>
                                            </div>
                                            <div id="305__Branch_lead_101__lead_heading_1" class="cont-leadhead" style=""><span id="label_lead_heading1" class="cont-leadhead"><label id="consno"></label></span>
                                            </div>
                                            <div id="211__Branch_lead_101__mobile_verify" class="forms-ac new-width" style=""><span style="text-align: left">Mobile Number</span>
                                                <input type="number" id="branchlead_number_mobile" name="branchlead_number_mobile" style="color : #333 !important"  maxlength="10" placeholder="Enter your Mobile Number" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength); enableVerify('mobile2');">
                                                
                                            </div>
                                            <div id="213__Branch_lead_101__email_verify" class="forms-ac new-width" style=""><span style="text-align: left">Email Address</span>
                                                <input type="text" id="branchlead_email" name="branchlead_email" style="color : #333 !important" maxlength="55" placeholder="Enter your Email Address" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength); enableVerify('email2');">
                                                
                                            </div>
                                            <div id="202__Branch_lead_101__full_name" class="forms-ac new-width" style=""><span id="label_full_name" name="label_full_name" class="">Full Name</span>
                                                <input type="text" id="input_full_name" class="" maxlength="50" placeholder="Enter Full Name" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);" onchange="validate_name(this);">
                                            </div>
                                            <div id="203__Branch_lead_101__address" class="forms-inac new-width" style=""> <span class="new-hd-inac" style="text-align: left">Communication Address</span><span style="text-align: left">Address Line 1</span>
                                                <input type="text" id="branchlead_add1" name="branchlead_add1" maxlength="40" placeholder="Enter your communication address" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);"><span style="text-align: left">Address Line 2</span>
                                                <input type="text" id="branchlead_add2" name="branchlead_add2" maxlength="40" placeholder="Enter your communication address" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);"><span style="text-align: left">Address Line 3</span>
                                                <input type="text" id="branchlead_add3" name="branchlead_add3" maxlength="40" placeholder="Enter your communication address" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);"><span style="text-align: left">Pincode</span>
                                                <input type="number" id="branchlead_pincode" name="branchlead_pincode" maxlength="6" placeholder="Enter your pincode" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);" onchange="verify_pincode(this);"><span style="text-align: left">City</span>
                                                <input type="text" id="branchlead_city" name="branchlead_city" maxlength="50" placeholder="Enter your City" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);"><span style="text-align: left">State</span>
                                                <select id="branchlead_state" name="branchlead_state"  placeholder="Select your state">
                                                    <option value="">Select Value</option>
                                                </select>
                                            </div>
                                            <div id="204__Branch_lead_101__button_submit_lead" class="" style="">
                                                <button id="button_submit_lead" name="button_submit_lead" class="button-inac" disabled="true" onclick="leadSubmit()">SUBMIT</button>
                                            </div>
                                        </div>
                                        <div id="32__Step1__pan_img_capture" class="hide" style=""><span>Upload PAN</span>
                                            <img id="pan" src="images/pan.png" name="pan" onclick="capture_image('file_pan_verify',150000);">
                                            <input type="file" id="file_pan_verify" name="file_pan_verify" style="display:none" accept="image/*" onchange="changeImage('file_pan_verify','pan',150000)">
                                        </div>
                                        <div id="aadhaar_info_display_div" class="" style="display:none">
                                            <div id="217__Display_Aadhaar_info__display_addhaar_info" class="forms-ac new-width" style=""><span>Please Confirm Your Aadhar Details</span>
                                                <img id="aadhaarEkyc" src="" name="aadhaarEkyc" class="span_on_img" onclick="capture_image(this);"><span>Prefix</span>
                                                <input type="text" id="cust_prefix" name="cust_prefix" readonly="true"><span>Name</span>
                                                <input type="text" id="cust_name" name="cust_name" readonly="true"><span>Relation Name</span>
                                                <input type="text" id="cust_relation_name" name="cust_relation_name" readonly="true"><span>DOB</span>
                                                <input type="text" id="cust_dob" name="cust_dob" readonly="true"><span>Gender</span>
                                                <input type="text" id="cust_gender" name="cust_gender" readonly="true">
                                            </div>
                                            <div id="218__Display_Aadhaar_info__address" class="forms-ac new-width" style=""> <span id="cust_gender" name="cust_gender" readonly="true"></span><span style="text-align: left">Address Line 1</span>
                                                <input type="text" id="permenant_add1" name="permenant_add1" readonly="true" maxlength="40" placeholder="Enter your communication address" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);"><span style="text-align: left">Address Line 2</span>
                                                <input type="text" id="permenant_add2" name="permenant_add2" readonly="true" maxlength="40" placeholder="" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);"><span style="text-align: left">Address Line 3</span>
                                                <input type="text" id="permenant_add3" name="permenant_add3" readonly="true" maxlength="40" placeholder="" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);"><span style="text-align: left">Pincode</span>
                                                <input type="number" id="permenant_pincode" name="permenant_pincode" readonly="true" maxlength="6" placeholder="Enter your pincode" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);" onchange="verify_pincode(this);"><span style="text-align: left">City</span>
                                                <input type="text" id="permenant_city" name="permenant_city" readonly="true" maxlength="40" placeholder="Enter your City" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);"><span style="text-align: left">State</span>
                                                <select id="permenant_state" name="permenant_state" readonly="true">
                                                    <option value="">Select Value</option>
                                                </select>
                                            </div>
                                            <div id="219__Display_Aadhaar_info__continue" class="" style="">
                                                <button id="button_continue_aad" class="button-ac" onclick="aadhar_ok();google_step1_aadhar_detail();">CONTINUE</button>
                                            </div>
                                        </div>
                                        <div id="291__Step1__continue" class="hide" style="">
                                            <button id="button_continue_aadhaar" name="button_continue_aadhaar" class="button-inac" onclick="aadhaarsubmit();google_step1_aadhaar()" disabled="disabled">CONTINUE</button>
                                        </div>
                        <div class="container">
                                        <div id="360__Step1__banner1_det" class="col-md-6 col-sm-12 col-xs-12 ban1-content" style="">
                                            <img src="images/Banner1.gif" class="img-responsive" onclick="mobile_view();"><span class="banner1-h1" onclick="mobile_view();">Fixed Deposits @7.75% p.a.</span>
                                            <span class="banner1-p1" onclick="mobile_view();">
                                                   <p class="banner1-p1">Interest Rates 7.75% p.a. are now just 5 minutes away. <br>Open 101 Account and enjoy the seamless benefits of banking, It’s so easy!!!</p>
                                                </span>
                                        </div>
                                        <div id="361__Step1__banner2_det" class="col-md-6 col-sm-12 col-xs-12 ban1-content" style="">
                                            <img src="images/Banner2.gif" class="img-responsive" onclick="mobile_view();"><span class="banner1-h1" onclick="mobile_view();">Open your 100% Account Today!</span>
                                            <span class="banner1-p1" onclick="mobile_view();">
                                                   <p class="banner1-p1">#101 Priority; Earn up to 100% return on your balance Unlock unlimited fund transfer, Superior offers and exclusive benefits, in</p>
                                                </span>
                                        </div>
                                        <div id="362__Step1__banner3_det" class="col-md-6 col-sm-12 col-xs-12 ban1-content" style="">
                                            <img src="images/Banner3.gif" class="img-responsive" onclick="mobile_view();"><span class="banner1-h1" onclick="mobile_view();">Open your account in less than 5 Minutes</span>
                                            <span class="banner1-p1" onclick="mobile_view();">
                                                   <p class="banner1-p1 col-b">Whenever you want, Wherever you want</p>
                                                </span>
                                        </div>
                                        <div id="363__Step1__banner4_det" class="col-md-6 col-sm-12 col-xs-12 ban1-content" style="">
                                            <img src="images/Banner4.gif" class="img-responsive" onclick="mobile_view();"><span class="banner1-h1" onclick="mobile_view();">Open Instant Paperless Account</span>
                                            <span class="banner1-p1" onclick="mobile_view();">
                                                   <p class="banner1-p1">Whenever you want wherever you want, Experience hassle-free seamless Banking</p>
                                                </span>
                                        </div>
                                        <div id="364__Step1__banner5_det" class="col-md-12 col-sm-12 col-xs-12 ban1-content" style="">
                                            <img src="images/Banner5.gif" class="img-responsive" onclick="mobile_view();"><span class="banner1-h1" onclick="mobile_view();">Your Bank in your Mobile Phone</span>
                                            <span class="banner1-p1" onclick="mobile_view();">
                                                   <p class="banner1-p1">Online Fixed Deposit, Bill Pay, unlimited fund transfers are just a click away with Fincare Mobile Banking </p>
                                                </span>
                                        </div>                  
                    <div id="365__Step1__benefit_details" class="" style=""> 
                                                   <section id="offer">
                                                      <div class="row">
                                                         <div class="hgroup title-noline">
                                                            <h2>Unique Benefits</h2>
                                                            <h3><span>Some Great Features</span></h3>
                                        </div>
                                        <article class="col-md-4 col-sm-6 col-xs-12 cu-bf-pad">
                                            <div class="hexagon figure">
                                        <div class="full-hexagon">
                                    <div class="hex-upper hex-box"></div>
                                    <div class="hex-middle">
                                        <img src="images/percentage.svg" height="80px" alt="icon">
                                    </div>
                                    <div class="hex-lower hex-box"></div>
                                </div>
                            </div>
                            <div class="figcaption">
                                <h4 class="benefits-icons-titles"><b class="tclr">Higher Interest Rates -</b> Upto 7% on savings Account and Upto 7.5% on FD.</h4>
                            </div>
                          </article>

                                            <article class="col-md-4 col-sm-6 col-xs-12">
                                                <div class="hexagon figure">
                                                    <div class="full-hexagon">
                                                        <div class="hex-upper hex-box"></div>
                                                        <div class="hex-middle">
                                                            <img src="images/withdraw.svg" height="80px" alt="icon">
                                                        </div>
                                                        <div class="hex-lower hex-box"></div>
                                                    </div>
                                                </div>
                                                <div class="figcaption">
                                                    <h4 class="benefits-icons-titles"><b class="tclr">Withdrawals -</b> Free withdrawals in 2, 00,000+ ATMs .</h4>
                                                </div>
                                            </article>
                                            <article class="col-md-4 col-sm-6 col-xs-12">
                                                <div class="hexagon figure">
                                                    <div class="full-hexagon">
                                                        <div class="hex-upper hex-box"></div>
                                                        <div class="hex-middle">
                                                            <img src="images/phones.svg" height="80px" alt="icon">
                                                        </div>
                                                        <div class="hex-lower hex-box"></div>
                                                    </div>
                                                </div>
                                                <div class="figcaption">
                                                    <h4 class="benefits-icons-titles"><b class="tclr">Free Transactions -</b> Enjoy Free transactions across NEFT/IMPS/RTGS</h4>
                                                </div>
                                            </article>
                                            <article class="col-md-4 col-sm-6 col-xs-12">
                                                <div class="hexagon figure">
                                                    <div class="full-hexagon">
                                                        <div class="hex-upper hex-box"></div>
                                                        <div class="hex-middle">
                                                            <img src="images/money-2.svg" height="80px" alt="icon">
                                                        </div>
                                                        <div class="hex-lower hex-box"></div>
                                                    </div>
                                                </div>
                                                <div class="figcaption">
                                                    <h4 class="benefits-icons-titles"><b class="tclr">Flexibility -</b> Flexible account change and easy upgrade to 101 Priority Account</h4>
                                                </div>
                                            </article>
                                            <article class="col-md-4 col-sm-6 col-xs-12">
                                                <div class="hexagon figure">
                                                    <div class="full-hexagon">
                                                        <div class="hex-upper hex-box"></div>
                                                        <div class="hex-middle">
                                                            <img src="images/online-shop2.svg" height="80px" alt="icon">
                                                        </div>
                                                        <div class="hex-lower hex-box"></div>
                                                    </div>
                                                </div>
                                                <div class="figcaption">
                                                    <h4 class="benefits-icons-titles"><b class="tclr">No Time Constraint -</b> Anyone, Anytime, Anywhere Banking</h4>
                                                </div>
                                            </article>
                                            <article class="col-md-4 col-sm-6 col-xs-12">
                                                <div class="hexagon figure">
                                                    <div class="full-hexagon">
                                                        <div class="hex-upper hex-box"></div>
                                                        <div class="hex-middle">
                                                            <img src="images/smartphone.svg" height="80px" alt="icon">
                                                        </div>
                                                        <div class="hex-lower hex-box"></div>
                                                    </div>
                                                </div>
                                                <div class="figcaption">
                                                    <h4 class="benefits-icons-titles"><b class="tclr">Paperless -</b> Instant and paperless Account</h4>
                                                </div>
                                            </article>
                                                       
                                                       </div>
                </section>
            
                                        </div>  
                </div>                                      
                <div id="otp_modal_div" class="modal-box" style="display:none">
                    <div id="269__OTP_Modal__modal_image" class="" style="">
                        <img src="images/smartphone.png" class="smartphone">
                    </div>
                    <div id="214__OTP_Modal__modal_validate_otp" class="otp-pw" style=""> <span>OTP has been sent to <label id="otp_sent_to"></label></span><span></span>
                        <input type="tel" id="model_otp" class="model_otp1" name="model_otp" maxlength="1" placeholder="1" onkeyup="verify_otp(this);">
                        <input type="tel" id="model_otp" class="model_otp2" name="model_otp" maxlength="1" placeholder="2" onkeyup="verify_otp(this);">
                        <input type="tel" id="model_otp" class="model_otp3" name="model_otp" maxlength="1" placeholder="3" onkeyup="verify_otp(this);">
                        <input type="tel" id="model_otp" class="model_otp4" name="model_otp" maxlength="1" placeholder="4" onkeyup="verify_otp(this);">
                        <input type="tel" id="model_otp" class="model_otp5" name="model_otp" maxlength="1" placeholder="5" onkeyup="verify_otp(this);">
                        <input type="tel" id="model_otp"  class="model_otp6" name="model_otp" maxlength="1" placeholder="6" onkeyup="verify_otp(this);">
                        <button id="button_otp_send" name="button_otp_send" class="button-popup" onclick="otp_send();">Verify</button><span id="otp_model_error" name="otp_model_error" class="error"> </span>
                        <span class="link">
                     <p class="otp-no">Didnt receive the OTP? <button id="btn-resend" class="otp-no" onclick="resendOTP1(this);resendBtn();" disabled="disabled">RESEND OTP</button></p>
                            </span>
                            
                            <input type ='hidden' value="" name="ReferenceNumber" class="ReferenceNumber">
                            <input type ='hidden' value="" name="aadhaarverifytxn" class="aadhaarverifytxn">
                            <input type ='hidden' value="" name="aadhaarno" class="aadhaarno">

                            
                        <span id="otp_model_hidden" class="link" style="display:none"> </span>
                    </div>
                </div>
                
                <div id="notification_modal_div" class="modal-tc-dbt" style="display:none">
                    <div id="352__mobile_view__mobile_content" class="" style=""><span class="" style="text-align:center">With your Mobile Phone Your Digital Savings Account is Just a few Fingertips Away!</span>
                        <span><span><br>
                    <div id="desktop_mobile" class="hide" style=""><span style="text-align: left">Mobile Number</span>
                        <input type="number" id="desktop_mobile_verify" name="desktop_mobile_verify" class="" style="color : #333 !important" maxlength="10" placeholder="Enter your Mobile Number" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength); enableVerify('mobile1');">
                                                        </div>
                    </span></span>
                        <button id="button_submit_notification" name="button_submit_notification" class="button-ac rating-chkbox" onclick="page_relaunch();">Proceed with Mobile Now!</button>
                    </div>
                </div>
                <div id="aadharotp_modal_div" class="cont-div" style="display:none">
               <span class="" style="text-align:center">Aadhaar number does not have verified mobile/email.  Thanks for showing interest in banking with us. Our Official / representative will get in touch with you</span>
                  <button id="button_submit_notification" name="button_submit_notification" class="button-ac rating-chkbox" onclick="page_relaunch();">OK</button>
                </div>
                <div id="otpflood_modal_div" class="cont-div" style="display:none">
                   <span class="" style="text-align:center">You have reached maximum attempt to get the Aadhaar OTP from UIDAI. Please try after some time.</span>
                      <button id="button_submit_notification" name="button_submit_notification" class="button-ac rating-chkbox" onclick="page_relaunch();">OK</button>
                </div>
                <div id="dedupe_modal_div" class="cont-div" style="display:none">
                    <span class="" style="text-align:center">Your relationship with Fincare SFB already exists. Please contact our branch.</span>
                        <button id="button_submit_notification" name="button_submit_notification" class="button-ac rating-chkbox" onclick="page_relaunch();">OK</button>
                </div>
                <div id="mobilededupe_modal_div" class="cont-div modal" style="display:none">
                    <span class="" style="text-align:center">You already tried opening account with us with the given Mobile number. Your Account creation process is under progress.Please contact the Bank on 'Support Number 1800 313 313' and we will assist you immediately.</span>
                        <button id="button_submit_mobile" name="button_submit_mobile" class="button-ac rating-chkbox" onclick="mob_close();">OK</button>
                </div>
                <div id="cif_modal_div" class="cont-div" style="display:none">
                    <span class="" style="text-align:center">So sorry. We're facing technical difficulty to open your account. Please contact the Bank on 'Support Number 1800 313 313' and we will assist you immediately!!.</span>
                        <button id="button_submit_notification" name="button_submit_notification" class="button-ac rating-chkbox" onclick="page_relaunch();">OK</button>
                </div>
                <div id="aml_modal_div" class="cont-div" style="display:none">
                    <span class="" style="text-align:center">Thanks for showing interest in banking with us. Our Official / representative will get in touch with you</span>
                        <button id="button_submit_notification" name="button_submit_notification" class="button-ac rating-chkbox" onclick="page_relaunch();">OK</button>
                </div>
                <div id="aadharage_modal_div" class="cont-div" style="display:none">
                    <span class="" style="text-align:center">You must be 18 years Old to Open an Account with us.</span>
                        <button id="button_submit_notification" name="button_submit_notification" class="button-ac rating-chkbox" onclick="page_relaunch();">OK</button>
                </div>
                <div id="Step2" class="hide" onload='fill_comm_addr();'>
                    <div id="36__Step2__same_as_comm_address_yn" class="forms-ac new-width" style=""> <span class="new-hd-ac" style="text-align: left">
                                                   Communication Address
                                                </span>
                        <fieldset id="spanrd-same_as_comm_address_yn">
                            <input type="radio" id="rd-same_as_comm_address_yn" name="rd-same_as_comm_address_yn" class="input-rd" onshow="filladdress(this);" onclick="filladdress(this);" value="Same_As_Permenant_Address" >
                            <label>Same As Permenant Address</label>
                        </fieldset>
                        <fieldset id="spanrd-same_as_comm_address_yn">
                            <input type="radio" id="rd-same_as_comm_address_yn" name="rd-same_as_comm_address_yn" class="input-rd" onshow="filladdress(this);" onclick="filladdress(this);" value="Other">
                            <label>Other</label>
                        </fieldset>
                    </div>
                    <div id="38__Step2__Other_Address" class="forms-ac new-width" style="display:none"> <span class="" style="text-align: left">Address Proof Type</span>
                        <select id="input_address_proof_type" name="input_address_proof_type" onchange="dropdown_verify(this);" >
                            <option value="">Select Value</option>
                        </select><span style="text-align: left">Address Proof Number</span>
                        <input type="text" id="input_address_proof_number" name="input_address_proof_number" disabled="true" maxlength="20" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);" onchange="validate_field(this);">
                    </div>
                    <div id="37__Step2__address" class="forms-ac new-width" style=""> <span id="input_address_proof_number" name="input_address_proof_number" disabled="true" maxlength="20" placeholder="Enter Your Address Proof Number" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);" onchange="validate_field(this);"></span><span style="text-align: left">Address Line 1</span>
                        <input type="text" id="comm_add_add1" name="comm_add_add1" maxlength="40" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);" onchange="validate_address(this);"><span style="text-align: left; margin-top: 10px !important;">Address Line 2</span>
                        <input type="text" id="comm_add_add2" name="comm_add_add2" maxlength="40" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);" onchange="validate_address(this);"><span style="text-align: left">Address Line 3</span>
                        <input type="text" id="comm_add_add3" name="comm_add_add3" maxlength="40" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);" onchange="validate_address(this);"><span style="text-align: left">Pincode</span>
                        <input type="number" id="comm_add_pincode" name="comm_add_pincode" maxlength="6" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);" onchange="verify_pincode(this);"><span style="text-align: left">City</span>
                        <input type="text" id="comm_add_city" name="comm_add_city" maxlength="50" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);" onchange="validate_name(this);"><span style="text-align: left; margin-top: 10px !important;">State</span>
                        <select id="comm_add_state" name="comm_add_state">
                            <option value="">Select Value</option>
                        </select>
                    </div>
                    <div id="41__Step2__Other_Address_Image" class="span_on_img" style="display:none"><span>Address Proof Image 1</span>
                        <img id="address_front" src="images/address.png" name="address_front" onclick="capture_image('file_other_address1');">
                        <input type="file" id="file_other_address1" name="file_other_address1" style="display:none" accept="image/*" onchange="changeImage('file_other_address1','address_front')"><span>Address Proof Image 2</span>
                        <img id="address_back" src="images/address.png" name="address_back" onclick="capture_image('file_other_address2');">
                        <input type="file" id="file_other_address2" name="file_other_address2" style="display:none" accept="image/*" onchange="changeImage('file_other_address2','address_back')"><span class="hide">Address Proof Image 3</span>
                        <img id="address_add" src="images/address.png" name="address_add" style="display:none" onclick="capture_image('file_other_address3');">
                        <input type="file" id="file_other_address3" name="file_other_address3" style="display:none" accept="image/*" onchange="changeImage('file_other_address3','address_add')">
                    </div>
                    <div id="44__Step2__continue" class="" style="">
                        <button id="button_continue" class="button-ac" onclick="addressSubmit();google_step2()">CONTINUE
                            <script>
                                gtag('event','steps',{'event_category':'create account','event_label':'step2'});
                            </script>
                        </button>
                    </div>
                </div>
                <div id="Step3" class="hide">
                    <div class="forms-ac new-width"> <span class="new-hd-ac" style="text-align: left">Profile Information</span>
                        <span style="text-align: left">Annual Income</span>
                        <select id="profile_info_annual_income" name="profile_info_annual_income" placeholder="Select your state">
                            <option value="">Select Value</option>
                        </select>   <span style="text-align: left">Occupation</span>
                        <select id="profile_info_occupation" name="profile_info_occupation" placeholder="Select your state">
                            <option value="">Select Value</option>
                         <option value="SPL">SALARIED- PVT SECTOR</option>
                         <option value="SPU">SALARIED- PUBLIC SECTOR</option>
                         <option value="12">SELF EMPLOYED</option>
                         <option value="HW">HOME MAKER</option>
                         <option value="RS">RETIRED</option>
                         <option value="ST">STUDENT</option>
                         <option value="14">Others</option>
                        </select>   
                        <span style="text-align: left">Marital Status</span><br>
                        <button id="button_single" name="button_single" class="button-fc btm-space30" onclick="single();">Single</button>
                        <button id="button_marital" name="button_marital" class="button-fc btm-space30" onclick="marital();"> Married</button>  
                    </div>
                    <div id="46__Step3__mother_name" class="new-width" style=""><span style="text-align: left">Mother's Name</span>
                        <input type="text" id="profile_info_mother_first_name" name="profile_info_mother_first_name" maxlength="40" placeholder="Enter your Mother First Name" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);" onchange="validate_name(this);"><span style="display: none">Mother's Maiden Name</span>
                        <input type="text" id="profile_info_mother_maiden_name" name="profile_info_mother_maiden_name" style="display: none" maxlength="40" placeholder="Enter your Mother Maiden Name" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);" onchange="validate_name(this);">
                        <input type="text" id="profile_info_mother_last_name" name="profile_info_mother_last_name" class="hidden" maxlength="20" placeholder="Enter your Mother Last Name" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);" onchange="validate_name(this);">
                    </div>
                    <div id="50__Step3__branch_select" class="forms-ac new-width" style=""> <span class="new-hd-ac" style="text-align: left">Please Select the Nearest Fincare Branch Below</span>
                        <select id="profile_info_branch" name="profile_info_branch" placeholder="Select">
                            <option value="">Select Value</option>
                        </select>
                    </div>
                    <div id="53__Step3__continue" class="" style="">
                        <button id="button_continue_profile" class="button-inac" onclick="validatedata();google_step3()" disabled="true">CONTINUE</button>
                    </div>
                </div>
                <div id="Step4" class="hide">
                    <div id="268__Step4__product_head" class="new-width" style=""><span class="new-hd-ac" style="text-align: left">Product Selection</span><span class="" style="text-align: left">Please Select the Product You Want</span>
                    </div>
                    <div id="56__Step4__product" class="card new-width" style="padding:10px" оnclick="produt_select1('1021');">
                        <fieldset id="spanrd_product_select">
                            <input type="radio" name="rd_product_select" class="input-rd" onclick="setProduct(this);" value="1021">
                            <label>101 First - Pay as you go
                                <script>
                                    window.dataLayer = window.dataLayer || [];function gtag(){dataLayer.push(arguments);}gtag('js', new Date());gtag('config', 'AW-736579055');
                                </script>
                            </label>
                        </fieldset><span class="cards-txt"><img src="images/ZeroBalanceMobile.png">Zero Balance Account</span><span class="cards-txt"><img src="images/MobileTransferMobile.png">IMPS, NEFT &amp; RTGS Enabled</span><span class="cards-txt"><img src="images/DebitCardMobile.png">Gold Debit Card (RuPay Classic)<script>$('#56__Step4__product').on('click',function(){ produt_selectclick('1021'); });</script></span><span class="cards-txt"><button class="know-more" id="prod_know_more_first" onclick="know_more_first()">KNOW MORE</button></span>
                    </div>
                    <div id="57__Step4__product" class="card new-width" style="padding:10px" onclick="produt_selectclick('1022');">
                        <fieldset id="spanrd_product_select">
                            <input type="radio" name="rd_product_select" class="input-rd" onclick="setProduct(this);" value="1022">
                            <label>101 Priority - Unlock exclusive privileges</label>
                        </fieldset><span id="span_prod1" class="card-cont cards-txt"><img src="images/100_Returns Mobile.png">100% Returns on Minimum Average Balance</span><span id="span_prod2" class="card-cont cards-txt"><img src="images/UnlimitedfundMobile.png">Unlimited free online fund transfers</span><span id="span_prod3" class="card-cont cards-txt"><img src="images/DebitCardMobile.png">Premier Debit Card (Rupay Platinum) with 30+ Airport lounge access)</span><span class="cards-txt"><button onclick="know_more()" class="know-more" id="prod_know_more">KNOW MORE</button></span>
                    </div>
                    <div id="58__Step4__offer_avail_benefits" class="hide" style="">
                        <fieldset id="checkbox_offer_avail_benfits" name="checkbox_offer_avail_benfits" onclick="enable_continue(this);">
                            <input type="checkbox" id="checkbox_offer_avail_benfits" name="checkbox_offer_avail_benfits" onclick="enable_continue(this);" value="Yes">
                            <label>I here by authorize Fincare to share my necessory details to offer patners for availing benfits.</label>
                        </fieldset>
                    </div>
                    <div id="306__Step4__know_more_modal" class="hide " style="">
                        <img src="images/end-button.png" class="close-img" onclick="$.modal.close();"><span class="modal-phd" style="text-align: left">101 Priority account</span><span id="know_more_modal_label" name="know_more_modal_label"><p class="prod-hd">Average Monthly Minimum Balance</p><p 'style':="" 'text-align:="" center'="">INR 25,000</p><p class="prod-hd">Superior Interest Rates on the 101 Priority account</p><ol><li>Enjoy 6% p.a. on your balances up to INR 1 lakh</li><li>For balances above INR 1 Lakh*, enjoy higher interest rates with 6% p.a. up to INR 1 Lakh, and 7% p.a. for the remaining balance over and above INR 1 Lakh</li><li>Open an FD from your 101 Priority account and earn up to 7.5% p.a.</li><li>If customer linked FD balances are or exceed INR 1 Lakh, AMB charges will be waived off</li></ol><p class="prod-hd">RuPay Platinum Debit Card</p><p class="prod-hd">Complimentary Lounge Program:</p><p>Access to 30+ domestic lounges two times per calendar quarter per card.</p><p class="prod-hd">Comprehensive Insurance Cover up to Rs.2 Lakhs:</p><p>Personal&nbsp;Accident&nbsp;Insurance and&nbsp;Permanent&nbsp;Total&nbsp;Disability cover up to Rs.2 Lakhs.</p><p class="prod-hd">24/7 Concierge Services:</p><p>Avail host of referral services from travel assistance to hotel reservations to Consultancy services.</p><p class="prod-hd">Transactions @ Zero Charge!</p><ol><li>100 free transactions per month across NEFT, IMPS, RTGS</li><li>Free debit card issuance</li><li>Zero debit card maintenance charges</li><li>15 free ATM transactions (financial/non-financial) per month</li></ol><p class="prod-hd">Fund Transfers</p><ol><li>100 transactions per month free across IMPS, NEFT, RTGS* and across any of the bank’s channels (online, branch, etc.)</li><li>Charges if transactions exceed 100 transactions for the month:<p class="prod-hd">IMPS:</p> <br><table class="table"><tbody><tr><td>Up to INR 25000</td><td>INR 2.5 per transaction</td></tr><tr><td>INR 25000 - INR 1 Lakh</td><td>INR 5 per transaction</td></tr><tr><td>INR 1 Lakh - INR 2 Lakh</td><td>INR 15 per transaction</td></tr></tbody></table><p class="prod-hd">NEFT:</p> <br><table class="table"><tbody><tr><td>Up to INR 10,000</td><td>INR 2.25 per transaction</td></tr><tr><td>INR 10,000 - INR 1 Lakh</td><td>INR 4.75 per transaction</td></tr><tr><td>INR 1 Lakh - INR 2 Lakh</td><td>INR 14.75 per transaction</td></tr><tr><td>Above INR 2 Lakhs</td><td>INR 24.75 per transaction</td></tr></tbody></table><p class="prod-hd">RTGS*:</p> <br><table class="table"><tbody><tr><td>INR 2 Lakhs - INR 5 Lakhs</td><td>INR 17 per transaction</td></tr><tr><td>Above INR 5 Lakhs</td><td>INR 44 per transaction</td></tr></tbody></table> <br><p>*RTGS available only after conversion to full KYC account</p></li></ol></span>
                    </div>
                    <div id="307__Step4__know_more_modal" class="hide" style="">
                        <img src="images/end-button.png" class="close-img" onclick="$.modal.close();"><span class="modal-phd" style="text-align: left">101 First account</span><span id="know_more_modal_label" name="know_more_modal_label"><p class="prod-hd">Zero Balance account</p><p 'style':="" 'text-align:="" center'="">No minimum average balance requirement</p><p class="prod-hd">Superior Interest Rates on the 101 First account</p><ol><li>Enjoy up to 7% p.a. on your savings account</li><li>Open an FD from your 101 First account and earn up to 7.5% p.a.</li></ol><p class="prod-hd">RuPay Classic Debit Card</p><p class="prod-hd">The Zero Edge!</p><ol><li>Free debit card issuance</li><li>Zero debit card maintenance charges for the first year</li><li>8 free ATM transactions (financial/non-financial) per month</li></ol><p class="prod-hd">Fund Transfers</p><p class="prod-hd">IMPS:</p><table class="table"><tbody><tr><td>Up to INR 25000</td><td>INR 2.5 per transaction</td></tr><tr><td>INR 25000 - INR 1 Lakh</td><td>INR 5 per transaction</td></tr><tr><td>INR 1 Lakh - INR 2 Lakh</td><td>INR 15 per transaction</td></tr></tbody></table><p class="prod-hd">NEFT:</p><table class="table"><tbody><tr><td>Up to INR 10,000</td><td>INR 2.25 per transaction</td></tr><tr><td>INR 10,000 - INR 1 Lakh</td><td>INR 4.75 per transaction</td></tr><tr><td>INR 1 Lakh - INR 2 Lakh</td><td>INR 14.75 per transaction</td></tr><tr><td>Above INR 2 Lakhs</td><td>INR 24.75 per transaction</td></tr></tbody></table><p class="prod-hd">RTGS*:</p><table class="table"><tbody><tr><td>INR 2 Lakhs - INR 5 Lakhs</td><td>INR 17 per transaction</td></tr><tr><td>Above INR 5 Lakhs</td><td>INR 44 per transaction</td></tr></tbody></table><p>*RTGS available only after conversion to full KYC account</p></span>
                    </div>
                        <div id="68__Step4__account_nominee_yn" class="new-width" style=""><span class="new-hd-ac" style="text-align: left">Account Nominee</span><span style="text-align: left">Nomination</span><br><br>
                      <button id="button_shownext_popup" name="button_shownext_popup" class="button-nc btm-space30" onclick="nomineeyes();"> Yes</button>
                      <button id="button_shownext_popup" name="button_shownext_popup" class="button-nc btm-space30" onclick="nomineeno();">No</button>
                   </div>
                    <div id="nominee_details_div" class="cont-div" style="display:none">
                        <div id="255__nominee_details__nominee_details" class="forms-ac new-width" style=""><span class="new-hd-ac" style="text-align: left">Nomination</span><span style="text-align: left">Nominee Name</span>
                            <input type="text" id="input_nominee_name" name="input_nominee_name" maxlength="50" placeholder="Enter Nominee Name" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);"><span style="text-align: left">Nominee Relation</span>
                            <select id="dropdown_nominee_relation" name="dropdown_nominee_relation" placeholder="Select">
                                <option value="">Select Value</option>
                                <option value="SPOUSE">SPOUSE[Husband/Wife]</option>
                                <option value="FATHER">FATHER</option>
                                <option value="MOTHER">MOTHER</option>
                                <option value="SON">SON</option>
                                <option value="DAUGHTER">DAUGHTER</option>
                                <option value="OTHERS">OTHERS</option>
                            </select>
                        </div>
                        <div id="257__nominee_details__nominee_dob" class="new-width" style=""><span style="text-align: left">Nominee DOB</span>
                            <fieldset id="date_nominee_dob" class="example date-dropdowns" onchange="nominee_dob_change(this);">
                            </fieldset>
                            <button id="button_shownext" class="button-inac" style="display:none">CONTINUE
                                <script>
                                    $('#date_nominee_dob').dateDropdowns({submitFieldName: 'date_nominee_dob',monthFormat: 'short'});
                                </script>
                            </button>
                        </div>
                        <div id="256__nominee_details__address" class="forms-ac new-width" style=""><br><br><span class="new-hd-inac" style="text-align: left">Nominee Address</span><span style="text-align: left">Address Line 1</span>
                            <input type="text" id="nominee_add1" name="nominee_add1" maxlength="40" placeholder="Enter Address Line1" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);"><span style="text-align: left">Address Line 2</span>
                            <input type="text" id="nominee_add2" name="nominee_add2" maxlength="40" placeholder="Enter Address Line2" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);"><span style="text-align: left">Address Line 3</span>
                            <input type="text" id="nominee_add3" name="nominee_add3" maxlength="40" placeholder="Enter Address Line3" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);">
                        </div>
                    </div>
                    <div id="minor_nominee_div" style="display:none">
                        <div id="258__minor_nominee__minor_nominee_details" class="forms-ac new-width" style=""><span class="new-hd-inac" style="text-align: left">Minor Nominee Guardian Details </span><span style="text-align: left">Minor Nominee Guardian Name</span>
                            <input type="text" id="text_minor_nominee_name" name="text_minor_nominee_name" maxlength="30" onblur="minornom();" placeholder="Enter Your Minor Nominee Guardian Name" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);"><span style="text-align: left">Minor Nominee Guardian Age</span>
                            <input type="number" id="input_minor_nominee_age" name="input_minor_nominee_age" maxlength="3" placeholder="Enter Your Minor Nominee Guardian Age" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);"><span style="text-align: left">Minor Nominee Guardian Relation</span>
                            <select id="input_minor_nominee_rel" name="input_minor_nominee_rel" placeholder="Enter Your  Minor Nominee Guardian Relation Name">
                                <option value="">Select Value</option>
                                <option value="SPOUSE">SPOUSE[Husband/Wife]</option>
                                <option value="FATHER">FATHER</option>
                                <option value="MOTHER">MOTHER</option>
                                <option value="SON">SON</option>
                                <option value="DAUGHTER">DAUGHTER</option>
                                <option value="OTHERS">OTHERS</option>
                            </select>
                        </div>
                        <div id="259__minor_nominee__address" class="forms-ac new-width" style=""><span class="new-hd-inac" style="text-align: left">Minor Nominee Guardian Address </span><span style="text-align: left">Address Line 1</span>
                            <input type="text" id="nominee_minor_add1" name="nominee_minor_add1" maxlength="40" placeholder="Enter Address Line1" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);"><span style="text-align: left">Address Line 2</span>
                            <input type="text" id="nominee_minor_add2" name="nominee_minor_add2" maxlength="40" placeholder="Enter Address Line2" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);"><span style="text-align: left">Address Line 3</span>
                            <input type="text" id="nominee_minor_add3" name="nominee_minor_add3" maxlength="40" placeholder="Enter Address Line3" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);">
                        </div>
                    </div>
                    <div id="284__Step4__button" class="" style="">
                      <button id="button_nominee_details" class="button-inac" style="display:none" disabled="true" onclick="ShowTNC(this);google_step4_nominee()">CONTINUE</button>
                   </div>
                   <div id="284__Step4__button1" class="" style="">
                      <button id="button_nomineeno_details" class="button-inac" style="display:none" disabled="true" onclick="ShowTNC(this);google_step4_nominee()">CONTINUE</button>
                   </div>
                    <div id="72__Step4__TNC" class="hide modal-tc" style="display:none">
                                            <div id="72__Step4__TNC1" class="modal-top" style=""> <span class="dbt-hd" style="text-align: center">Terms and Conditions</span><span id="text_tnc_text" name="text_tnc_text"><ol><li> I hereby state that I have not used the OTP based Aadhaar(e - KYC) verification to open any other bank account in Fincare Small Finance or any other bank.</li> <li> I hereby state that I have no objection in authenticating myself with Aadhaar based authentication system and consent to providing my Aadhaar Number, Biometric and / or(One Time Pin) OTP data(and / or any similar authentication mechanism) for Aadhaar Based authentication for the purposes of availing of the Banking Service from Fincare Small Finance Bank.</li> <li> I understand that the Biometric and / or(One Time Pin) OTP(and / or any similar authentication mechanism) I may provide for authentication shall be used only for authenticating my identity through Aadhaar Authentication system for that specific transaction and for no other purposes.I understand that Fincare Small Finance Bank shall ensure security and confidentiality of my personal identity data provided for the purpose Aadhaar based authentication.</li> <li>I submit my Aadhaar number and voluntarily give my consent to:<ol> <li> Use my Aadhaar Details to authenticate me from UIDAI.</li> <li> Use my Registered Mobile Number in the bank records for sending SMS alerts to me.</li> <li> Link the Aadhaar Number to all my existing or new or future accounts and customer profile(CIF) with your Bank.</li> </ol> </li><li>I understand that my information submitted to the bank herewith shall not be used for any purpose other than mentioned above. I hereby declare that all the above information voluntarily furnished by me is true, correct and complete.</li> <li> The bank may disclose information about customers account, if required or permitted by law, rule or regulations, or at the request of any public or regulatory authority or if such disclosure is required for the purpose of preventing frauds, or in public interest, without specific consent of the account holder/s. </li><li>Any Resident Indian who is 18 years and above who provides his/her details in the Digital Savings Account application for opening Fincare 101 account offered by Fincare Small Finance Bank Limited is a Customer</li><li>By logging into and providing his/her details, customer agrees to the terms and conditions of the Bank as listed in <a href="https://www.fincarebank.com/sites/default/files/2018-10/SA-TC-English.pdf" target="_blank" style="color:#00F; cursor: pointer">https://www.fincarebank.com/sites/default/files/2018-10/SA-TC-English.pdf</a></li><li>By visiting the Fincare 101,Apply Now link and sharing all information, customer consents to provide his/her name, contact details and other information on an at will basis with Bank.</li><li>Customer authorize Fincare Small Finance Bank to call on the given number to explain the features of the product and marketing</li><li>Bank shall not be liable for any connectivity/ signal/ data issues leading to non-completion or wrong/false/incomplete information being provided by the customer leading to the incompletion of his/her application.</li><li>The customer herewith agrees to provide his/ her valid Aadhaar number and valid PAN Card details. He/she understands that opening an account is subject to correct, complete and accurate information is provided.</li><li>Customer agrees that upon successful OTP validation of Aadhaar, the name of the customer in the account will be the same name as appearing in his/her Aadhaar Card</li><li>Application once submitted cannot be withdrawn by the customer. Bank shall not be liable to pay for any costs (technical/ data plan related or otherwise) incurred by the customer in the course of the downloading or sharing of his/her details on the application.</li><li>The customer herewith agrees to provide the accurate documentation and information as listed in the app for the purpose of account opening. Customer understands and agrees that failure to provide requisite documentation and information shall result in rejection of application by the Bank. The customer agrees that Bank has every right to reject the account opening application, if there is any erroneous, incomplete or misleading information provided by the customer or for any other reason whatsoever with/without assigning any reason or if KYC documents submitted do not comply with the KYC norms of the Bank.</li><li>This account opening process is not available for existing Fincare Small Finance Bank customers.</li><li>This account opening process is also not available for customers who are FATCA reportable. Such customers are requested to approach the branch and to comply with requirement of opening an account. </li><li>Bank reserves the right to take necessary action, legal or otherwise, if it finds any wilful modification/ withholding of information or misrepresentation by the customer.</li><li>Customers will have restricted access on this account till KYC is completed</li><li>Customer declares and confirms that the Bank shall be entitled to rely on all/any communication, information and details provided on the electronic form and all such communications, information and details shall be final and legally binding on the Customer. </li><li>Customer understands and confirms that the Bank has every right to close or debit freeze the account, if the details provided by him/her are found to be inaccurate, incorrect or false by the Bank or for any other reason whatsoever without assigning any reason thereof. In such an event, the bank shall not be liable to pay any interest on the amount of deposit &amp; the refund of amount deposited in the account will be refunded to the source account/or issue a Demand Draft to the Customer. In such an event, bank will retain the documents / Photographs (if any) and any other signed document submitted. </li><li>This application is available for opening a savings account by Resident Indian Individuals only.</li><li>Customer gives consent to the Bank to fetch his/her demographic details from Unique Identification Authority of India (UIDAI), using biometric authentication which will be used for KYC purposes. The demographic details include name, DOB, Father's Name, gender, address and photograph. Customer further consents to the Bank to fetch his/her contact details from UIDAI which includes the contact number and email ID.</li><li>The Bank reserves the right to cancel the Customer ID and Account Number (A/C) allotted to the customer, if the customer does not complete the verification process within the allotted time.</li><li>The Bank reserves the right to hold the accounts in Debit Freeze or close the Account even after account activation in case of any discrepancy found as part of regular monitoring and document verification activities.</li><li>The customer herewith agrees to be contacted by the bank to receive information in respect of account maintenance, alerts, payments due, updates on existing and new products, servicing of account for sales, marketing or servicing their relationship with Fincare Small Finance Bank Limited and its group companies / associates or agents through Telephone / Mobile / SMS / Email etc. Further he/she understands that the consent to receive calls / communications shall be valid and shall prevail over their current or any subsequent registration of their mobile number for NDNC and shall continue to be treated as customer consent / acceptance.</li><li>The customer herewith agrees that if the application is rejected, Bank will retain the documents / Photographs and any other signed document submitted by the customer on the Web app or otherwise.</li><li>Customer confirms to have read, understood and will be bound to/ abide by the Terms and Conditions of account opening and the general terms applicable to account as available on Fincare Small Finance Bank's website</li><li>The customer herewith confirms that he/she has not used the OTP based Aadhaar (e-KYC) verification to open any other bank account.</li><li>Fincare 101 account opened with UIDAI based OTP, with credit balance of not more than 90 Thousand. The overall cumulative deposit in such an account cannot exceed 1.9 Lakhs, unless you complete full KYC process is completed</li><li>The customer herewith agrees to complete his full KYC by visiting his nearest Fincare Small Finance Bank branch/ by requesting a visit from the bank representative and providing his biometric details within 12 months of opening such account.</li><li>The customer agrees that failure to complete full KYC within 12 months will lead to his account being closed by the bank.</li><li>The customer agrees that if her/ his balance in the limited KYC account exceeds 90 Thousand in a given day, bank reserves the right to put the account on a total freeze, till such time that customer completes the full KYC process.</li><li>In case of account being put under total freeze, the customer agrees that principal amount on such deposits kept along with the interest accrued (if any) will be payable to him only upon completing his full KYC.</li><li>The customer will not be eligible to get a Cheque book for this account till the time full KYC is completed</li><li>The customer agrees that such an account will be subject to regular scrutiny and monitoring from the bank and bank shall reserve the right to completely freeze/debit freeze/ close the account at its sole discretion.</li><li>If Customer fails to provide full KYC for any reason, customer understands and agrees that the bank reserves right to close the account post which the bank will issue a Demand Draft and dispatch the same in the customer communication address available in bank records</li><li>101 First customers will be offered a Rupay debit cared</li><li>101 Priority customers will be offered a Rupay platinum debit card with 30+ Airport lounge</li><li>Foreign Account Tax Compliance Act (FATCA) is a United States (US) Federal Law that requires non-US financial institutions to report to the Internal Revenue Service (IRS) of U.S. certain information about financial accounts held by U.S taxpayers, or by foreign entities in which U.S. taxpayers hold a substantial ownership interest. The Government of India has signed the Inter-Governmental Agreement with Internal Revenue Service (IRS) of the United States on July 9, 2015 to implement FATCA in India.</li><li>The purpose of FATCA is to counter tax evasion by US taxpayers by requiring non-US financial institutions to report information on their US accountholders to the IRS.</li>
                                            <li>I here by authorize Fincare to share my necessory details to offer patners for availing benfits.</li><br></ol> </span>
                                            </div>
                                            <div id="296__Step1__pop_up_btns" class="bttn-tc-group" style="">
                                                <button id="button_shownext_tnc" name="button_shownext_tnc" class="button-ac btm-space" onclick="aml_score_check();google_step4_tnc()">I AGREE</button>
                                            </div>
                                        </div>
                </div>
                <div id="account_sucess_page" class="hide">
                    <div id="266__AccountSuccess__account_header" class="new-width" style=""><span class="new-hd-ac" style="text-align: left">Congratulations <label id="label_cust_name"> </label></span><span style="text-align: left">You are now Account Holder With Fincare Small Finance Bank </span>
                    </div>
                    <div id="fund__AccountSuccess__account" class="bttn-bc-group new-width" style="">
                    <span class='btn-tc btm-space20' style="text-align: left">Now you can start funding your account</span>
                    <button class='button-tc btm-space20' onclick="fund_now();">Fund My Account</button>
                    </div>
                    <div id="fund__Account" class="hide" style="">
                    <span class="display-pghd" style="text-align: left">Enter the amount to be deposited</span>
                    <input type="text" id="input_fund_amount" name="input_fund_amount" placeholder="Min ₹ 1,000 - Max ₹ 90,000">&nbsp;<br>
                    <button class='button-funac' id="fund_amn" onclick="fund_amount();">Proceed For Payment</button>
                    </div>
                    <div id="285__AccountSuccess__account_success_details" class="new-width" style=""><span class="display-pghd" style="text-align: left">Account Name</span>
                        <input type="text" id="input_account_name" name="input_account_name" readonly="true"><span class="display-pghd" style="text-align: left">Customer ID</span>
                        <input type="text" id="input_customer_id" name="input_customer_id" readonly="true"><span class="display-pghd" style="text-align: left">Account No</span>
                        <input type="text" id="input_account_no" name="input_account_no" readonly="true"><span class="display-pghd" style="text-align: left">Branch Name</span>
                        <input type="text" id="input_branch" name="input_branch" readonly="true"><span class="display-pghd" style="text-align: left">IFSC Code</span>
                        <input type="text" id="label_ifsc_code" name="label_ifsc_code" readonly="true">
                    </div>
                    <div id="virtual_debit_card" class="bttn-bc-group new-width" style="display:none;">
                    <span class='btn-tc btm-space20' style="text-align: left">Virtual Debit Card</span>
                    <p>Your Virtual Debit Card is created.</p><p>Please download and login to Mobile Banking application to get all the details. </p>
                    </div>
                    <div id="295__AccountSuccess__mb_details" class="margin:0 auto new-width" style=""><span class="new-hd-ac" style="text-align: left">Fincare Mobile Banking</span><span><a href="https://play.google.com/store/apps/details?id=com.fincare.mb" target="__blank"><img src="images/googleplay.png"></a></span><span><a href="https://itunes.apple.com/us/app/fincare-mobile-banking/id1286645133?mt=8" target="__blank"><img src="images/apple.png"></a></span>
                    </div>
                    <div id="266__AccountSuccess_photoupload" class="new-width" style=""><span class="new-hd-ac" style="text-align: left">Photo Upload</span>
                   <div id="42__Step2__customer_selfie_signature" class="span_on_img" style=""><span>Take Your Selfie</span>
                      <img id="customer" src="images/capture-selfie.png" name="customer" onclick="capture_image('file_customer_selfie');">
                      <input type="file" id="file_customer_selfie" name="file_customer_selfie" style="display:none" accept="image/*" capture="user" onchange="changeImage('file_customer_selfie','customer')"><span>Upload Signature Image</span>
                      <img id="signature" src="images/capture-sign.png" name="signature" onclick="capture_image('file_customer_sign');">
                      <input type="file" id="file_customer_sign" name="file_customer_sign" style="display:none" accept="image/*" onchange="changeImage('file_customer_sign','signature')"><span style="font-size:12px">*Sign on a sheet of paper, and take signature photo only</span>
                      <button id="button_photoupload" name="button_photoupload" class="button-ac" onclick="photosubmit();">CONTINUE</button>
                   </div>
                   </div>
                    
                    
                    <div id="kyc_modal_div" class="modal-tc-dbt" style="display:none">
                        <div id="351__KYC__KYC_Schedule" class="" style="">
                            <img src="images/end-button.png" class="close-img" onclick="kyc_skip();"><span class="new-hd-ac" style="text-align:center">Schedule for Doorstep Full KYC</span>
                            <img src="images/question-mark.png" class="close-imgkyc" onclick="more_kyc();"><span class="cons-txt-ac" style="text-align:center">(optional)</span><span class="kyc-hd-ac" style="text-align:center">Know More about the Full KYC</span><span class="kyc-hd-ac" style="text-align:center">To keep your account balance more than 1 Lakh, complete your FULL KYC.</span>
                            <button id="button_KYC_popup" name="button_KYC_popup" class="button-tc btm-space20 kyc-space20" onclick="Schedule_kyc();">Schedule NOW</button>
                            <button id="button_KYC_popup" name="button_KYC_popup" class="btn-tc btm-space20 kyc-space20" onclick="kyc_skip();">I'll do it LATER</button>
                        </div>
                    </div>
                    <div id="kyc_know_modal_div" class="modal-tc-dbt" style="display:none">
                        <div id="357__Know_KYC__Know_more_KYC" class="" style=""><span class="new-hd-ac" style="text-align:center" onclick="full_kyc();">Know more about Full KYC</span><span class="kyc-hd-ac" style="text-align:center" onclick="full_kyc();">According to RBI Regulations, an account opened through Aadhaar OTP e-KYC have following limitations</span><span id="kyc_terms" class="menu_terms" onclick="full_kyc();"><ol class="trems-list"><li>Balance should not exceed Rs 1 Lakh.</li><li>Aggregate of all the credits should not exceed Rs 2 Lakh</li><li>Full KYC must completed in one year of account creation.</li></ol></span><span class="kyc-hd-ac" style="text-align:center" onclick="full_kyc();">To do away with these limitations, complete your Full KYC.</span>
                        </div>
                    </div>
                    
                    <div id="Schedule_div" class="cont-div" style="display:none">
                        <div id="363__Schedule_KYC__Schedule_det" class="" style="">
                            <img src="images/end-button.png" class="close-img" onclick="kyc_skip();"><span class="new-hd-ac" style="text-align: center">Full KYC Appointment Scheduling</span><span class="new-hd-ac" style="text-align: left">Doorstep KYC Address</span>
                            <fieldset id="spanrd-same_as_curr_address_yn">
                                <input type="radio" id="rd-same_as_curr_address_yn" name="rd-same_as_curr_address_yn" class="input-rd" onshow="fillkycaddress(this);" onclick="fillkycaddress(this);" value="Same_As_Current_Address" selected="selected">
                                <label>Same As Current Address</label>
                            </fieldset>
                            <fieldset id="spanrd-same_as_curr_address_yn">
                                <input type="radio" id="rd-same_as_curr_address_yn" name="rd-same_as_curr_address_yn" class="input-rd" onshow="fillkycaddress(this);" onclick="fillkycaddress(this);" value="Other">
                                <label>Other</label>
                            </fieldset>
                        </div>
                        <div id="364__Schedule_KYC__Kyc_address" class="forms-ac" style=""><span style="text-align: left">Address Line 1</span>
                            <input type="text" id="kyc_comm_add_add1" name="kyc_comm_add_add1" maxlength="40" placeholder="Enter your communication address" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);" onchange="validate_address(this);"><span style="text-align: left">Address Line 2</span>
                            <input type="text" id="kyc_comm_add_add2" name="kyc_comm_add_add2" maxlength="40" placeholder="Enter your communication address" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);" onchange="validate_address(this);"><span style="text-align: left">Address Line 3</span>
                            <input type="text" id="kyc_comm_add_add3" name="kyc_comm_add_add3" maxlength="40" placeholder="Enter your communication address" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);" onchange="validate_address(this);"><span style="text-align: left">City</span>
                            <input type="text" id="kyc_comm_add_city" name="kyc_comm_add_city" maxlength="50" onkeyup="pincodecheck();" placeholder="Enter your City" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);" onchange="validate_name(this);"><span style="text-align: left">Pincode</span>
                            <input type="number" id="kyc_comm_add_pincode" name="kyc_comm_add_pincode" maxlength="6" placeholder="Enter your pincode" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);" onkeyup="pincodecheck();" onchange="verify_pincode(this);"><span style="text-align: left">State</span>
                            <select id="kyc_state" name="kyc_state" placeholder="Select your state">
                                <option value="">Select Value</option>
                            </select>
                        </div>
                        <div id="365__Schedule_KYC__Schedule_details" class="" style=""><span class="new-hd-ac" style="text-align:left">Schedule Slot</span><span style="text-align: left">Date</span>
                            <input type="date" id="kyc_date" name="kyc_date"  max ="", min= "" placeholder="dd/mm/yyy" onchange="validate_address(this);"><span style="text-align: left">Time</span>
                            <select id="kyc_time" name="kyc_time" placeholder="Select Time">
                                <option value="">Select Value</option>
                                <option value="Morning">Morning Slot</option>
                                <option value="Afternoon">Afternoon Slot</option>
                                <option value="Evening">Evening Slot</option>
                            </select>
                            <button id="button_submit_kycschedule" name="button_submit_kycschedule" class="button-ac rating-chkbox" onclick="fullKyc_submit();">SUBMIT</button>
                        </div>
                        
                        <div id="Covid_alert" class="cont-div" style="display:none">
                     <img src="images/end-button.png" class="close-img" onclick="covid();"><span class="modal-phd" style="text-align: center">COVID- 19 Alert</span>
                    
                           <p class='modal-b'><b >Due to ongoing COVID-19 pandemic and subsequent lockdown, our doorstep KYC services has been stoped.</b></p>
                           <p class='modal-b'><b>We will send you the link to schedule your Full KYC as soon as the doorstep Full-KYC services resume.</b></p>

                        </span>
                  </div>
                  <div id="Debit_Card_Details" class="cont-div" style="display:none">
                     <img src="images/end-button.png" class="close-img" onclick="close_dbt();"><span class="modal-phd" style="text-align: left"></span>
                           <p class='modal-b'><b >You will receive your Debit card and the PIN in two separate deliveries at your communication address. Standard delivery time is 10 working days, but due to the ongoing covid 19 pandemic and the subsequent lockdown, the same may get delayed.</b></p>
                           <p class='modal-b'><b>You will be eligible for a Cheque Book after you complete the full KYC with the bank.</b></p>

                        </span>
                  </div>
                  
                  <!-- ==============================vkyc=================== -->
                  <div id="video_kyc_modal_div" class="modal-tc-dbt1" style="display:none">
                    <div id="351__video__KYC_Schedule" class="" style="">

                        <div class="" style="text-align:center;">


                            <br>
                               <img src="images/consent_video.png" class="image_resp" alt="">
                        
                               <h6 style="padding:0 20px;line-height: 1.5; color: #000;">As per RBI regulation, you are required to complete your full KYC to enjoy all benefits of the account</h6>
                               <br>
                               <h6 style="padding:0 30px;line-height: 1.5;color: #000;">Would you like to give your consent for Video KYC, Which typically takes 2 minutes?</h6>

                          <br>
                               <input type="button" class="btn btn-md " onclick="storeImageOnServer();" style="background-color: #A8060A;color:white;width: 100%;" value="YES, I GIVE MY CONSENT"></button> <br>
                               <br>
                               <button type="button" class="btn btn-md" style="color:black;float:none;"  onclick="newCIFcreation();google_step4_tnc();">  <u>No.&nbsp;continue with Doorstep KYC</u></button>
                               <br>
                              <h6 style="padding:0 20px;line-height: 1.5; color:gray;">Once you provide consent, Video session should be completed within 15 days</h6>
                            
                            </div>
                            
                        <!-- <span class="videokyc-hd-ac" style="text-align:center">As per RBI regulation, you are required to complete your full kyc to remove all restrictions from your account.</span><br><span class="videokyc-hd-ac" style="text-align:center">To keep your account balance more than 1 Lakh, complete your FULL KYC.</span><br>
                        <button id="button_submit_videokyc" name="button_submit_videokyc" class="button-ac-desktop rating-chkbox" onclick="storeImageOnServer();newCIFcreation('initiate');">YES, initiate Video KYC</button><br>
                        <span>No,</span>
                        newCIFcreation();google_step4_tnc();
                        <button id="button_KYC_popup" name="button_KYC_popup" class="btn-tc btm-space20 kyc-space20" onclick="full_kyc();newCIFcreation();google_step4_tnc();">Continue with Doorstep KYC</button> -->
                    </div>
                </div>
                <div id="video_kycsatrt_modal_div" class="modal-tc-dbt1" style="display:none">
                    <div id="351__video__KYCstart_Schedule" class="" style="">

                        <div class="" style="text-align:center;">
                            <br>
                                 <img src="images/proceed_kyc.svg" style="margin-top:60px;" alt="">
                            <br>
                                 <h6 style="padding:0 10px;line-height: 1.5;margin-top:60px;color:black;">Please keep your PAN card handy and be in a well lit place to ensure proper video session</h6>
                            
                            <br>
                                 <input type="button" class="btn btn-md" onclick="createProfileIdForVideoKYC();" style="background-color: #A8060A;color:white;width: 100%;" value="PROCEED TO VIDEO KYC"></button>
                    
                        </div>
                        <!-- <span class="videokyc-hd-ac" style="text-align:center">Please keep your PAN card handy and be in a well lit place to ensure proper video session</span><br> -->
                        <!-- <button id="button_videokyc" name="button_videokyc" class="button-ac-desktop rating-chkbox" onclick=" createProfileIdForVideoKYC();">Proceed to Video KYC</button> -->
                        <!-- <button id="button_KYC_popup" name="button_KYC_popup" class="btn-tc btm-space20 kyc-space20" onclick="schedulelater_video();">Schedule later</button> -->
                    </div>
                </div>
                <div id="video_kycsatrt_div" class="modal-tc-dbt1" style="display:none">
                    <div id="351__video__KYCstart" class="" style="">
                        <!-- <span class="videokyc-hd-ac" style="text-align:center">You will be connected with the Bank official, shortly. Please stay online, your approx. Wait time in 2 min.</span><br> -->
                        <!-- <button id="button_videostart" name="button_videostart" class="button-ac-desktop rating-chkbox" onclick="launchToVideoKYC();">Start</button> -->
                        <div class="">
                            <div class="modal-body" style="text-align:center;width:100%;">
                              <button type="button" style="padding:0px 0px 10px 0px" class="close" data-dismiss="modal">&times;</button>
                                <h6 style="font-size:1.1em; font-weight:500;padding:20px 49px 0px 49px;">You will be connected with the Bank official, shortly</h6><br>
                                <h6 style="font-size:1.1em; font-weight:500;padding:0px 49px;">Please stay online, your approximate wait time is 2 min</h6> <br>
              
                              <div class="progress" style="margin:0 auto;width:60%;height:30px;border:1px solid lightgray;background-color:white;border-radius:0px;">
                                <!-- <div class="progress-bar" role="progressbar" style="width: 10%;background-color:#ffd800;"></div>
                                <div class="progress-bar" role="progressbar" style="width: 10%;background-color:#fdba12;" ></div>
                                <div class="progress-bar" role="progressbar" style="width: 10%;background-color:#fcb316;"></div>
                                <div class="progress-bar" role="progressbar" style="width: 10%;background-color:#f99b1c;"></div>
                                <div class="progress-bar" role="progressbar" style="width: 10%;background-color:#f6881f;"></div>
                                <div class="progress-bar" role="progressbar" style="width: 10%;background-color:#b01d22;"></div>
                                <div class="progress-bar" role="progressbar" style="width: 10%;background-color:#de1c23;"></div>
                                <div class="progress-bar" role="progressbar" style="width: 10%;background-color:#b01d22;"></div> -->
                                <img src='images/vkyc_loader.gif' id="loader-image" width="64" height="64" style="width:100%;"/>
                              </div>
                              
                            </div>
                            
                            <div class="modal-footer" style="border-top:none;">
                              <div style="text-align:center;margin:0 auto; display:block;width:100%;">
                                <!-- <input type="button" style="color:#A8060A;font-weight:600;" class="btn btn-md" onclick="launchToVideoKYC();" value="OK"> -->
                                <button id="button_submit_hau" name="button_submit_hau"  style="display:none" class="button-ac" onclick="launchToVideoKYC();">OK</button>

                              </div>
                           
                            </div>
                        </div>
                    
                    </div>
                </div>
                <div id="video_kycfail_div" class="modal-tc-dbt" style="display:none">
                    <div id="351__video__KYCfail" class="" style="">
                        <span class="videokyc-hd-ac" style="text-align:center">Currently all our bank officials are busy.</span>
                        <span class="videokyc-hd-ac" style="text-align:center">Please schedule an appoinment for video KYC</span>
                        <span class="videokyc-hd-ac" style="text-align:center">Sorry for the inconvenience.</span>
                        <br>
                        <button id="button_videostart" name="button_videostart" class="button-ac-desktop rating-chkbox" onclick="video_fail();">OK</button>
                    </div>
                </div>
                <div id="schedule_videokyc__div" class="modal-tc-dbt" style="display:none">
                    <div id="351__schedule__videoKYC_" class="" style="">
                        <span class="new-hd-ac" style="text-align:center">Video KYC appointment scheduling</span>
                        <span class="new-hd-ac" style="text-align:left">Schedule Slot</span><span style="text-align: left">Date</span>
                        <input type="date" id="kyc_date" name="kyc_date"  max ="", min= "" placeholder="dd/mm/yyy" onchange="validate_address(this);"><span style="text-align: left">Time</span>
                        <select id="kyc_time" name="kyc_time" placeholder="Select Time">
                            <option value="">Select Value</option>
                            <option value="1">Morning Slot</option>
                            <option value="2">Afternoon Slot</option>
                            <option value="3">Evening Slot</option>
                            
                            
                        </select>
                        <button id="submit_videokycschedule" name="submit_videokycschedule" class="button-ac-desktop rating-chkbox" onclick="videoKyc_submit();">SUBMIT</button>
                    </div>
                </div>
                <div id="kyc_modal_div" class="modal-tc-dbt" style="display:none">
                    <div id="351__KYC__KYC_Schedule" class="" style="">
                        <img src="images/end-button.png" class="close-img" onclick="kyc_skip();"><span class="new-hd-ac" style="text-align:center">Schedule for Doorstep Full KYC</span>
                        <img src="images/question-mark.png" class="close-imgkyc" onclick="more_kyc();"><span class="cons-txt-ac" style="text-align:center">(optional)</span><span class="kyc-hd-ac" style="text-align:center">Know More about the Full KYC</span><span class="kyc-hd-ac" style="text-align:center">To keep your account balance more than 1 Lakh, complete your FULL KYC.</span>
                        <button id="button_KYC_popup" name="button_KYC_popup" class="button-tc btm-space20 kyc-space20" onclick="Schedule_kyc();">Schedule NOW</button>
                        <button id="button_KYC_popup" name="button_KYC_popup" class="btn-tc btm-space20 kyc-space20" onclick="kyc_skip();">I'll do it LATER</button>
                    </div>
                </div>
                <div id="kyc_know_modal_div" class="modal-tc-dbt" style="display:none">
                    <div id="357__Know_KYC__Know_more_KYC" class="" style=""><span class="new-hd-ac" style="text-align:center" onclick="full_kyc();">Know more about Full KYC</span><span class="kyc-hd-ac" style="text-align:center" onclick="full_kyc();">According to RBI Regulations, an account opened through Aadhaar OTP e-KYC have following limitations</span><span id="kyc_terms" class="menu_terms" onclick="full_kyc();"><ol class="trems-list"><li>Balance should not exceed Rs 1 Lakh.</li><li>Aggregate of all the credits should not exceed Rs 2 Lakh</li><li>Full KYC must completed in one year of account creation.</li></ol></span><span class="kyc-hd-ac" style="text-align:center" onclick="full_kyc();">To do away with these limitations, complete your Full KYC.</span>
                    </div>
                </div>
                <div id="Schedule_div" class="cont-div" style="display:none">
                    <div id="363__Schedule_KYC__Schedule_det" class="" style="">
                        <img src="images/end-button.png" class="close-img" onclick="kyc_skip();"><span class="new-hd-ac" style="text-align: center">Full KYC Appointment Scheduling</span><span class="new-hd-ac" style="text-align: left">Doorstep KYC Address</span>
                        <!-- <fieldset id="spanrd-same_as_curr_address_yn">
                            <input type="radio" id="rd-same_as_curr_address_yn" name="rd-same_as_curr_address_yn" class="input-rd" onshow="fillkycaddress(this);" onclick="fillkycaddress(this);" value="Same_As_Current_Address" selected="selected">
                            <label>Same As Current Address</label>
                        </fieldset>
                        <fieldset id="spanrd-same_as_curr_address_yn">
                            <input type="radio" id="rd-same_as_curr_address_yn" name="rd-same_as_curr_address_yn" class="input-rd" onshow="fillkycaddress(this);" onclick="fillkycaddress(this);" value="Other">
                            <label>Other</label>
                        </fieldset> -->
                    <!-- </div> -->
                    <!-- <div id="364__Schedule_KYC__Kyc_address" class="forms-ac" style=""><span style="text-align: left">Address Line 1</span>
                        <input type="text" id="kyc_comm_add_add1" name="kyc_comm_add_add1" maxlength="40" placeholder="Enter your communication address" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);" onchange="validate_address(this);"><span style="text-align: left">Address Line 2</span>
                        <input type="text" id="kyc_comm_add_add2" name="kyc_comm_add_add2" maxlength="40" placeholder="Enter your communication address" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);" onchange="validate_address(this);"><span style="text-align: left">Address Line 3</span>
                        <input type="text" id="kyc_comm_add_add3" name="kyc_comm_add_add3" maxlength="40" placeholder="Enter your communication address" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);" onchange="validate_address(this);"><span style="text-align: left">City</span>
                        <input type="text" id="kyc_comm_add_city" name="kyc_comm_add_city" maxlength="50" onkeyup="pincodecheck();" placeholder="Enter your City" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);" onchange="validate_name(this);"><span style="text-align: left">Pincode</span>
                        <input type="number" id="kyc_comm_add_pincode" name="kyc_comm_add_pincode" maxlength="6" placeholder="Enter your pincode" oninput="javascript: if (this.value.length > this.maxLength) this.value= this.value.slice(0, this.maxLength);" onkeyup="pincodecheck();" onchange="verify_pincode(this);"><span style="text-align: left">State</span>
                        <select id="kyc_state" name="kyc_state" placeholder="Select your state">
                            <option value="">Select Value</option>
                        </select>
                    </div>
                    <div id="365__Schedule_KYC__Schedule_details" class="" style=""><span class="new-hd-ac" style="text-align:left">Schedule Slot</span><span style="text-align: left">Date</span>
                        <input type="text" id="kyc_date1" name="kyc_date"  max ="", min= "" placeholder="dd/mm/yyy" onchange="validate_address(this);"><span style="text-align: left">Time</span>
                        <select id="kyc_time1" name="kyc_time" placeholder="Select Time">
                            <option value="">Select Value</option>
                            <option value="1">Morning Slot</option>
                            <option value="2">Afternoon Slot</option>
                            <option value="3">Evening Slot</option>
                        </select>
                        <button id="button_submit_kycschedule" name="button_submit_kycschedule" class="button-ac rating-chkbox" onclick="fullKyc_submit();">SUBMIT</button>
                    </div> -->


                  <!-- ==================================eod-vkyc===================== -->
                  
                        <div id="Rating_div" class="cont-div" style="display:none">
                            <div id="260__Rating__customer_rating" class="" style="">
                                <img src="images/end-button.png" class="close-img" onclick="skip_rating(this);"><span class="new-hd-ac" id="old_rate" style="text-align: center"><h2>Congratulations! </h2><p>You opened your 101 Savings account in <span class="new-time" id="tmc"></span> minutes only</p><hr></span>
                        <span class="hide" id='new_rate' style="text-align: center"><h2>Congratulations! </h2><p>You have successfully opened your Savings account at Fincare</p></span>
                                <span class="cons-txt-ac" style="text-align: center">How Would You Rate Your 101 Account Opening Experience?</span><fieldset class="rating-stars text-center" style=" display:block;text-align: center"><span id="r1" onclick="setRating(this);" class="stardesign staropa f1"></span><span id="r2" onclick="setRating(this);" class="stardesign staropa f2"></span><span id="r3" onclick="setRating(this);" class="stardesign staropa f3"></span><span id="r4" onclick="setRating(this);" class="stardesign staropa f4"></span><span id="r5" onclick="setRating(this);" class="stardesign staropa f5"></span>
                                </fieldset>
                            </div>
                            <br><br>
                            <div id="261__Rating__customer_rating_less" class="hide rating-chkbox" style=""><span class="new-hd-ac" style="text-align:center">Oops! Your feedback will help us to improve</span><span class="cons-txt-ac btm-space15" style="text-align:center">Help us to understand what went wrong to serve you better? </span>
                                
                                    <input type="checkbox" name="checkbox_customer_rating_less" value="Too_much_doc">
                                   <label>Too much documentation</label>
                                
                                   <input type="checkbox" name="checkbox_customer_rating_less"   value="Noguidance">
                                   <label>Difficult to navigate</label>
                                
                                   <input type="checkbox" name="checkbox_customer_rating_less"  value="Did_not_have_everything_ready"> 
                                   <label>Did not have everything ready/handy</label>
                                  
                                   <input type="checkbox" name="checkbox_customer_rating_less"  value="Did_not_receive_SMS/Emails_for_completing_the_process">
                                   <label>Did not receive timely SMS/Email</label>
                                
                                   <input type="checkbox" name="checkbox_customer_rating_less"  value="Prefer to use Laptop/Desktop">
                                   <label>Prefer to use Laptop/Desktop</label>
                                
                                   <input type="checkbox" name="checkbox_customer_rating_less" value="Other Reasons" onclick='otherreson();'>
                                   <label>Other Reasons </label>
                                
                                <input type="text" id="rating-input" class="hide">
                                <button id="button_submit_rl" class="button-ac btm-space" onclick="Storeratingless()">SUBMIT</button>
                            </div>
                            <br><br>
                            <div id="262__Rating__customer_rating_more" class="hide" style=""><span class="new-hd-ac" style="text-align:center">Rate and Share your Experience</span>
                                <input type="text" id="text_customer_rating_more_email1" name="text_customer_rating_more_email1"  placeholder="Enter your Friend Email ID" aria-required="true">
                                <input type="text" id="text_customer_rating_more_email2" name="text_customer_rating_more_email2" placeholder="Enter your Friend Email ID">
                                <input type="text" id="text_customer_rating_more_email3" name="text_customer_rating_more_email3" placeholder="Enter your Friend Email ID">
                                <input type="text" id="text_customer_rating_more_email4" name="text_customer_rating_more_email4" placeholder="Enter your Friend Email ID">
                                <input type="text" id="text_customer_rating_more_email5" name="text_customer_rating_more_email5" placeholder="Enter your Friend Email ID"><span class="new-hd-ac" style="text-align:center">Share Your Experience</span>
                                <fieldset class="new-hd-ac" style="text-align:center">
                                    <button class="button focus" onclick="setShare(this);" id="defaultOpen">Share</button>
                                    <button class="button" onclick="setShare(this);" id="btn_customize">Customize</button>
                                    <div id="share" class="tabcontent" style="background-color: rgb(241, 241, 241); display: block;">
                                        <textarea class="sharetextarea" placeholder="">Hey Guys! I just opened my instant online 1O1 account with Fincare Small Finance Bank!Fixed deposits with interest rates up to 7.5% and savings interest rates of up to 7% are now just 5 minutes away.</textarea>
                                    </div>
                                    <div id="customize" class="tabcontent" style="background-color: rgb(248, 248, 248); display: none;">
                                        <textarea class="sharetextarea" placeholder="Please compose your customized message here..."></textarea>
                                    </div>
                                </fieldset>
                                
                            </div>
                            <br>
                            <div id="263__Rating__customer_rating_more_tab" class="" style="">
                                <button id="button_submit_rm" class="button-ac btm-space" onclick="Storeratingmore()" style="">SUBMIT</button>
                            </div>
                            <div id="264__Rating__customer_hau" class="rating-chkbox" style="display: none;"><span class="new-hd-ac" style="text-align:center">How did you hear about us</span>
                    
                                
                                    <input type="checkbox" id="checkbox_customer_hau" name="checkbox_customer_hau" required="true" onclick="" value="Other_sources">
                                    <label>Other Sources</label>
                        
                                <input type="text" id="text_hau" class="hide">
                                <button id="button_submit_hau" name="button_submit_hau" class="button-ac" onclick="Storehau()">SUBMIT</button>
                            </div>
                        </div>
                        
                    </div>
                </div>
                </form>
                </span>
                </span>
                </div>
                </form>
                </span>
                </span>
                </div>
                </div>
            </div>
        </div>
    </div>
    </section>
    <div id="FIN10_FirstPagetooltip" class="sno" style="width:200px;position: absolute;">
        <p id="tooltext"></p>
    </div>
    </div>
    </div>
    <div id="page_2" class="sno"></div>
    </div>
    <div id="366__Footer_Step1__footer_step1" class="" style=""> <span id="footer" class="footer">
                                                <p>© 2020 | Fincare Small Finance Bank Limited</p>
                                             </span>
        <span>
        <div id="wait" style="display: none;">
                <div id="wait-container">
                   <img src="images/demo_wait.gif" id="loader-image" width="64" height="64">
                   Please Wait.....
                </div>
             </div>
                                                <section id="footer-2">
                                                   <div class="container">
                                                      <div class="row">
                                                         <a href="#top" class="toTopLink" style="display: none;">
                                                            <img src="images/arrow-top.png" height="10" width="20" alt="">
                                                            <div class="circle">
                                                               <div class="full-hexagon">
                                                                  <div class="hex-upper hex-box offer-hex"></div>
                                                                  <div class="hex-middle offer-hex"></div>
                                                                  <div class="hex-lower hex-box offer-hex"></div>
                                                               </div>
                                                            </div>
                                                         </a>
                                                      </div>
                                                   </div>
                                                </section>
                                             </span>
    </div>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.css">
    <script>
         $("#kyc_date1").datepicker({
        minDate: "+2", // months count starts from 0
        maxDate: "+365",
        //buttonText: "Select date",
        dateFormat: 'yy-mm-dd',
       // showOn: "both"
    });
    </script>
    <script type="text/javascript" charset="utf-8"></script>
    <!-- Facebook Pixel Code -->
    <script>
      !function(f,b,e,v,n,t,s)
      {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
      n.callMethod.apply(n,arguments):n.queue.push(arguments)};
      if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
      n.queue=[];t=b.createElement(e);t.async=!0;
      t.src=v;s=b.getElementsByTagName(e)[0];
      s.parentNode.insertBefore(t,s)}(window, document,'script',
      'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '1777642888951591');
      fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
      src="https://www.facebook.com/tr?id=1777642888951591&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->
    
    <!--Linked IN code-->
    
    <script type="text/javascript">
    _linkedin_partner_id = "1898914";
    window._linkedin_data_partner_ids = window._linkedin_data_partner_ids || [];
    window._linkedin_data_partner_ids.push(_linkedin_partner_id);
    </script><script type="text/javascript">
    (function(){var s = document.getElementsByTagName("script")[0];
    var b = document.createElement("script");
    b.type = "text/javascript";b.async = true;
    b.src = "https://snap.licdn.com/li.lms-analytics/insight.min.js";
    s.parentNode.insertBefore(b, s);})();
    </script>
    <noscript>
    <img height="1" width="1" style="display:none;" alt="" src="https://px.ads.linkedin.com/collect/?pid=1898914&fmt=gif" />
    </noscript>
                     <script src="//platform.twitter.com/oct.js" type="text/javascript"></script>
    
    <!-- Twitter -->
    <script>
    !function(e,t,n,s,u,a){e.twq||(s=e.twq=function(){s.exe?s.exe.apply(s,arguments):s.queue.push(arguments);
    },s.version='1.1',s.queue=[],u=t.createElement(n),u.async=!0,u.src='//static.ads-twitter.com/uwt.js',
    a=t.getElementsByTagName(n)[0],a.parentNode.insertBefore(u,a))}(window,document,'script');
    twq('init','o3goh');
    twq('track','PageView');
    </script>
    
    <!-- Twitter code end -->
       <script src="js/obj_design1.js"></script>
        <script>
        (function(i,s,o,g,r,a,m,n){i.moengage_object=r;t={};q=function(f){return function(){(i.moengage_q=i.moengage_q||[]).push({f:f,a:arguments})}};f=['track_event','add_user_attribute','add_first_name','add_last_name','add_email','add_mobile','add_user_name','add_gender','add_birthday','destroy_session','add_unique_user_id','moe_events','call_web_push','track','location_type_attribute'],h={onsite:["getData","registerCallback"]};for(k in f){t[f[k]]=q(f[k])}for(k in h)for(l in h[k]){null==t[k]&&(t[k]={}),t[k][h[k][l]]=q(k+"."+h[k][l])}a=s.createElement(o);m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m);i.moe=i.moe||function(){n=arguments[0];return t};a.onload=function(){if(n){i[r]=moe(n)}}})(window,document,'script','https://cdn.moengage.com/webpush/moe_webSdk.min.latest.js','Moengage')

          Moengage = moe({
         app_id:"H28RVHXH8JV7VNHS8H6TDYBX",
          debug_logs: 1,
          cluster: "in"
        });
        </script>
        <script src="js/obj_design1.js"></script>
        <script>
        $(function(){
    sess.images = [];
    });
        </script>
        <script>
        function searchDropdown(){
            $("#branchlead_state,#dropdown_dbt_bank_list,#cust_prefix,#comm_add_state,#profile_info_annual_income,#profile_info_occupation,#profile_info_religion,#profile_info_branch,#dropdown_nominee_relation,#nominee_state,#nominee_minor_state,#input_minor_nominee_rel").select2();
         }
        function searchble_dd_comm_add()
        {
            $("#comm_add_state,#input_address_proof_type").select2();
        }

        
        $(".openSubMenu").each(function() {
        $(this).click(function() {
        $(this).parent().find("b").toggleClass("caret-up");
        $(this).parent().find("li").toggleClass("active");
                var id =    $(this).next("ul").attr('id');
                var hidden =    $(this).next("ul").attr('hidden');

        var x = document.getElementById(id);
            if (x.hidden === true) {
                x.hidden = false;

            } else {
                x.hidden = true;
            }
        });
    });
   $("#kyc_state").val("");
    $("#comm_add_state").val("");
     $("#branchlead_state").val("");
        </script>
</body>

</html>